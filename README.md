# Usage
Assumption: current directory is `/var/www` which is also your Apache `DocumentRoot`.

Clone the app
```
git clone git@bitbucket.org:rcole/sylo.git
```

Switch to the cloned directory
```
cd sylo
```

Initialize all submodules (aka dependencies used in this app)
```
git submodule update --init --recursive
```

Add the right permissions
```
chmod -R 0777 app/resources
```

Copy the database connections file and edit it to add your connection data
```
cp app/config/bootstrap/connections.sample.php app/config/bootstrap/connections.php
```

Set up db with some basic records
```
./li3 db reload
```

## Dependencies
- nodejs
- npm
- grunt
- compass
- gsutil

### Install on debian based distro
- apt-get install ruby ruby-dev nodejs npm nodejs-legacy python-pip
- npm install -g grunt-cli
- gem install compass
- pip install gsutil

### Gsutil
Docs here https://cloud.google.com/storage/docs/gsutil_install
Basicly need to run
```
gsutil config
```

and create new service credentials p12 type, which generate p12 private key. This key have to be stored in app/keys/gcskey.p12
