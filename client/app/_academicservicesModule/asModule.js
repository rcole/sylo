// asModule.js Starts here 

var academicservicesApp = angular.module('academicservicesApp' , [
  'globals',
  'semesterProgramModule',
  'semesterProgramClassModule',
  'semestersModule',
  'programsModule',
  'extcodesModule',
  'myclassesModule',
  'usersModule',
  'modulesModule',
  'flow',
  'MessageCenterModule',
  'uservoiceModule',
  'analyticsModule',
  'submissionsListModule'
]);

academicservicesApp.config(['$stateProvider','$urlRouterProvider','$locationProvider',function($stateProvider, $urlRouterProvider, $locationProvider){
    
  $urlRouterProvider.otherwise("/programs");
  $locationProvider.html5Mode(false);
    
}]);