// semestersModule.js Starts here 

// move semester routes from classnest and myclassesnest here.

var semestersModule = angular.module('semestersModule', ['smart-table','ngResource']);

semestersModule.config(['$stateProvider','$urlRouterProvider','$locationProvider',function($stateProvider, $urlRouterProvider, $locationProvider){
  
  var semesters = {
    name: "semesters",
    url: "/semesters",
    templateUrl: "app/webroot/tpl/semesterList.tpl.html",
    controller: "semestersController as semesters"
  }, 
  semesterEdit = {
    name: "semesterEdit",
    parent: 'semesters',
    url: "/:semester/edit",
    templateUrl: "app/webroot/tpl/semesterForm.tpl.html",
    controller: "semesterEditController as semForm"
  },
  semesterCreate = {
    name: "semesterCreate",
    parent: 'semesters',
    url: "/create",
    templateUrl: "app/webroot/tpl/semesterForm.tpl.html",
    controller: "semesterCreateController as semForm"
  };

  $stateProvider
	.state(semesters)
	.state(semesterEdit)
	.state(semesterCreate);

}]);