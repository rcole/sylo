// start semestersController.js

semestersModule.controller('semesterCreateController', ['$scope', '$q', '$timeout', '$state', 'semesterFactory', 'messageCenterService', function ($scope, $q, $timeout, $state, semesterFactory, messageCenterService) {
  
    console.log('semesterCreateController instantiated');

    var ctrl = this;

    ctrl.mode = 'create';

    ctrl.modal = true; 

    ctrl.message = '';

    ctrl.createItem = function(){
  
        semesterFactory.create(ctrl.item).then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while saving the semester.", { timeout: 6000 });

                console.log(data);

            } else {

                ctrl.message = 'Semester Created';

                $timeout(function(){

                    $state.go('^', {reload:true});

                }, 1500);

            }

            $scope.$parent.semesters.loadList();

        },

        function(error){

            console.log(error);

        });

    };

}]);
