semesterProgramModule.controller('programController', ['$scope', '$q', '$stateParams', '$state', 'programsFactory', 'globalsFactory', 'messageCenterService', function ($scope, $q, $stateParams, $state, programsFactory, globalsFactory, messageCenterService) {
  
  // as prg
  
  console.log('programController');

  var ctrl = this;

  ctrl.programSelection = {};

  programsFactory.get($stateParams)

    .then(function (data){

        ctrl.programSelection = data.program;
    
    }, function (error){
    
        messageCenterService.add('danger', "Something went wrong. Could you try that again?", { timeout: 6000 });
  
    });
  
}]);