semesterProgramModule.controller('prgSubmissionsController', ['$scope', '$q', '$stateParams', 'programSubmissionsFactory', 'messageCenterService', function ($scope, $q, $stateParams, programSubmissionsFactory, messageCenterService) {

    console.log('prgSubmissionsController instantiated');

    var ctrl = this;

    ctrl.subsList = [];
  
    ctrl.displayList = [];

    ctrl.loaded = false;

    programSubmissionsFactory.query($stateParams)

    .then(function (data){

        ctrl.subsList = data.submissions;

        ctrl.loaded = true;

    }, function (error){

        messageCenterService.add('danger', "Something went wrong. Could you try that again?", { timeout: 6000 });

    });

}]);