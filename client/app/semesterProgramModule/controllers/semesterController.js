// start semesterController.js

semesterProgramModule.controller('semesterController', ['$scope', '$q', '$stateParams', '$state', 'semesterFactory', 'messageCenterService', function ($scope, $q, $stateParams, $state, semesterFactory, messageCenterService) {

    console.log('semesterController instantiated');

    ctrl = this;

    ctrl.semesterSelection = {};

    semesterFactory.get($stateParams)

    .then(function (data){

        ctrl.semesterSelection = data.semester;
    
    }, function (error){
    
        messageCenterService.add('danger', "Something went wrong. Could you try that again?", { timeout: 6000 });
  
    });

    ctrl.semesterList = [];

    ctrl.loadList = function(){

        semesterFactory.query().then(function(data){

            ctrl.semesterList = data.semesters;

            ctrl.loaded = true;

        }, function (error){
      
            messageCenterService.add('danger', "Something went wrong. Could you try that again?", { timeout: 6000 });
      
        });

    };

    ctrl.loadList();

}]);