semesterProgramModule.controller('prgClassesController', ['$scope', '$q', '$stateParams', 'classesFactory', 'usersFactory', 'messageCenterService', function ($scope, $q, $stateParams, classesFactory, usersFactory, messageCenterService) {

    // as prgClasses

    console.log('prgClassesController instantiated');

    var ctrl = this;

    ctrl.loaded = false;

    ctrl.classList = [];

    ctrl.loadList = function(){

        console.log('loading classes');

        classesFactory.query($stateParams).then(function(data){

            ctrl.loaded = true; 

            ctrl.classList =  data.classes;

        }, function (error){

            messageCenterService.add('danger', "Something went wrong. Could you try that again?", { timeout: 6000 });

        });

    };

    ctrl.loadList();

    usersFactory.teachers().then(function (data){

        ctrl.teachersList =  data;

    }, function (error){

        messageCenterService.add('danger', "Something went wrong. Could you try that again?", { timeout: 6000 });

    });
  
}]);