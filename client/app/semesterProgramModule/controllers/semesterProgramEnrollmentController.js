// start programSubControllers.js

semesterProgramModule.controller('prgEnrollmentController', ['$scope', '$q', '$stateParams', 'programEnrollmentFactory', 'messageCenterService', function ($scope, $q, $stateParams, programEnrollmentFactory, messageCenterService) {
 
  console.log('prgEnrollmentController instantiated');

  var ctrl = this;
 
  ctrl.enrollment = [];

  ctrl.displayList = [];

  programEnrollmentFactory.query($stateParams)

    .then(function (data){

      ctrl.enrollment = data.students;

      ctrl.displayList = [].concat(ctrl.enrollment);

    }, function (error){

      messageCenterService.add('danger', "Something went wrong. Could you try that again?", { timeout: 6000 });

  });

}]);