// programSubmissionsFactory.js

semesterProgramModule.factory('programSubmissionsFactory', ['$http', '$resource', '$q', function ($http, $resource, $q) {
  
  var programSubmissionsResource = $resource('api/semester/:semester/program/:program/submissions.json', 
      { semester: '@semester', program: '@program' },
      {'query' : {method: 'GET', isArray: false} 
    });

  var factory = {

    query : function (params) {
    
      var deferred = $q.defer();
    
      programSubmissionsResource.query({semester: params.semester, program: params.program},
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });
    
      return deferred.promise;
    
    }

  };

  return factory;

}]);