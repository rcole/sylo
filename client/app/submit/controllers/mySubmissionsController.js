// start submitController.js

submitModule.controller('mySubmissionsController', ['$scope', '$q', '$interval', 'submitFactory', 'messageCenterService', function ($scope, $q, $interval, submitFactory, messageCenterService) {

    console.log('mySubmissionsController instantiated');

    var ctrl = this;

    ctrl.subsList = [];

    ctrl.subsDisplayList = [];

    var trySemester = $interval(function(){

        if($scope.$parent.globals.currentSemester) {
   
            getSubs();
       
            stopTrying();
            
        }

    }, 100);

    function stopTrying(){

        $interval.cancel(trySemester);

        trySemester = undefined;

    }

    function getSubs(){

        submitFactory.mySubmissions({semester: $scope.$parent.globals.currentSemester})

        .then(function (data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while getting your submissions.", { timeout: 6000 });

            } else {

                ctrl.subsList = data.submissions;

                ctrl.subsDisplayList = [].concat(ctrl.subsList);

                ctrl.loading = false;
            }

        }, 

        function (error){

            console.log('Error');

            console.log(error);

        });

    }




}]);
