// start submitController.js

// States:
// no assignment chosen
// assignment chosen
// files added for submission
// submission complete
// submission error

submitModule.controller('submitController', ['$scope', '$q', '$timeout', 'messageCenterService', 'studentFactory', 'ignoredFileTypes', 'analyticsFactory', function ($scope, $q, $timeout, messageCenterService, studentFactory, ignoredFileTypes, analyticsFactory) {
  
    console.log('submitController instantiated');

    var ctrl = this;

    ctrl.submissionComplete = false;

    ctrl.assignmentChosen = false;

    ctrl.hasFiles = false;

    ctrl.hasBeenWarned = false;

    ctrl.showDrop = function(){

        if (ctrl.assignmentChosen && !ctrl.submissionComplete) return true;

        return false;

    };

    ctrl.showTable = function(){

        if (ctrl.hasFiles) return true;

        return false;

    };

    ctrl.showStatement = function(){

        if(ctrl.hasFiles && !ctrl.submissionComplete) return true;

        return false;

    };

    ctrl.currentClasses = [];

    ctrl.selectClass = function(){

        ctrl.assignmentChosen = true;

    };

    (function myLoop (i) { 
        // we have to wait until we recieve the student's ID before we can get a class list
        
        setTimeout(function () {   
                
            if ($scope.globals.userInfo === {} && --i) { 

                myLoop(i);
            
            } else {
            
                getStudentClasses($scope.globals.userInfo.id);
            
            }
        
        }, 1000);

    })(10);  



    function getStudentClasses(stuID){

        studentFactory.query({studentId: stuID})

        .then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong getting the class list.", { timeout: 6000 });

            } else {

                ctrl.currentClasses = data.classes;

                var c = data.classes;
               
                var a = [];

                for (var i = c.length - 1; i >= 0; i--) {

                    for (var m = c[i].assignments.length - 1; m >= 0; m--) {
                    
                        c[i].assignments[m].classTitle = c[i].title;
                    
                    }

                    a = a.concat(c[i].assignments);
                
                }

                ctrl.currentAssignments = a;

            }

        },

        function(error){

            messageCenterService.add('danger', "Something went wrong getting the class list.", { timeout: 6000 });

        });

    }

    ctrl.groupAssignments = function(assgn){

        return assgn.classTitle;

    };
    
    ctrl.uploadItems = function($flow){
    
        $flow.opts.query = {student_id: $scope.globals.userInfo.id, assignment_id: ctrl.submissionParams};
    
        $flow.upload();

        ctrl.uploadStart = new Date();
    
    };

    ctrl.canUpload = function(){

        if (ctrl.submissionParams && ctrl.agree) {
    
            return true;
    
        } else {

            return false;

        }

    };

    ctrl.newSubmission = function($flow){

        $flow.cancel();

        ctrl.submissionComplete = false;

        ctrl.assignmentChosen = false;

        ctrl.submissionParams = '';

        ctrl.hasFiles = false;

    };

    ctrl.filesAdded = function($flow, $files){

        ctrl.dropClass = '';

        ctrl.hasFiles = true;

        if($files.length > 8 && !ctrl.hasBeenWarned){

            ctrl.hasBeenWarned = true;

            messageCenterService.add('warning', "You are uploading a lot of files. If they are all part of a single work, please compress them first as a .zip file.", {status: 'permanent' });
                    
        }
        var ignored = [];

        angular.forEach($files, function(value, key) {
            if (ignoredFileTypes.indexOf(value.name) > -1 || value.size === 0){
                ignored.push(value.name);
                $flow.removeFile(value);
            }
        });

        if(ignored.length > 0){
            var ignoredString = "";
            for (var i = ignored.length - 1; i >= 0; i--) {
                ignoredString = ignoredString + ignored[i];
                if(i > 0){
                    ignoredString = ignoredString + ', ';
                }
            };
            messageCenterService.add('info', "These files were ignored: " + ignoredString , {status: 'permanent' });
        }
    };

    ctrl.fileRetry = function($file){
        ctrl.uploadMessage = 'Retrying ' + $file.name;
    };

    ctrl.fileError = function($file){
        ctrl.uploadMessage = 'Failed to upload ' + $file.name;
        messageCenterService.add('danger', "Something went wrong uploading your submission! Please check your connection, and try it again. If you keep having issues, contact your instructor.", { status: messageCenterService.status.permanent  });
    };

    var fetchDataTimer;

    ctrl.complete = function($flow){
        $timeout.cancel(fetchDataTimer);
        ctrl.submissionComplete = true;
        ctrl.count = $flow.files.length;
        ctrl.uploadEnd = new Date();
        
        fetchDataTimer = $timeout(function(){
            makeAndSendReport($flow);
        }, 1200);
    };

    function makeAndSendReport(last$flow){
        var fileError = false,
            uploadDuration = (ctrl.uploadEnd - ctrl.uploadStart) / 1000;

        ctrl.erroredFilesDisplay = [];

        for (var i = last$flow.files.length - 1; i >= 0; i--) {
            var report = {};

            if(last$flow.files[i].error) {
                fileError = true;
                ctrl.erroredFilesDisplay.push(last$flow.files[i].name);
                report.fileName = last$flow.files[i].name;
                report.fileSize = last$flow.files[i].size;
                report.fileError = last$flow.files[i].error;
                report.uploadDuration = uploadDuration;
                analyticsFactory.uploadErrors(report);
            } else {
                report.fileName = last$flow.files[i].name;
                report.fileSize = last$flow.files[i].size;
                report.fileError = last$flow.files[i].error;
                report.uploadDuration = uploadDuration;
                analyticsFactory.uploadSuccess(report);
            }      
        }

        if (fileError) {
            ctrl.submissionResult = 'error';
        } else {
            ctrl.submissionResult = 'success';
        }
    }
}]);
