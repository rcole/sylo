programsModule.controller('programsController', ['$scope','$q','programsFactory', 'usersFactory', 'messageCenterService', function($scope, $q, programsFactory, usersFactory, messageCenterService) {

    var ctrl = this;

    ctrl.loaded = false;

    ctrl.loadList = function(){

        programsFactory.query().then(function (data){

            ctrl.programList = data.programs;

            ctrl.loaded = true;

        }, function (error){

            messageCenterService.add('danger', "Something went wrong. Could you try that again?", { timeout: 6000 });

        }); 

    };

    ctrl.loadList();

    ctrl.programLeaders = [];

    usersFactory.programLeaders().then(function (data){

        ctrl.programLeaders = data.roles;
    
    }, function (error){
        console.warn(error);
        messageCenterService.add('danger', "Something went wrong. Could you try that again?", { timeout: 6000 });
    });

}]);