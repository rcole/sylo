modulesModule.controller('prgModulesController', ['$scope', '$q', '$stateParams', 'modulesFactory', 'programsFactory', 'messageCenterService', function ($scope, $q, $stateParams, modulesFactory, programsFactory, messageCenterService) {

    var ctrl = this;

    ctrl.loaded = false;

    ctrl.loadList = function(){

        modulesFactory.query($stateParams).then(function (data){

            ctrl.loaded = true;

            ctrl.programModuleList = data.modules;

        }, function (error){

            messageCenterService.add('danger', "Something went wrong getting the modules. Could you try that again?", { timeout: 6000 });

        });

    };

    ctrl.loadList();

    programsFactory.query().then(function (data){

        ctrl.programList = data.programs;

    }, function (error){
        console.warn(error);
        messageCenterService.add('danger', "Something went wrong getting the programs. Could you try that again?", { timeout: 6000 });

    });
    
}]);