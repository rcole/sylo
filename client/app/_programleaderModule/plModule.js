// plModule.js Starts here 

var programleaderApp = angular.module('programleaderApp' , [
  'globals',
  'semesterProgramModule',
  'semesterProgramClassModule',
  'semestersModule',
  'programsModule',
  'extcodesModule',
  'myclassesModule',
  'usersModule',
  'modulesModule',
  'flow',
  'MessageCenterModule',
  'uservoiceModule',
  'analyticsModule',
  'submissionsListModule'
]);
// we need to route to the program classes for the program leader... How to get that?

programleaderApp.config(['$stateProvider','$urlRouterProvider','$locationProvider',function($stateProvider, $urlRouterProvider, $locationProvider){
    
  $urlRouterProvider.otherwise("/programs");
  $locationProvider.html5Mode(false);
    
}]);