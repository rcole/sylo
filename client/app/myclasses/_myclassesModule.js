// myclassesModule.js Starts here 

var myclassesModule = angular.module('myclassesModule', ['smart-table','ngResource']);

myclassesModule.config(['$stateProvider','$urlRouterProvider','$locationProvider',function($stateProvider, $urlRouterProvider, $locationProvider){
    
  $urlRouterProvider.when('/semester/{semester}/myclasses', '/semester/{semester}/myclasses/classes');
  
  $urlRouterProvider.when('/semester/{semester}/myclasses/class/{class}', '/semester/{semester}/myclasses/class/{class}/assignments');

  // $state.go('myClasses', {semester: $scope.$parent.globals.currentSemester});

  // routes for the myclasses group of pages
  var mcRoot = { // fugly!
    name: "myRoot",
    url: "/myclasses",
    templateUrl: "app/webroot/tpl/mc.tabFrame.tpl.html",
    controller: "mcRootController"
  },
  myClassesSemesterView = {
    name: "myClassesSemesterView",
    url: "/semester/:semester",
    controller: "semesterController as sem",
    abstract: true,
    template: '<ui-view/>'
  }, 
  myClasses = {
    name: "myClasses",
    parent: 'myClassesSemesterView',
    url: "/myclasses",
    templateUrl: "app/webroot/tpl/mc.tabFrame.tpl.html",
    controller: "myClassesController as mc"
  },
  myClassesSubmissions = {
    name: "myClassesSubmissions",
    parent: 'myClasses',
    url: "/submissions",
    templateUrl: "app/webroot/tpl/mc.submissions.tpl.html",
    controller: "myClassesSubmissionsController as mcSubs"
  },
  myClassesClasses = {
    name: "myClassesClasses",
    parent: 'myClasses',
    url: "/classes",
    templateUrl: "app/webroot/tpl/mc.classes.tpl.html",
    controller: "myClassesClassesController as mcClasses"
  },
  myClassesClassView = {
    name: "myClassesClassView",
    parent: 'myClasses',
    url: "/class/:class",
    templateUrl: "app/webroot/tpl/mc.class.tabFrame.tpl.html",
    controller: "myClassesClassController as mcClass"
  },
  myClassesStudentSubmissions = {
    name: "myClassesStudentSubmissions",
    parent: 'myClasses',
    url: "/submissions/:id",
    templateUrl: "app/webroot/tpl/mc.student.submissions.tpl.html",
    controller: "myClassesStudentSubmissionsController as mcStuSubs"
  },
  myClassesClassSubmissions = {
    name: "myClassesClassSubmissions",
    parent: 'myClassesClassView',
    url: "/submissions",
    templateUrl: "app/webroot/tpl/mc.class.submissions.tpl.html",
    controller: "myClassesClassSubmissionsController as mcClsSubs"
  },
  myClassesClassAssignments = {
    name: "myClassesClassAssignments",
    parent: 'myClassesClassView',
    url: "/assignments",
    templateUrl: "app/webroot/tpl/mc.class.assignments.tpl.html",
    controller: "myClassesClassAssignmentsController as mcClsAssign"
  },
  myClassesClassAssignmentEdit = {
    name: "myClassesClassAssignmentEdit",
    parent: 'myClassesClassAssignments',
    url: "/:assg/edit",
    templateUrl: "app/webroot/tpl/mc.class.assignmentForm.tpl.html",
    controller: "mcAssignmentEditController as mcAssignForm"
  },
  myClassesClassAssignmentCreate = {
    name: "myClassesClassAssignmentCreate",
    parent: 'myClassesClassAssignments',
    url: "/create",
    templateUrl: "app/webroot/tpl/mc.class.assignmentForm.tpl.html",
    controller: "mcAssignmentCreateController as mcAssignForm"
  },
  myClassesClassEnrollment = {
    name: "myClassesClassEnrollment",
    parent: 'myClassesClassView',
    url: "/enrollment",
    templateUrl: "app/webroot/tpl/mc.class.enrollment.tpl.html",
    controller: "myClassesClassEnrollmentController as mcClsEnroll"
  },
  myClassesClassFeedback = {
    name: "myClassesClassFeedback",
    parent: 'myClassesClassView',
    url: "/feedback",
    templateUrl: "app/webroot/tpl/mc.class.feedback.tpl.html",
    controller: "myClassesClassFeedbackController as mcClsFeed"
  },
  myClassesClassDocumentation = {
    name: "myClassesClassDocumentation",
    parent: 'myClassesClassView',
    url: "/documentation",
    templateUrl: "app/webroot/tpl/mc.class.documentation.tpl.html",
    controller: "myClassesClassDocumentationController as mcClsDoc"
  };

  $stateProvider
    .state(mcRoot)
    .state(myClassesSemesterView)
    .state(myClasses)
    .state(myClassesSubmissions)
    .state(myClassesClasses)
    .state(myClassesClassView)
    .state(myClassesClassAssignments)
    .state(myClassesClassSubmissions)
    .state(myClassesClassEnrollment)
    .state(myClassesClassFeedback)
    .state(myClassesClassDocumentation)
    .state(myClassesStudentSubmissions)
    .state(myClassesClassAssignmentEdit)
    .state(myClassesClassAssignmentCreate);
}]);