// start mcRootController.js

myclassesModule.controller('mcRootController', ['$scope', '$state', '$timeout', function ($scope, $state, $timeout) {

    $timeout(function(){

        $state.go('myClasses', {semester: $scope.$parent.globals.currentSemester});

    }, 1500);   

}]);