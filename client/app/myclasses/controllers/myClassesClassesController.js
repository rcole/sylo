// myClassesClassesController.js

myclassesModule.controller('myClassesClassesController', ['$scope', '$q', 'myClassesFactory', '$stateParams', function ($scope, $q, myClassesFactory, $stateParams) {

  var ctrl = this;

  ctrl.myClassesExist = false;

  ctrl.classList = [];

  // pass in semester from "selectedSemester"
  myClassesFactory.query($stateParams)

    .then(function (data){

      if (data.classes && data.classes.length > 0) ctrl.myClassesExist = true; 

      ctrl.classList =  data.classes;

    }, function (error){

      console.warn(error);

    });

}]);