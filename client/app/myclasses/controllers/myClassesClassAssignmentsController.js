// myClassesClassAssignmentsController.js

myclassesModule.controller('myClassesClassAssignmentsController', ['$scope', '$q', '$stateParams', 'myClassesAssignmentFactory', 'messageCenterService', function ($scope, $q, $stateParams, myClassesAssignmentFactory, messageCenterService) {

    var ctrl = this;

    ctrl.loaded = false;

    ctrl.assignmentList = [];

    ctrl.loadList = function(){

        myClassesAssignmentFactory.query($stateParams).then(function (data){

            ctrl.assignmentList = data.assignments;

            ctrl.loaded = true;

        }, function (error){
            console.warn(error);
            messageCenterService.add('danger', "Something went wrong. Could you try that again?", { timeout: 6000 });

        });

    };

    ctrl.loadList();
  
}]);