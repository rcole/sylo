// myClassesClassSubmissionsController.js

myclassesModule.controller('myClassesStudentSubmissionsController', ['$scope', '$q', '$stateParams', 'myClassesSubmissionsFactory', function ($scope, $q, $stateParams, myClassesSubmissionsFactory) {

    var ctrl = this;

    ctrl.loaded = false;

    ctrl.subsList =  [];
    
    ctrl.displayList = [];

    ctrl.submissionsExist = false;

    myClassesSubmissionsFactory.student($stateParams)

    .then(function (data){

        if (data.submissions && data.submissions.length > 0) ctrl.submissionsExist = true; 

        ctrl.subsList =  data.submissions;

        ctrl.displayList =  data.submissions;

        if(data.submissions.length > 0) {

            // ghetto get user info
            
            ctrl.student = data.submissions[0].student;

        }

        ctrl.loaded = true;

    }, function (error){

        console.warn(error);

    });

    // single class stuff here.

}]);