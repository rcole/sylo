// myClassesClassController.js

myclassesModule.controller('myClassesClassController', ['$scope', '$q', 'myClassesFactory', 'globalsFactory', '$stateParams', '$state', 'messageCenterService', function ($scope, $q, myClassesFactory, globalsFactory, $stateParams, $state, messageCenterService) {

    var ctrl = this;

    ctrl.classSelection = {};

    myClassesFactory.get($stateParams)

    .then(function (data){

        ctrl.classSelection = data.classes[0];
    
    }, function (error){
        console.warn(error);
        messageCenterService.add('danger', "Something went wrong. Could you try that again?", { timeout: 6000 });
  
    });

    ctrl.classSwitcher = function($item, $model){

        page = $state.current.name;

        $state.go(page, {class: $item.slug});

    };

}]);
