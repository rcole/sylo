// myClassesClassEnrollmentController.js

myclassesModule.controller('myClassesClassEnrollmentController', ['$scope', '$q', '$stateParams', 'myClassesEnrollmentFactory', function ($scope, $q, $stateParams, myClassesEnrollmentFactory) {

  var ctrl = this;

  ctrl.studentList = [];

  ctrl.displayList = [];


  myClassesEnrollmentFactory.query($stateParams)

    .then(function (data){

        ctrl.studentList =  data.students;

        ctrl.displayList =  data.students;

    }, function (error){

        console.warn(error);

    });

}]);