// myClassesSubmissionsController.js

myclassesModule.controller('myClassesSubmissionsController', ['$scope', '$q', '$stateParams', 'myClassesSubmissionsFactory', 'messageCenterService', function ($scope, $q, $stateParams, myClassesSubmissionsFactory, messageCenterService) {

  var ctrl = this;

  ctrl.loaded = false;

  ctrl.subsList = [];
  
  ctrl.displayList = [];

  myClassesSubmissionsFactory.all($stateParams).then(function (data){
  
      if (data._error != 1) {

        ctrl.subsList = data.submissions;

        ctrl.displayList = data.submissions;

      } else {

        messageCenterService.add('danger', "Something went wrong. Could you try that again?", { timeout: 6000 });

      }

      ctrl.loaded = true;
  
    }, function (error){
        console.warn(error);
      messageCenterService.add('danger', "Something went wrong. Could you try that again?", { timeout: 6000 });
  
    });

}]);