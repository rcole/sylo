// myClassesClassSubmissionsController.js

myclassesModule.controller('myClassesClassSubmissionsController', ['$scope', '$q', '$stateParams', 'myClassesSubmissionsFactory', 'messageCenterService', function ($scope, $q, $stateParams, myClassesSubmissionsFactory, messageCenterService) {
    
    var ctrl = this;

    ctrl.subsList = [];
  
    ctrl.displayList = [];

    ctrl.loaded = false;

    myClassesSubmissionsFactory.classSubs($stateParams)

    .then(function (data){

      if (data._error != 1) {

        ctrl.subsList = data.submissions;

      } else {

        messageCenterService.add('danger', "Something went wrong. Could you try that again?", { timeout: 6000 });
        
      }

      ctrl.loaded = true;

    }, function (error){
        console.warn(error)
        messageCenterService.add('danger', "Something went wrong. Could you try that again?", { timeout: 6000 });
});

}]);