// really not done. Break this up, flatten the methods

myclassesModule.factory('myClassesFactory', ['$http', '$resource', '$q', function ($http, $resource, $q) {

  var myClassesResource = $resource('api/semester/:semester/myclasses.json', {semester: '@semester'},
    
    {'query': {method: 'GET', isArray: false, cache: true} });

  var myClassResource = $resource('api/semester/:semester/myclasses/:class.json',

    { semester: '@semester', prg: '@prg', class: '@class' },

    {

      'get': {method: 'GET'}

    });


  var factory = {
   
    query : function(params) {
   
      var deferred = $q.defer();

      myClassesResource.query({semester: params.semester},
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;
   
    },

    get: function (params) {

      var deferred = $q.defer();

      myClassResource.get({semester: params.semester, class: params.class},
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    }
  
  };
  
  return factory;

}]);