// really not done. Break this up, flatten the methods

myclassesModule.factory('myClassesAssignmentFactory', ['$http', '$resource', '$q', function ($http, $resource, $q) {

  var mcAssignmentResource = $resource('api/semester/:semester/myclasses/:class/assignment/:assg.json', 

    { semester: '@semester', class: '@class', assg: '@assg'},
    
    {

      'get': {method: 'GET', isArray: false},

      'update': {method: 'POST', isArray: false},

      'remove': {method: 'POST', isArray: false} 

    });

   var mcAssignmentsResource = $resource('api/semester/:semester/myclasses/:class/assignments.json', 

    { semester: '@semester', class: '@class'},
    
    {

      'query': {method: 'GET', isArray: false},

      'create': {method: 'POST', isArray: false}

    });


  var factory = {

    get : function(params) {
   
      var deferred = $q.defer();

      mcAssignmentResource.get({semester: params.semester, class: params.class, assg: params.assg},
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;
   
    },
   
    query : function(params) {
   
      var deferred = $q.defer();

      mcAssignmentsResource.query({semester: params.semester, class: params.class},
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;
   
    },

    update : function(params, payload){

      var deferred = $q.defer();

      payload._method = 'put';

      mcAssignmentResource.update({semester: params.semester, class: params.class, assg: params.assg}, payload,
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    },

    create : function(params, payload){

      var deferred = $q.defer();

      mcAssignmentsResource.create({semester: params.semester, class: params.class}, payload, 
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    },

    remove : function(params){

      var deferred = $q.defer();

      var payload = { _method: 'delete' };

      mcAssignmentResource.remove({semester: params.semester, class: params.class, assg: params.assg}, payload,
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    }
  
  };
  
  return factory;

}]);