// really not done. Break this up, flatten the methods

myclassesModule.factory('myClassesFeedbackFactory', ['$http', '$resource', '$q', function ($http, $resource, $q) {

  var mcFeedbackResource = $resource('api/semester/:semester/myclasses/:class/feedback/:id.json', 

    { semester: '@semester', class: '@class', id: '@id' },
    
    {

      'get': {method: 'GET', isArray: false, cache: true},

      'update': {method: 'POST'},

      'remove': {method: 'POST'} 

    });

   var mcFeedbacksResource = $resource('api/semester/:semester/myclasses/:class/feedback.json', 

    { semester: '@semester', class: '@class'},
    
    {

      'query': {method: 'GET', isArray: false, cache: true},

      'create': {method: 'POST'}

    });


  var factory = {

    get : function(params) {
   
      var deferred = $q.defer();

      mcFeedbackResource.query({semester: params.semester, class: params.class, id: params.id},
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;
   
    },
   
    query : function(params) {
   
      var deferred = $q.defer();

      mcFeedbacksResource.query({semester: params.semester, class: params.class},
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;
   
    },

    update : function(params, payload){

      var deferred = $q.defer();

      payload._method = 'put';

      mcFeedbackResource.query({semester: params.semester, class: params.class, id: params.id},
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    },

    create : function(params, payload){

      var deferred = $q.defer();

      mcFeedbacksResource.query({semester: params.semester, class: params.class}, payload, 
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    },

    remove : function(params){

      var deferred = $q.defer(),
          payload = { _method: 'delete' };

      mcFeedbackResource.remove({semester: params.semester, class: params.class, id: params.id}, payload,
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    }
  
  };
  
  return factory;

}]);