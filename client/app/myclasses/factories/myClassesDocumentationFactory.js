// really not done. Break this up, flatten the methods

myclassesModule.factory('myClassesDocumentationFactory', ['$http', '$resource', '$q', function ($http, $resource, $q) {

  var mcDocResource = $resource('api/semester/:semester/myclasses/:class/documentation/:id.json', 

    { semester: '@semester', class: '@class', id: '@id' },
    
    {

      'get': {method: 'GET', isArray: false, cache: true},

      'update': {method: 'POST'},

      'remove': {method: 'POST'} 

    });

   var mcDocsResource = $resource('api/semester/:semester/myclasses/:class/documentation.json', 

    { semester: '@semester', class: '@class'},
    
    {

      'query': {method: 'GET', isArray: false, cache: true},

      'create': {method: 'POST'}

    });


  var factory = {

    get : function(params) {
   
      var deferred = $q.defer();

      mcDocResource.query({semester: params.semester, class: params.class, id: params.id},
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;
   
    },
   
    query : function(params) {
   
      var deferred = $q.defer();

      mcDocsResource.query({semester: params.semester, class: params.class},
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;
   
    },

    update : function(params, payload){

      var deferred = $q.defer();

      payload._method = 'put';

      mcDocResource.query({semester: params.semester, class: params.class, id: params.id},
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    },

    create : function(params, payload){

      var deferred = $q.defer();

      mcDocsResource.query({semester: params.semester, class: params.class}, payload, 
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    },

    remove : function(params){

      var deferred = $q.defer(),
          payload = { _method: 'delete' };

      mcDocResource.remove({semester: params.semester, class: params.class, id: params.id}, payload,
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    }
  
  };
  
  return factory;

}]);