// globalModule.js Starts here 

var globals = angular.module('globals', ['ui.router', 'ngQuickDate', 'MessageCenterModule']);

globals.config(['$stateProvider','$urlRouterProvider','$locationProvider', '$provide', '$httpProvider', '$sceDelegateProvider', function($stateProvider, $urlRouterProvider, $locationProvider, $provide, $httpProvider, $sceDelegateProvider){
    
  $urlRouterProvider.otherwise("/");
  $locationProvider.html5Mode(false);
  
  var profile = {
    name: "profile",
    url: "/profile",
    templateUrl: "app/webroot/tpl/profileEdit.tpl.html",
    controller: "profileController as profile"
  },
  logout = {
      name: "logout",
      url: "/logout",
      template: "",
      controller: "logoutController as logout"
  };
  
  $stateProvider.state(profile);
  $stateProvider.state(logout);

  var dateProperties = ['created','updated','begins','ends','expires_date','due'];

  var propsToDelete = ['created','updated'];

    $httpProvider.interceptors.push('httpInterceptor');
    $httpProvider.interceptors.push('errorInterceptor');

    $httpProvider.defaults.transformResponse.push(function transformRes(res){

        // Unix to Date

        var checker = function r(obj) {
                      
            for (var key in obj) {

                if (typeof obj[key] == "object") {

                    r(obj[key]);
                
                } else if (typeof obj[key] != "function"){

                    if (dateProperties.indexOf(key) > -1){

                        obj[key] = new Date(obj[key]*1000);

                    }

                }
        
            }
            
        };

        if (typeof res == "object") {

            // So we don't process html and other non-json responses. 
            // Since this goes after the built-ins we'll have an object in place of json at this point.

            checker(res);
        }
        return res;
    });

    $httpProvider.defaults.transformRequest.unshift(function transformReq(req){
        // Date to unix
        var checker = function r(obj) {
            for (var key in obj) {
                if (propsToDelete.indexOf(key) > -1){
                    delete obj[key];
                } else {
                    if (typeof obj[key] == "object" && !obj[key] instanceof Date) {
                        r(obj[key]);
                    } else if (typeof obj[key] != "function" && obj[key] !== null){
                        if (dateProperties.indexOf(key) > -1){
                            obj[key] = obj[key].valueOf() / 1000;
                        }
                    }
                }
            }
        };
        checker(req);
        return req;
    });

    // this is for the file viewer module
    $sceDelegateProvider.resourceUrlWhitelist([
      'self',
      'http://*.google.com/**'
    ]);
    
}]);