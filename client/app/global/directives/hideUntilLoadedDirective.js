globals.directive('hideUntilLoaded', [ function () {
        return {
                restrict: 'A',

                scope: {},

                transclude: true,

                template:   '<div class="loadingSpinner"></div>' +
                            '<ng-transclude class="ng-transclude"></ng-transclude>'
        };
}]);