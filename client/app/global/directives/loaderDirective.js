globals.directive("loader", ['$rootScope', '$timeout', function ($rootScope, $timeout) {
   
    return function ($scope, element, attrs) {

        var time, 
        running, 
        fetchDataTimer;

        var $icon = element.find('path');

        $scope.$on("loader_show", function () {

            fetchDataTimer = $timeout(function () {

                // element.addClass('loading');

                $scope.endLoader();
            
            }, 800);
   
        });
   
        $scope.$on("loader_hide", function () {

            $icon.bind("animationiteration webkitAnimationIteration MSAnimationIteration oAnimationIteration", function(){
                
                $icon.unbind();

                // element.removeClass('loading');
            
            });

            $timeout.cancel(fetchDataTimer);
      
        });

        $scope.endLoader = function(){

            $timeout(function () {

                $timeout.cancel(fetchDataTimer);

                $icon.bind("animationiteration webkitAnimationIteration MSAnimationIteration oAnimationIteration", function(){
                
                    $icon.unbind();

                    // element.removeClass('loading');

                });
                        
            }, 10000);

        };
   
    };

}]);