globals.directive('modalBack', ['$window', '$state', '$document', function factory($window, $state, $document) {
 
      return {
 
        restrict   : 'EA',
   
        link: function (scope, element, attrs) {

            var elsewhere = true;

            var clickElsewhere = function() {
            
                if (elsewhere) {
            
                    // go back here

                    $state.go('^');
            
                    $document.off('click', clickElsewhere);
            
                }
            
                elsewhere = true;
            
            };
        
            element.on('click', function(e){

                // catch clicks here as to not close the modal.
        
                elsewhere = false;
            
                $document.off('click', clickElsewhere);
            
                $document.on('click', clickElsewhere);
        
            });
  
        }
 
      };
 
    }]);