// start globals.js

globals.controller('globalsController', ['$scope', '$location', '$stateParams', '$state', '$rootScope', 'globalsFactory','semesterFactory', 'programsFactory', 'usersFactory', 'profileFactory', 'uvFactory', 'analyticsFactory', 'messageCenterService', function($scope, $location, $stateParams, $state, $rootScope, globalsFactory, semesterFactory, programsFactory, usersFactory, profileFactory, uvFactory, analyticsFactory, messageCenterService){
    // globalsController as globals

    var ctrl = this;

    // still need this until all instances of old select2 plugin are removed.
    ctrl.selectOptions = {allowClear:true };

    ctrl.selectedSemester = semesterFactory.selectedSemester; 
    
    semesterFactory.init($stateParams.semester)

    ctrl.semesterList = [];
    ctrl.selectedSemester = null; 
    ctrl.currentSemester = null;
    
    var semesterUpdater = function(){
        // this gets called from the semester factory whenever the selected sem changes. 
        ctrl.selectedSemester = semesterFactory.selectedSemester; 
        ctrl.semesterList = semesterFactory.semesterList;
        ctrl.currentSemester = semesterFactory.currentSemester;
        ctrl.currentSemesterObj = semesterFactory.currentSemesterObj;
        
        // if the selected semester does not match the one in the current route, then update the view
        if($stateParams.semester && $stateParams.semester != semesterFactory.selectedSemester) {
            $state.go('programView', {semester: semesterFactory.selectedSemester});
        }
        
    };

    ctrl.changeSemester = function(selection){
        semesterFactory.setSelectedSemester(selection);
    };
    
    $rootScope.$on('stateUpdate:semester', semesterUpdater);
    
    ctrl.userInfo = {};

    profileFactory.get().then(function(data){

        // find out if the current user is a teacher
        angular.forEach(data.user.roles_users, function(value, key){
            if (value.role_id == 3) {
                data.user.isTeacher = true;
            }
        });
        // sets up user voice user ID
        uvFactory(data.user);
        analyticsFactory.identify(data.user);
        ctrl.userInfo = data.user;

    }, function(error){
        messageCenterService.add('danger', "Something went wrong. Could you try that again?", { timeout: 6000 });
    });

    ctrl.state = {
        userToolsVisible : false,
        toggleUserTools : function(){
            this.userToolsVisible = !this.userToolsVisible;
        }
    };

    $scope.$on('$locationChangeSuccess', function () {
        semesterFactory.init($stateParams.semester);
        ctrl.state.userToolsVisible = false;
    });

    ctrl.teachers = [];

    usersFactory.teachers().then(function (data){
      ctrl.teachers = data.roles;
    }, function (error){
      messageCenterService.add('danger', "Something went wrong. Could you try that again?", { timeout: 6000 });
    });

}]);
