// start profile.js

globals.controller('profileController', ['$scope', '$q', '$timeout', 'profileFactory', 'validatorFactory', 'roleClassesFactory', 'messageCenterService', function ($scope, $q, $timeout, profileFactory, validatorFactory, roleClassesFactory, messageCenterService) {
  
    var ctrl = this;

    ctrl.validating = false;

    ctrl.message = '';

    var time, 
        running, 
        fetchDataTimer;

    ctrl.myProfile = {};
  
    profileFactory.get().then(function (data){

        ctrl.myProfile = data.user;

        ctrl.original = angular.copy(ctrl.myProfile);

        getClasses(data.user.id);

    }, function (error){

        messageCenterService.add('danger', "Something went wrong. Could you try that again?", { timeout: 6000 });

    });

    function getClasses(id){

        roleClassesFactory.studentClasses({id: id}).then(function(data){

            ctrl.studentClasses = data.classes;

        });

        roleClassesFactory.teacherClasses({id: id}).then(function(data){

            ctrl.teacherClasses = data.classes;

        });

    }    

    ctrl.validateEmail = function(){

        var mailToCheck = ctrl.myProfile.email;

        $timeout.cancel(fetchDataTimer);

        $scope.myProfileEditForm.email_address.$setValidity('unique', true);
        
        if (mailToCheck){ 

            ctrl.validating = true;

            fetchDataTimer = $timeout(function () {

                validatorFactory('users', 'email', {validate: mailToCheck})

                .then(function(data){

                    // show validation message here with ctrl.emailInvalid == true if not good
                    // set form invalid if mail isn't valid

                    if(data.success === true) {

                        $scope.myProfileEditForm.email_address.$setValidity('unique', true);

                    } else {

                        $scope.myProfileEditForm.email_address.$setValidity('unique', false);

                    }

                    ctrl.validating = false;

                }, function (error){
                
                    messageCenterService.add('danger', "Something went wrong. Could you try that again?", { timeout: 6000 });

                    ctrl.validating = false;
                
                });
            
            }, 800);
            
        }

    };

    ctrl.cancelEdit = function(){ // untested
    
        ctrl.myProfile = angular.copy(ctrl.original);
    
    };

    ctrl.saveProfile = function(){

        profileFactory.update(ctrl.myProfile).then(function (data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while saving yor info.", { timeout: 6000 });

            } else {

                ctrl.passwordEdit = false;

                ctrl.myProfile.password = '';

                ctrl.myProfile.password_again = '';

                ctrl.message = 'Profile Updated';

                $timeout(function(){

                    ctrl.message = '';

                }, 1500);

            }

        }, function (error){

            messageCenterService.add('danger', "Something went wrong. Could you try that again?", { timeout: 6000 });

        });
        
    };

}]);