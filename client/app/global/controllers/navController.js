// Start nav.js

globals.controller ('navController', ['$scope','$q','$state','$stateParams','programsFactory','globalsFactory', 'messageCenterService', function($scope, $q, $state, $stateParams, programsFactory, globalsFactory, messageCenterService) {

    var ctrl = this;
 
    ctrl.programsListOpen = false;

    ctrl.toggleProgramsList = function(){
 
        ctrl.programsListOpen = !ctrl.programsListOpen; 
  
    };
    
    ctrl.programList = [];

    programsFactory.query().then(function (data){

        ctrl.programList = data.programs;

    }, function (error){

        messageCenterService.add('danger', "Something went wrong. Could you try that again?", { timeout: 6000 });

    });

}]);