// start extensionsCodesControllers.js

extcodesModule.controller('extensioncodeCreateController', ['$scope', '$q', '$timeout', '$stateParams', '$state', 'roleClassesFactory', 'extensioncodesFactory', 'usersFactory', 'messageCenterService', function ($scope, $q, $timeout, $stateParams, $state, roleClassesFactory, extensioncodesFactory, usersFactory, messageCenterService) {

    var ctrl = this;
    
    var currentSemester = ctrl.currentSemester; 
    // to get this loaded in time... janky, but otherwise would need nested promises in loadXXX functions
    
    ctrl.mode = 'create';

    ctrl.modal = true; 

    ctrl.message = '';

    ctrl.studentList = [];

    ctrl.prgList = [];

    ctrl.studentAssgns = [];

    ctrl.item = {};

    usersFactory.students().then(function (data){

        ctrl.studentList = data.roles;

    }, function (error){

        messageCenterService.add('danger', "Something went wrong while looking up the student list. Try reloading the page.", { timeout: 6000 });

    });

    ctrl.loadAssignments = function(item){

        ctrl.item.assignment_id = '';

        roleClassesFactory.studentClasses({id: item}).then(function(data){

            var classes =  data.classes;

            var result = [];

            for (var a = classes.length - 1; a >= 0; a--) {
                
                for (var b = classes[a].assignments.length - 1; b >= 0; b--) {
                    
                    result.push(classes[a].assignments[b]);
                
                }

            }

            ctrl.studentAssgns = result;

        },

        function(error){

            messageCenterService.add('danger', "Something went wrong while looking up the assignment list. Try reloading the page.", { timeout: 6000 });

        });

    };
  
    ctrl.createItem = function(){
  
        extensioncodesFactory.create(ctrl.item).then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while saving the extension code.", { timeout: 6000 });

            } else {

                ctrl.message = 'Extension Code Created';

                $timeout(function(){

                    $state.go('^', {reload:true});

                }, 1500);

            }

            $scope.$parent.extCodes.loadList();

        },

        function(error){

            messageCenterService.add('danger', "Something went wrong while creating this extension code. Try again, sorry. I hate myself.", { timeout: 6000 });

        });

    };
  
}]);