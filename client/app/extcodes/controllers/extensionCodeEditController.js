// start extensionsCodesControllers.js

extcodesModule.controller('extensioncodeEditController', ['$scope', '$q', '$timeout', '$stateParams', '$state', 'extensioncodesFactory', 'usersFactory', 'roleClassesFactory', 'messageCenterService', function ($scope, $q, $timeout, $stateParams, $state, extensioncodesFactory, usersFactory, roleClassesFactory, messageCenterService) {

    var ctrl = this;

    var currentSemester = ctrl.currentSemester; // to get this loaded in time... janky, but otherwise would need nested promises in loadXXX functions

    ctrl.extList = [];

    ctrl.displayList = [];
    
    ctrl.studentList = [];

    ctrl.mode = 'edit';

    ctrl.modal = true; 

    ctrl.deleting = false;

    ctrl.message = '';

    ctrl.studentAssgns = [];

    ctrl.item = {};

    ctrl.toggleDelete = function(){

        ctrl.deleting = !ctrl.deleting;

    };

    ctrl.deleteItem = function(){
  
        extensioncodesFactory.remove($stateParams).then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while deleting the code.", { timeout: 6000 });

            } else {
                
                ctrl.message = 'Extension Code Deleted';

                $timeout(function(){

                    $state.go('^', {reload:true});

                }, 1500);
            }

            $scope.$parent.extCodes.loadList();

        },

        function(error){

            messageCenterService.add('danger', "Something went wrong while deleting the extension code. Try again.", { timeout: 6000 });

        });

    };
    
    ctrl.updateItem = function(){
  
        extensioncodesFactory.update($stateParams, ctrl.item).then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while saving the code.", { timeout: 6000 });

            } else {

                ctrl.message = 'Extension Code Updated';

                $timeout(function(){

                    ctrl.message = '';

                }, 1500);

            }

            $scope.$parent.extCodes.loadList();

        },

        function(error){

            messageCenterService.add('danger', "Something went wrong while updating this extension code. Try again!", { timeout: 6000 });

        });

    };

    extensioncodesFactory.get($stateParams).then(function(data){

        if (data._error.code == 1) {

            messageCenterService.add('danger', "Something went wrong while retrieving the extension code.", { timeout: 6000 });

        } else {

            ctrl.item = data.extensionCode;

            ctrl.loadAssignments(data.extensionCode.student_id);

        }

    },

    function(error){

        messageCenterService.add('danger', "Something went wrong while looking up the extension codes. Try reloading the page.", { timeout: 6000 });

    });

    usersFactory.students().then(function (data){

        ctrl.studentList = data.roles;

    }, function (error){
    
        messageCenterService.add('danger', "Something went wrong while looking for the students. Try reloading the page, I dunno.", { timeout: 6000 });
    
    });

    ctrl.loadAssignments = function(item){

        ctrl.item.assignment_id = '';

        roleClassesFactory.studentClasses({id: item}).then(function(data){

            var classes =  data.classes;

            var result = [];

            for (var a = classes.length - 1; a >= 0; a--) {
                
                for (var b = classes[a].assignments.length - 1; b >= 0; b--) {
                    
                    result.push(classes[a].assignments[b]);
                
                }

            }

            ctrl.studentAssgns = result;

        },

        function(error){

            messageCenterService.add('danger', "Something went wrong while looking up the assignments. Try reloading the page.", { timeout: 6000 });

        });

    };
      
}]);