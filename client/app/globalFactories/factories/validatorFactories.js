// start validatorFactories.js

globals.factory('validatorFactory', ['$http', '$resource', '$q', function ($http, $resource, $q) {
  
  var validatorResource = $resource('api/:collection/validate/:field.json',

    {

      collection : '@collection', 

      field : '@field' 
    
    },
    
    {

      'validate': {method: 'POST'} 

    });

  var factory = function (col, fld, payload) {
    
      // validate some data
    
      var deferred = $q.defer();
    
      validatorResource.validate({collection : col, field : fld}, payload, 
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });
    
      return deferred.promise;
         
  };

  // add methods for getting and setting selected semester
  
  return factory;

}]);