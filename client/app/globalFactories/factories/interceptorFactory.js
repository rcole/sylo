globals.factory('httpInterceptor', ['$q', '$rootScope', function ($q, $rootScope) {

    var numLoadings = 0;

    return {
        request: function (request) {
            // Show loader
            
            numLoadings++;
            
            $rootScope.$broadcast("loader_show");
            
            return request || $q.when(request);

        },
        response: function (response) {

            if ((--numLoadings) === 0) {
                // Hide loader
            
                $rootScope.$broadcast("loader_hide");
            }

            return response || $q.when(response);

        },
        responseError: function (response) {

            if ((--numLoadings) === 0) {
                // Hide loader
             
                $rootScope.$broadcast("loader_hide");
            
            }

            return $q.reject(response);
        }
    };
}]);

globals.factory('errorInterceptor', ['$q', '$rootScope', function ($q, $rootScope) {

    var isJson = function(url){
        var splits = url.split('.');
        return splits[splits.length-1] === 'json';
    };
    
    return {
        response: function (response) {
            if(response.data._error !== undefined){
                if(isJson(response.config.url)){
                    var errObj;
                    if(response.data._error._error !== undefined){
                        errObj = response.data._error._error;
                    } else {
                        errObj = response.data._error;
                    }

                    if(errObj.code === 1 && errObj.status >= 450){
                        // do a heap error log
                        analyticsFactory.logError('error: 1 :' + response.config.url);
                        console.warn('error: 1 :', errObj.code, errObj.message, errObj.status, response.config.url);
                        return $q.reject(response);
                    }
                }
            } else if(isJson(response.config.url)){
                // do a heap error log
                analyticsFactory.logError('error: 2 :' + response.config.url);
                console.warn('error: 2 :', response.data);
                return $q.reject(response);
            }

            return response || $q.when(response);

        },
        responseError: function (response) {
            analyticsFactory.logError('error: 3 :' + response.config.url);
            console.log('error: 3 :', response);
            // do a heap error log
            return $q.reject(response);
        }
    };
}]);
