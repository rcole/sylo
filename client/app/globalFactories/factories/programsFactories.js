// programsFactory.js

globals.factory('programsFactory', ['$http','$resource','$q', function($http, $resource, $q) {
  

  var programsResource = $resource('api/programs.json', {},
    
    { 
    
      'query': {method: 'GET', isArray: false, cache: false},

      'create': {method: 'POST'} 
    
    });


  var programResource = $resource('api/program/:program.json', {program: '@program'},
    
    {
    
      'get': {method: 'GET'},
    
      'update': {method: 'POST'},
    
      'remove': {method: 'DELETE'} 
    
    });

  var authorizationsResource = $resource('api/programauthorizations.json',

    {},

    {

      'add': {method: 'POST'},

      'remove': {method: 'POST'} 

    });


  var factory = {

    query : function () {

      var deferred = $q.defer();

      programsResource.query({},
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    },

    get : function (params) {

      var deferred = $q.defer();

      programResource.get({program: params.program},
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    },

    create : function (payload) {

      var deferred = $q.defer();

      programsResource.create(payload, 
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    },

    update : function (params, payload) {

      payload._method = 'put';

      var deferred = $q.defer();

      programResource.update({program: params.program}, payload, 
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    },

    remove : function (params) {

      var deferred = $q.defer();

      var payload = { _method: 'delete' };

      programResource.remove({program: params.program}, payload, 
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    },
    addAuth : function (payload) {

      payload._method = 'put';

      var deferred = $q.defer();

      authorizationsResource.add({}, payload, 
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    },
    removeAuth : function (payload) {

      payload._method = 'delete';

      var deferred = $q.defer();

      authorizationsResource.remove({}, payload, 
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    }

  };
    
  return factory;

}]);