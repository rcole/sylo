globals.factory('globalsFactory', [ function () {

    // this is mostly a collection of rando util type functions for use globally
    // will probably split this up eventually
    
    var factory = {

      // takes a program or class slug and strips out dashes
      slugCleaner : function(slug){

        var clean;
        
        if(slug) {

          clean = slug.replace(/-/g, " ");

        } else {

          clean = "";

        }
     
        return clean;
     
      },

      // could go into the users factory automatically on query or as middleware in type builders
      makeFullName : function(data){
     
        angular.forEach(data.users, function(value, key){
     
          value.full_name = value.first_name + ' ' + value.last_name;
     
        });
     
      }
    
    };

    return factory;
}]);