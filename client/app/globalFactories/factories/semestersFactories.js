    // start semesterf.js

globals.factory('semesterFactory', ['$http', '$resource', '$q', '$rootScope', 'messageCenterService', function ($http, $resource, $q, $rootScope, messageCenterService) {
        
        // currentSemester 
        // selectedSemester
        // semesterList
        // setSemester() || 'current'
            // ==> selectCurrentSemester()
        // get 
        // create 
        // update
        // remove 
        // query 
        // init
            // query()
                // puts to semesterList
            // get and set current semester , or null
            // sets selectedSemester based on arg
        
        // order of sem loading
            // stateparams
            // current
            // last ended
            
    var semestersResource, semesterResource, currentSemesterResource, f = {};

    semestersResource = $resource('api/semesters.json', {},
        {
            'query': { method: 'GET', isArray: false, cache: false },
            'create': {method: 'POST'} 
        });

    semesterResource = $resource('api/semester/:semester.json', {semester:'@semester'},
        {
            'get': {method: 'GET'},
            'update': {method: 'POST'},
            'remove': {method: 'POST'}
        }); 

    currentSemesterResource = $resource('api/semesters/current.json', {}, 
        {
            'query': {method: 'GET', isArray: false, cache: true} 
        });

    f.init = function(semesterParam){
        async.parallel([
            function(cb) {
                f.query()
                    .then(function(data){
                        cb(null, data);
                    }, function(err){
                        cb(err, null);
                    });
            },
            function(cb) {
                f.getCurrentSemester()
                    .then(function(data){
                        cb(null, data);
                    }, function(err){
                        cb(err, null);
                    });
            }],
            function(err, results) {
                if (err){
                    messageCenterService.add('danger', "Error retrieving semesters. Could you try that again?", { timeout: 6000 });
                } else {
                    f.semesterList = results[0].semesters;
                    f.currentSemester = results[1].semester.title;
                    f.currentSemesterObj = results[1].semester;
                    if(semesterParam){
                        f.selectedSemester = semesterParam;
                    } else if(f.currentSemester){
                        f.selectedSemester = f.currentSemester;
                    } else {
                        f.selectedSemester = f.semesterList[f.semesterList.length - 1].title;
                    }
                    $rootScope.$broadcast('stateUpdate:semester');
                }
            });
    };
    
    f.semesterList = [];
    
    f.selectedSemester = null;
    
    f.setSelectedSemester = function(s){
        
        if(s === 'current'){
             
        } else {
            f.selectedSemester = s;
        }
        
        $rootScope.$broadcast('stateUpdate:semester');
    
    };
    
    f.query = function () {
    
        // returns list of all semesters
        
        var deferred = $q.defer();
        
        semestersResource.query({}, 
            function(resp) {
            deferred.resolve(resp);
            },
            function(resp){
            deferred.reject(resp);
            });
        
        return deferred.promise;
    
    };
   
    f.get = function (params) {
 
        var deferred = $q.defer();
    
        semesterResource.get({semester: params.semester},
            function(resp) {
            deferred.resolve(resp);
            },
            function(resp){
            deferred.reject(resp);
            });
    
        return deferred.promise;
 
    };
 
    f.create = function (payload) {

        var deferred = $q.defer();
    
        semestersResource.create(payload, 
            function(resp) {
            deferred.resolve(resp);
            },
            function(resp){
            deferred.reject(resp);
            });

        return deferred.promise;
 
    };
 
    f.update = function (params, payload) {
        var deferred = $q.defer();
        payload._method = 'put';
    
        semesterResource.update({semester: params.semester}, payload, 
            function(resp) {
                deferred.resolve(resp);
            },
            function(resp){
                deferred.reject(resp);
            });

        return deferred.promise;
 
    };
 
    f.remove = function (params) {
        var deferred = $q.defer();
        var payload = { _method: 'delete' };
   
        semesterResource.remove({semester: params.semester}, payload, 
            function(resp) {
                deferred.resolve(resp);
            },
            function(resp){
                deferred.reject(resp);
            });

        return deferred.promise; 
    };

        // returns currently active semester
    f.getCurrentSemester = function(){   
        var deferred = $q.defer();
    
        currentSemesterResource.query({},
            function(resp) {
                deferred.resolve(resp);
            },
            function(resp){
                deferred.reject(resp);
            });
    
        return deferred.promise;
   
    };
  
    return f;

}]);