// usersFactories.js

globals.factory('usersFactory', ['$http', '$resource', '$q', 'messageCenterService', function ($http, $resource, $q, messageCenterService) {
  
  var usersResource = $resource('api/users.json', {},
   
    { 
      
      'query' : {method: 'GET', isArray: false},

      'create': {method: 'POST'} 

    });

  var userResource = $resource('api/user/:id.json', {id: '@id'},
    
    {
    
      'get': {method: 'GET', isArray: false, cache: true},      
    
      'update': {method: 'POST'},
    
      'remove': {method: 'POST'}
    
    });

  var roleResource = $resource('api/role/:role.json', {role: '@role'},
   
    { 
      
      'query' : {method: 'GET', isArray: false}

    });


  var factory = {

    query : function () {
    
      var deferred = $q.defer();
    
      usersResource.query({},
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });
    
      return deferred.promise;
    
    },
    
    get: function (params) {

      var deferred = $q.defer();

      // this is to allow for either only an ID string or an object containing an ID property to call the factory

      var ID;
      
      if (params.hasOwnProperty('user')) {

        ID = params.user;

      } else {

        ID = params;

      }

      userResource.get({id: ID},
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;
  
    },
  
    create : function (payload) {
  
      var deferred = $q.defer();
  
      usersResource.create(payload, 
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;
  
    },
  
    update : function (params, payload) {
  
      payload._method = 'put';
  
      var deferred = $q.defer();
  
      userResource.update({id: params.user}, payload, 
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;
  
    },
  
    remove : function (params) {
  
      var payload = { _method: 'delete' };

      var deferred = $q.defer();
  
      userResource.remove({id: params.user}, payload, 
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;
  
    },

    teachers : function(){

      var deferred = $q.defer();
  
      roleResource.query({role: 'teacher'}, 
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    },

    programLeaders : function(){

      var deferred = $q.defer();
  
      roleResource.query({role: 'prgLead'}, 
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    },

    admins : function(){

      var deferred = $q.defer();
  
      roleResource.query({role: 'admin'}, 
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    },

    students : function(){

      var deferred = $q.defer();
  
      roleResource.query({role: 'student'}, 
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    },

    academicServices : function(){

      var deferred = $q.defer();
  
      roleResource.query({role: 'acdServ'}, 
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    },

    externalVerifier : function(){

      var deferred = $q.defer();
  
      roleResource.query({role: 'extVer'}, 
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    },

    studentRecords : function(){

      var deferred = $q.defer();
  
      roleResource.query({role: 'studRec'}, 
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;
    
    }

  };


  function buildUserLists(callback){

    listsBuilt = true;

    factory.query().then(function (data){
  
      angular.forEach(data.users, function(value, key){

        var rolesForAUser = [];

        angular.forEach(value.roles_users, function(value, key){

          // pack all roles for each user into an array
          rolesForAUser.push(value.role_id);

        });
        
        // check array for each role ID and push it into the correct array list.
        if(rolesForAUser.indexOf("1") != -1) admins.push(value);
        if(rolesForAUser.indexOf("2") != -1) programLeaders.push(value);
        if(rolesForAUser.indexOf("3") != -1) teachers.push(value);
        if(rolesForAUser.indexOf("4") != -1) academicServices.push(value);
        if(rolesForAUser.indexOf("5") != -1) externalVerifier.push(value);
        if(rolesForAUser.indexOf("6") != -1) studentRecords.push(value);
        if(rolesForAUser.indexOf("7") != -1) students.push(value);
    
      });
      
      return callback;
  
    }, function (error){
  
      messageCenterService.add('danger', "Something went wrong. Could you try that again?", { timeout: 6000 });
  
    });

  }

  return factory;

}]);