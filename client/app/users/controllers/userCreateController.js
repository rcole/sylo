// usersControllers.js

usersModule.controller('userCreateController', ['$scope', '$state', '$timeout', 'usersFactory', 'messageCenterService', 'userRoles', function ($scope, $state, $timeout, usersFactory, messageCenterService, userRoles) {
  
  console.log('userCreateController instantiated');

  var ctrl = this;

    ctrl.mode = 'create';

    ctrl.modal = true; 

    ctrl.message = '';

    ctrl.passwordEdit = true;

    ctrl.rolesList = userRoles;

    ctrl.createItem = function(){

        ctrl.item.roles_users = [];

        for (var i = ctrl.roles.length - 1; i >= 0; i--) {

            var temp = {
            
                role_id: ctrl.roles[i]
            
            };
            
            ctrl.item.roles_users.push(temp);
        
        }
  
        usersFactory.create(ctrl.item).then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while saving the user.", { timeout: 6000 });

                console.log(data);

            } else {

                ctrl.message = 'User Created';

                $timeout(function(){

                    $state.go('^', {reload:true});

                }, 1500);

            }

            $scope.$parent.users.loadList();

        },

        function(error){

            console.log(error);

        });

    };

}]);