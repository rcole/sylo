// usersControllers.js

usersModule.controller('usersImportController', ['$scope', 'usersFactory', 'messageCenterService', function ($scope, usersFactory, messageCenterService) {
  
  console.log('usersImportController instantiated');

  var ctrl = this;

  var toInject;

  ctrl.checkJson = function(){

    ctrl.jsonStatus = '';
    
    try {
    
        var o = JSON.parse(ctrl.jsonText);

        // Handle non-exception-throwing cases:
        // Neither JSON.parse(false) or JSON.parse(1234) throw errors, hence the type-checking,
        // but... JSON.parse(null) returns 'null', and typeof null === "object", 
        // so we must check for that, too.
    
        if (o && typeof o === "object" && o !== null) {

          toInject = o;

          ctrl.jsonStatus = 'Valid';

          console.log(toInject);
        
        }
    
    }
    
    catch (e) { 

      console.log(e);

      ctrl.jsonStatus = 'Invalid';

    }

  };

  ctrl.importAsJson = function(){

    ctrl.status = [];

    // for mass importing, pareses text to json and imports

    async.each(toInject.users, function(file, callback) {

      // Perform operation on file here.

      ctrl.status.push('Processing user: ' + file.email);

      console.log('Processing user ' + file.email);

      usersFactory.create(file)

      .then(function(data){

        console.log('data: ', data);

          if (data._error.code == 1) {

            ctrl.status.push('ERROR: ' + file.email);

              callback(data._error);

          } else {

              console.log('user processed');

              ctrl.status.push('User processed: ' + file.email);
    
              callback(); 

          }

      },
      function(error){
        console.log(error);
      });
      
    }, function(err){
      
        if( err ) {

          // One of the iterations produced an error.
          
          // All processing will now stop.

          console.log('Something went wrong', err);
       
        } else {
        
          console.log('All files have been processed successfully');

          ctrl.status.push('All users have been imported successfully');
       
        }
    
    });

  };

}]);