// roleClassesFactories.js

usersModule.factory('roleClassesFactory', ['$http', '$resource', '$q', function($http, $resource, $q) {

  var teacherResource = $resource('api/teacher/:id/classes.json', {id: '@id'},
    {
      'get': {
        method: 'GET',
        isArray: false,
        cache: true
      }
    });


  var studentResource = $resource('api/student/:id/classes.json', {id: '@id'},
    {
      'get': {
        method: 'GET',
        isArray: false,
        cache: true
      }
    });


  var factory = {

    teacherClasses: function(params) {

      var deferred = $q.defer(),
          ID;

      if (params.id !== undefined) {
        ID = params.id;
      } else {
        ID = params;
      }

      teacherResource.get({id: ID },
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp) {
          deferred.reject(resp);
        });

      return deferred.promise;

    },

    studentClasses: function(params) {

      var deferred = $q.defer(),
          ID;

      if (typeof(params.id) !== undefined) {
        ID = params.id;
      } else {
        ID = params;
      }

      studentResource.get({id: ID },
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    }

  };

  return factory;

}]);