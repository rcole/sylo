// usersRolesFilter.js

usersModule.filter('makeUserRole', ['userRoles', function(userRoles) {

    return function(input) {

        for (var i = userRoles.length - 1; i >= 0; i--) {
    
            if (userRoles[i].role_id == input) {
        
                return userRoles[i].role_title;

            }
            
        }

    };

}]);


usersModule.filter('makeUserStatus', ['userStatus', function(userStatus) {

    return function(input) {

        for (var i = userStatus.length - 1; i >= 0; i--) {
    
            if (userStatus[i].status_id == input) {
        
                return userStatus[i].status_title;

            }
            
        }

    };

}]);