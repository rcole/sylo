// teaModule.js Starts here 

var teacherApp = angular.module('teacherApp' , [
  'globals',
  'semesterProgramModule',
  'semesterProgramClassModule',
  'semestersModule',
  'programsModule',
  'extcodesModule',
  'myclassesModule',
  'usersModule',
  'modulesModule',
  'flow',
  'MessageCenterModule',
  'uservoiceModule',
  'analyticsModule',
  'submissionsListModule'
]);

teacherApp.config(['$stateProvider','$urlRouterProvider','$locationProvider',function($stateProvider, $urlRouterProvider, $locationProvider){
    
  $urlRouterProvider.otherwise("/myclasses");
  $locationProvider.html5Mode(false);
    
}]);