uservoiceModule.factory('analyticsFactory', [function() {    

    var isLocal = window.location.hostname === 'localhost' || window.location.hostname === 'submiststaging.praguecollege.cz'; // add staging url here as well

    if(isLocal){
        heap.load("2094147294");
    } else {
        heap.load("912591352");
    }

    factory = {
        identify: function(profile){
            if(!isLocal){
                heap.identify({
                    name: profile.first_name + ' ' + profile.last_name,
                    syloId: profile.id,
                    userRole: profile.roles_users[0].role_id,
                    email: profile.email
                });
            }
        },

        uploadErrors : function(report){
            if(!isLocal) heap.track('Upload Errors', report);
        },
        uploadSuccess : function(report){
            if(!isLocal) heap.track('Upload Success', report);
        },
        logError : function (report) { 
            if(!isLocal) heap.track('Ajax Error', report);
         }
    };

    return factory;
    
}]);