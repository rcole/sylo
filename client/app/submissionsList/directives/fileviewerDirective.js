submissionsListModule.directive('fileviewer', ['ngModalDefaults', '$sce', '$location', function(ngModalDefaults, $sce, $location) {
      
      return {
      
        restrict: 'E',

        require: '^submissionList',
      
        scope: {
      
          show: '=',
            
          onClose: '&?',

          subsArray: '=',

          subIndex: '='
      
        },
      
        replace: true,
      
        transclude: true,
      
        link: function(scope, element, attrs) {

          var webNativeFiles = ['jpeg','jpg','png','bmp','gif','tiff','svg','mp3','mpeg','wav','ogg','mp4'];

          var setupCloseButton, setupStyle;

          scope.dialogTitle = '';

          scope.pathToFile = '';

          scope.fileViewerPath = function(){

            var activeSub = scope.subsArray[scope.subIndex];

            var fe = activeSub.name.split('.').pop().toLowerCase();

            if(webNativeFiles.indexOf(fe) >= 0){

              scope.pathToFile = $location.$$protocol + '://' + $location.$$host + '/uploads/' + activeSub.path + activeSub.name;

            } else {

              scope.pathToFile = 'http://drive.google.com/viewer?url=' + $location.$$protocol + '://' + $location.$$host + '/uploads/' + activeSub.path + activeSub.name + '&embedded=true';

            }

          };
      
          setupCloseButton = function() {

            scope.closeButtonHtml = $sce.trustAsHtml(ngModalDefaults.closeButtonHtml);
      
            return scope.closeButtonHtml;
      
          };
      
          setupStyle = function() {
      
            scope.dialogStyle = {};
      
            if (attrs.width) {
      
              scope.dialogStyle.width = attrs.width;
      
            }
      
            if (attrs.height) {

                scope.dialogStyle.height = attrs.height;
      
                return scope.dialogStyle.height;
      
            }
      
          };
      
          scope.hideModal = function() {

            scope.show = false;
      
            return scope.show;
      
          };
      
          scope.$watch('show', function(newVal, oldVal) {
      
            if (newVal && !oldVal) {

              // on

              scope.fileViewerPath();
      
              document.getElementsByTagName("body")[0].style.overflow = "hidden";
      
            } else {

              // off
      
              document.getElementsByTagName("body")[0].style.overflow = "";
      
            }
      
            if ((!newVal && oldVal) && (scope.onClose !== null)) {

              // off 

              scope.pathToFile = '';
      
              return scope.onClose();
      
            }
      
          });
      
          setupCloseButton();
      
          return setupStyle();
      
        },
      
        templateUrl: 'tpl/fileviewer.tpl.html'
      
      };

    }

  ]);