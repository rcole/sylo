submissionsListModule.directive('submissionList', ['downloadFactory', function (downloadFactory) {

    return {
        
        restrict: "A",

        scope: {
            list: '=',
            displayAs: '@',
            loaded: '='
        },

        replace: true,

        templateUrl: 'tpl/subList.tpl.html',

        controller: function($scope){

            var unopenableFiles = ['rar','zip','gzip','psd','ai','indd','mov','tar'];

            $scope.subToDownload = {};

            $scope.modalOpen = false;

            $scope.activeSubIndex = '0';

            $scope.showModal = function(index){

                $scope.modalOpen = true;

                $scope.activeSubIndex = index;

            };

            $scope.displayList = [].concat($scope.list);

            $scope.groupByStudent = function() {

                // nothing yet

            };

            $scope.groupByAssignment = function() {

                // nothing yet

            };

            $scope.groupByClass = function() {

                // nothing yet

            };

            $scope.testIfViewable = function(name){

                var fe = name.split('.').pop().toLowerCase();

                if(unopenableFiles.indexOf(fe) >= 0){

                    return true;

                } else {

                    return false;

                }

            };

            $scope.downloadGroup = function(){

                var arr = _.values($scope.subToDownload);

                _.pull(arr, "");

                downloadFactory.download(arr).then(function(data){

                    console.log(data);

                }, function(error){
                
                    console.log('bulk download error: ' + error);
                
                });

            };

        },

        link: function (scope, elem, attrs, ctrl) {

    
        }

    };

}]);