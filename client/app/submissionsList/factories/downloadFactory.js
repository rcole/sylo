// submissions list module

submissionsListModule.factory("downloadFactory", ['$http', '$resource', '$q', function ($http, $resource, $q) {

  var downloadGroupResource = $resource('api/download/submissions.json', {},
    
    {

      'download': {method: 'POST'} 

    });


  var factory = {
    
    download : function (downloadList) {
      // this should be passed an array of submission IDs
 
      var deferred = $q.defer();
 
      downloadGroupResource.download({submissions_to_download: downloadList},
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });
 
      return deferred.promise;
 
    }

  };
  
  return factory;

}]);