submissionsListModule.provider("ngModalDefaults", function() {

    return {

      options: {

        closeButtonHtml: "<span class='ng-modal-close-x'>X</span>"

      },

      $get: function() {

        return this.options;

      },

      set: function(keyOrHash, value) {

        var k, v, _results;

        if (typeof keyOrHash === 'object') {

          _results = [];

          for (k in keyOrHash) {

            v = keyOrHash[k];

            _results.push(this.options[k] = v);

          }

          return _results;

        } else {

          this.options[keyOrHash] = value;

          return this.options[keyOrHash];

        }

      }

    };

  });