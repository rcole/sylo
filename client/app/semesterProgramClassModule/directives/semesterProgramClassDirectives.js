// start semesterProgramClassDirectives.js

semesterProgramClassModule.directive('assignmentBlock', ['globalsFactory', 'usersFactory', 'classAssignmentFactory', '$stateParams', function(globalsFactory, usersFactory, classAssignmentFactory, $stateParams) { 

  // not done yet
  return {
    restrict: 'A',
    templateUrl: "app/webroot/tpl/part.class.assignment.block.tpl.html",
    scope: {
      object: '=',
      currentsemester: '=',
      submissions: '='
    },
    link : function (scope, elem, attrs) {

      // Defines percentages of submissions in this order [subs, late, remaining]
      scope.bar = [];

      scope.state = $stateParams;
      scope.factory = classAssignmentFactory;
      scope.cleanSlug = globalsFactory.slugCleaner;
      scope.linker = globalsFactory.clsLinker; // calls linking methods in globalsFactory for building links to pages
      scope.original = angular.copy(scope.object);
      scope.selectedForDownload = scope.assignmentSubsGrid.selectedItems; // this needs to get a list of download links maybe

      scope.manyToDownload = function(){
        if(scope.selectedForDownload.length > 1){
          return true;
        } else {
          return false;
        }
      };

      scope.downloadSubmission = function(){
        console.log('do downloady stuff here');
      };

      scope.edit = {
        viewingSubs: false, // controls subs accordion
        editing : false, // controls the flip
        isNew : false, // controls edit button actions and text
        deleting : false,
        toggle : function(){
          this.editing = !this.editing;
          this.deleting = false;
          this.viewingSubs = false;
        },
        toggleSubs : function(){
          this.viewingSubs = !this.viewingSubs;
          scope.assignmentSubsGrid.selectAll(false);
        },
        toggleDeleting : function(){
          this.deleting = !this.deleting;
        },
        cancelEdit : function(){ // untested
          scope.object = angular.copy(scope.original);
        },
        canSave : function() {
          return scope.assignmentEditForm.$valid && !angular.equals(scope.original, scope.object);
        }
      };
    },

    controller : function($scope, $attrs){

      // TODO: not right data yet, 
      // this might go away if the data will come from the assignment item json itself
      $scope.assignmentSubsList = usersFactory.query()
        .then(function (data){

          // add a fullname property for the grid to display
          globalsFactory.makeFullName(data); 

          return data.users;
        }, 
        function (error){
          console.log('Error');
          console.log(error);
        }, 
        function(progress){
          console.log('progressBack');
          console.log(progress);
        });

      // TODO: add correct data 
      $scope.assignmentSubsGrid = { 
        data: 'assignmentSubsList',
        enableSorting: true,
        headerRowHeight: 30,
        selectedItems: [],
        headerClass: 'subGridHeader',
        rowHeight: 30,
        columnDefs: [
          {field:'full_name', displayName:'Student'}, 
          {field:'email', displayName:'Email'}, 
          {field:'submission_date', displayName: 'Sub. Date'},
          {field:'subs', displayName:'download', cellTemplate: '<div><div class="ngCellText"><a ng-click="downloadSubmission(); $event.stopPropagation()"><span>download</span></a></div></div>'}
        ]
      };

    }
  };
}]);