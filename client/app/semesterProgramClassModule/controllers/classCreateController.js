// start classesControllers.js

semesterProgramModule.controller('prgClassCreateController', ['$scope', '$q', '$stateParams', '$state', '$timeout', 'classesFactory', 'modulesFactory', 'usersFactory', 'programsFactory', 'messageCenterService', function ($scope, $q, $stateParams, $state, $timeout, classesFactory, modulesFactory, usersFactory, programsFactory, messageCenterService) {

    var ctrl = this;

    var localProgramList = [];

    ctrl.mode = 'create';

    ctrl.modal = true; 

    ctrl.message = '';

    ctrl.moduleList = [];

    ctrl.teachers = [];
    
    var currentTime = new Date().getTime();

    ctrl.onlyFuture = function(d) {
        return d.getTime() > currentTime;
    };

    ctrl.createItem = function(){

        if(ctrl.teachers.length > 0){

            ctrl.item.teacher_authorizations = [];

            for (var j = ctrl.teachers.length - 1; j >= 0; j--) {

                var temp = {
                
                    teacher_id: ctrl.teachers[j],

                    semester_id: $scope.$parent.globals.currentSemesterObj.id
                
                };
                
                ctrl.item.teacher_authorizations.push(temp);
            
            }

            ctrl.item.semester_id = $scope.$parent.globals.currentSemesterObj.id;

            for (var i = localProgramList.length - 1; i >= 0; i--) {
              
                if(localProgramList[i].slug == $stateParams.program){

                    ctrl.item.program_id = localProgramList[i].id;
                    
                }
          
            }

            classesFactory.create($stateParams, ctrl.item).then(function(data){

                ctrl.message = 'Class Created';

                $timeout(function(){

                    $state.go('^', {reload:true});

                }, 1500);

                $scope.$parent.prgClasses.loadList();

            },

            function(error){
                console.warn(error);
                messageCenterService.add('danger', "Something went wrong while saving the class. Don't forget to add a teacher!", { timeout: 6000 });

            });

        } else {
            
            messageCenterService.add('warning', "Please select a teacher!", { timeout: 6000 });

        }

    };

    modulesFactory.query($stateParams).then(function (data){

        ctrl.moduleList = data.modules;

    }, function (error){
        console.warn(error);
        messageCenterService.add('danger', "Something went wrong while getting the module list. Try reloading the page.", { timeout: 6000 });

    });

    programsFactory.query().then(function (data){

        localProgramList = data.programs;

    }, function (error){
        console.warn(error);
        messageCenterService.add('danger', "Something went wrong while looking up the list of programs. Try reloading the page.", { timeout: 6000 });

    });

}]);