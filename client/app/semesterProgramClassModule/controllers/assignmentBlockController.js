// semesterProgramClassAssignmentController.js

semesterProgramClassModule.controller('classAssignmentBlockController', ['$scope', '$q', '$stateParams', 'classAssignmentFactory', 'usersFactory', 'messageCenterService', function ($scope, $q, $stateParams, classAssignmentFactory, usersFactory, messageCenterService) {

    var ctrl = this;

    ctrl.viewingSubs = false;

    ctrl.selectedForDownload = [];

    ctrl.toggleSubs = function(){
 
        ctrl.viewingSubs = !ctrl.viewingSubs;
 
        ctrl.selectedForDownload = [];
 
    };

    ctrl.manyToDownload = function(){
 
        if(ctrl.selectedForDownload.length > 1) return true;
 
        return false;
  
    };

    ctrl.downloadSubmission = function(){
 
        console.info('do downloady stuff here');
 
    };

    ctrl.assignmentSubsGrid = [
        {
            id: "1",
            student_id: "70",
            file: "submission1.pdf",
            receipt: "8369hd",
            created: "1414510836",
            updated: "1414510836"
        },
        {
            id: "2",
            student_id: "71",
            file: "submission2.pdf",
            receipt: "8sd9hd",
            created: "1414510836",
            updated: "1414510836"
        },
        {
            id: "3",
            student_id: "73",
            file: "submission3.pdf",
            receipt: "8369dw",
            created: "1414510836",
            updated: "1414510836"
        },
        {
            id: "4",
            student_id: "72",
            file: "submission14.pdf",
            receipt: "836sdhd",
            created: "1414510836",
            updated: "1414510836"
        }
    ];

    ctrl.assignmentSubsDisplayList = [].concat(ctrl.assignmentSubsGrid);

    // TODO: get submissions data somehow.

    // $scope.assignmentSubsList = classAssignmentFactory.query()
    
    //   .then(function (data){
    
    //     // add a fullname property for the grid to display
    //     globalsFactory.makeFullName(data); 
    
    //     return data.users;
    
    //   }, 
    
    //   function (error){
    
    //     console.log('Error');
    
    //     console.log(error);
    
    //   });
    
      // grid for subs here

}]);