// myClassesClassSubmissionsController.js

// The factory for this doesn't exist yet.

semesterProgramClassModule.controller('classStudentSubmissionsController', ['$scope', '$q', '$stateParams', 'myClassesSubmissionsFactory', function ($scope, $q, $stateParams, myClassesSubmissionsFactory) {

    console.log('classStudentSubmissionsController instantiated');

    var ctrl = this;

    ctrl.subsList =  [];
    
    ctrl.displayList = [];

    ctrl.submissionsExist = false;

    myClassesSubmissionsFactory.student($stateParams)

    .then(function (data){

        if (data.submissions && data.submissions.length > 0) ctrl.submissionsExist = true; 

        ctrl.subsList =  data.submissions;

        ctrl.displayList =  data.submissions;

        if(data.submissions.length > 0) {

            // ghetto get user info
            
            ctrl.student = data.submissions[0].student;

        }

    }, function (error){

        messageCenterService.add('danger', "Something went wrong while looking up your submissions. Try reloading the page.", { timeout: 6000 });

    });

    // single class stuff here.

}]);