// semesterProgramClassController.js

semesterProgramClassModule.controller('classController', ['$scope', '$q', 'classesFactory', 'globalsFactory', '$stateParams', '$state', 'messageCenterService', function ($scope, $q, classesFactory, globalsFactory, $stateParams, $state, messageCenterService) {
  
  console.log('class controller init');

    var ctrl = this;

    ctrl.classSelection = {};

    ctrl.classList = [];    

    ctrl.classSwitcher = function($item, $model){

        page = $state.current.name;

        $state.go(page, {class: $item.slug});
  
    };

    classesFactory.query($stateParams) 

    .then(function (data){

        if (data.classes && data.classes.length > 0) ctrl.classesExist = true; 

            ctrl.classList =  data.classes;

        }, function (error){

            messageCenterService.add('danger', "Something went wrong while looking up your classes. Try reloading the page.", { timeout: 6000 });

    });
    
    classesFactory.get($stateParams)

    .then(function (data){

        ctrl.classSelection = data.class;
    
    }, function (error){
    
        messageCenterService.add('danger', "Something went wrong while looking up your class. Try reloading the page.", { timeout: 6000 });
  
    });

}]);