// submissionsController.js

semesterProgramClassModule.controller('classSubmissionsController', ['$scope', '$q', '$stateParams', 'classSubmissionFactory', 'usersFactory', 'messageCenterService', function ($scope, $q, $stateParams, classSubmissionFactory, usersFactory, messageCenterService) {
  
    console.log('classSubmissionsController instantiated');

    var ctrl = this;

    ctrl.loaded = false;

    ctrl.subsList = [];

    ctrl.loadList = function(){

        classSubmissionFactory.query($stateParams).then(function (data){

            ctrl.subsList = data.submissions;

            ctrl.loaded = true;

        }, function (error){

            messageCenterService.add('danger', "Something went wrong while looking up your submissions. Try reloading the page.", { timeout: 6000 });

        });

    };

    ctrl.loadList();

}]);