// classAssignmentsController.js

semesterProgramClassModule.controller('classAssignmentsController', ['$scope', '$q', '$stateParams', 'classAssignmentFactory', 'usersFactory', 'messageCenterService', function ($scope, $q, $stateParams, classAssignmentFactory, usersFactory, messageCenterService) {

    var ctrl = this;

    ctrl.loaded = false;

    ctrl.assignmentList = [];

    ctrl.loadList = function(){

        classAssignmentFactory.query($stateParams).then(function (data){

            ctrl.assignmentList = data.assignments;

            ctrl.loaded = true;

        }, function (error){
            console.warn(error);
            messageCenterService.add('danger', "Something went wrong while looking up the assignments. Try reloading the page.", { timeout: 6000 });

        });

    };

    ctrl.loadList();

}]);