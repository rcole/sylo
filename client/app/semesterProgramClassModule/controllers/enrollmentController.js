// semesterProgramClassEnrollmentController.js

semesterProgramClassModule.controller('classEnrollmentController', ['$scope', '$q', '$stateParams', 'classEnrollmentFactory', 'messageCenterService', function ($scope, $q, $stateParams, classEnrollmentFactory, messageCenterService) {
  
    console.log('classEnrollmentController instantiated');
  
    var ctrl = this;
      
    ctrl.classEnrollments = [];

    ctrl.loaded = false;

    ctrl.loadList = function(){
  
        classEnrollmentFactory.query($stateParams).then(function (data){
      
            ctrl.classEnrollments = data.students;

            ctrl.displayList = data.students;

            ctrl.loaded = true;

        }, function (error){

            messageCenterService.add('danger', "Something went wrong while looking up your students. Try reloading the page.", { timeout: 6000 });

        });

    };

    ctrl.removeStudent = function(stuId){

        var enrollments = [{

            class_id: $scope.$parent.cls.classSelection.id,
      
            semester_id: $scope.$parent.sem.semesterSelection.id,
      
            student_id: stuId

        }];
        
        classEnrollmentFactory.remove($stateParams, {enrollments: enrollments}).then(function (data){
    
            if (data._error.code == 1) {
  
                messageCenterService.add('danger', "Something went wrong while removing that student.", { timeout: 6000 });
    
            } else {
          
                messageCenterService.add('success', "Student removed successfully.", { timeout: 6000 });

                // remove the student from the array here. not sure which will reflect in the table though...

            }

            ctrl.loadList();
  
        }, function (error){
  
            messageCenterService.add('danger', "Something went wrong while looking up your students. Try reloading the page.", { timeout: 6000 });
  
        });

    };

    ctrl.loadList();
  
}]);