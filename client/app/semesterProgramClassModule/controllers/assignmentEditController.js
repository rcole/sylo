// semesterProgramClassAssignmentController.js

semesterProgramClassModule.controller('assignmentEditController', ['$scope', '$q', '$stateParams', '$state', '$timeout', 'classAssignmentFactory', 'messageCenterService', function ($scope, $q, $stateParams, $state, $timeout, classAssignmentFactory, messageCenterService) {

    var ctrl = this;

    ctrl.mode = 'edit';

    ctrl.modal = true; 

    ctrl.deleting = false;

    ctrl.message = '';

    ctrl.toggleDelete = function(){

        ctrl.deleting = !ctrl.deleting;

    };

    ctrl.deleteItem = function(){
  
        classAssignmentFactory.remove($stateParams).then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while deleting the assignment.", { timeout: 6000 });

            } else {
                
                ctrl.message = 'Assignment Deleted';

                $timeout(function(){

                    $state.go('^', {reload:true});

                }, 1500);
            }

            $scope.$parent.clsAssign.loadList();

        },

        function(error){
            console.warn(error);
            messageCenterService.add('danger', "Something went wrong while deleting the assignment. Try again?", { timeout: 6000 });

        });

    };
    
    ctrl.updateItem = function(){
  
        classAssignmentFactory.update($stateParams, ctrl.item).then(function(data){

            if (data._error.code == 1) {
                console.warn(data);
                messageCenterService.add('danger', "Something went wrong while saving the assignment.", { timeout: 6000 });

            } else {

                ctrl.message = 'Assignment Updated';

                $timeout(function(){

                    ctrl.message = '';

                    $state.go('assignmentEdit', {assg: data.assignment.slug});

                }, 1500);

            }

            $scope.$parent.clsAssign.loadList();

        },

        function(error){
            console.warn(error);
            messageCenterService.add('danger', "Something went wrong while saving the assignment. Try again?", { timeout: 6000 });

        });

    };

    classAssignmentFactory.get($stateParams).then(function(data){

        if (data._error.code == 1) {
            console.warn(data);
            messageCenterService.add('danger', "Something went wrong while retrieving the assignment.", { timeout: 6000 });

        } else {

            ctrl.item = data.assignment;

        }

    },

    function(error){
        console.warn(error);
        messageCenterService.add('danger', "Something went wrong while looking up your assignment. Try reloading the page.", { timeout: 6000 });

    });

}]);