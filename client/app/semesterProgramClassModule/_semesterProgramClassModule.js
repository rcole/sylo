// semesterProgramClassModule.js Starts here 

// this module depends on semesterProgramModule. these routes will error with out it.

var semesterProgramClassModule = angular.module('semesterProgramClassModule', ['ui.router','smart-table','ngResource','ui.select','semesterProgramModule','usersModule']);

// routes for semester > program > class pathways
semesterProgramClassModule.config(['$stateProvider','$urlRouterProvider','$locationProvider', function($stateProvider, $urlRouterProvider, $locationProvider){    
    
  $urlRouterProvider.when('/semester/{semester}/program/{program}/class/{class}/', '/semester/{semester}/program/{program}/class/{class}/assignments');

  var classView = {
    name: "classView",
    parent: 'programView',
    url: "/class/:class",
    templateUrl: "app/webroot/tpl/class.tabFrame.tpl.html",
    controller: "classController as cls"
  },
  classAssignments = {
    name: "classAssignments",
    parent: 'classView',
    url: "/assignments",
    templateUrl: "app/webroot/tpl/class.assignmentsList.tpl.html",
    controller: "classAssignmentsController as clsAssign"
  },
  assignmentEdit = {
    name: "assignmentEdit",
    parent: 'classAssignments',
    url: "/:assg/edit",
    templateUrl: "app/webroot/tpl/assignmentForm.tpl.html",
    controller: "assignmentEditController as assignForm"
  },
  assignmentCreate = {
    name: "assignmentCreate",
    parent: 'classAssignments',
    url: "/create",
    templateUrl: "app/webroot/tpl/assignmentForm.tpl.html",
    controller: "assignmentCreateController as assignForm"
  },
  classEnrollment = {
    name: "classEnrollment",
    parent: 'classView',
    url: "/enrollment",
    templateUrl: "app/webroot/tpl/class.enrollment.tpl.html",
    controller: "classEnrollmentController as clsEnroll"
  },
  classEnrollmentEdit = {
    name: "classEnrollmentEdit",
    parent: 'classEnrollment',
    url: "/add",
    templateUrl: "app/webroot/tpl/class.enrollmentForm.tpl.html",
    controller: "classEnrollmentFormController as clsEnrollForm"
  },
  classSubmissions = {
    name: "classSubmissions",
    parent: 'classView',
    url: "/submissions",
    templateUrl: "app/webroot/tpl/class.submissions.tpl.html",
    controller: "classSubmissionsController as clsSubs"
  },
  classFeedback = {
    name: "classFeedback",
    parent: 'classView',
    url: "/feedback",
    templateUrl: "app/webroot/tpl/class.feedback.tpl.html",
    controller: "classFeedbackController as clsFeed"
  },
  classDocumentation = {
    name: "classDocumentation",
    parent: 'classView',
    url: "/documentation",
    templateUrl: "app/webroot/tpl/class.documentation.tpl.html",
    controller: "classDocumentationController as clsDoc"
  };

  $stateProvider
      .state(classView)
      .state(classAssignments)
      .state(assignmentCreate)
      .state(assignmentEdit)
      .state(classEnrollment)
      .state(classEnrollmentEdit)
      .state(classSubmissions)
      .state(classFeedback)
      .state(classDocumentation);
 }]);