// usersFactories.js

semesterProgramClassModule.factory('classEnrollmentFactory', ['$http', '$resource', '$q', function ($http, $resource, $q) {
  
  var programEnrollmentResource = $resource('api/semester/:semester/program/:program/class/:class/students.json', 

    { semester: '@semester', program: '@program', class: '@class' },
   
    { 
      
      'query' : {method: 'GET', isArray: false},

      'create' : {method: 'POST'},

      'remove' : {method: 'POST'}

    });

  var factory = {

    // add promises
    query : function (params) {
    
      var deferred = $q.defer();
    
      programEnrollmentResource.query({semester: params.semester, program: params.program, class: params.class},
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });
    
      return deferred.promise;
    
    },

    create : function (params, payload) {
    
      var deferred = $q.defer();
    
      programEnrollmentResource.create({semester: params.semester, program: params.program, class: params.class}, payload,
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });
    
      return deferred.promise;
    
    },

    remove : function (params, payload) {

      var deferred = $q.defer();

      payload._method = 'delete';

      programEnrollmentResource.remove({semester: params.semester, program: params.program, class: params.class}, payload, 
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    }

  };

  return factory;

}]);