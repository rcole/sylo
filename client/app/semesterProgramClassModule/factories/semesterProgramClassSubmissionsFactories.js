// semesterProgramClassSubmissionFactories.js

semesterProgramClassModule.factory('classSubmissionFactory', ['$http', '$resource', '$q', function ($http, $resource, $q) {


  var classSubmissionsResource = $resource('api/semester/:semester/program/:program/class/:class/submissions.json',

    { semester: '@semester', program: '@program', class: '@class' },

    {

      'query': {method: 'GET'}

    });

  var factory = {

    query: function (params) {

      var deferred = $q.defer();

      classSubmissionsResource.query({semester: params.semester, program: params.program, class: params.class},
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    }

  };

  return factory;

}]);