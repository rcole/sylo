//w semesterProgramClassFactories.js

semesterProgramClassModule.factory('classesFactory', ['$http', '$resource', '$q', function ($http, $resource, $q) {
  
  // TODO: include program and semester here, this is all classes, but should be for a program
  // deprecated
  var allClassesResource = $resource('api/classes.json', 
  
    { semester: '@semester', program: '@program' },
    
    {'query': {method: 'GET', isArray: false} 

  });

  var programClassesResource = $resource('api/semester/:semester/program/:program/classes.json',

    { semester: '@semester', program: '@program' },

    {

      'query': {method: 'GET', isArray: false},

      'create': {method: 'POST'}

    });

  var programClassResource = $resource('api/semester/:semester/program/:program/class/:class.json',

    { semester: '@semester', program: '@program', class: '@class' },

    {

      'get': {method: 'GET', isArray: false},

      'update': {method: 'POST'},

      'remove': {method: 'POST'} 

    });

  var authorizationsResource = $resource('api/teacherauthorizations.json',

    {},

    {

      'add': {method: 'POST'},

      'remove': {method: 'POST'} 

    });


  var factory = {

    // add promises
    queryAll: function () {

      var deferred = $q.defer();

      allClassesResource.query({},
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    },

    query: function (params) {

      var deferred = $q.defer();

      programClassesResource.query({semester: params.semester, program: params.program},
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    },

    get: function (params) {

      var deferred = $q.defer();

      programClassResource.get({semester: params.semester, program: params.program, class: params.class},
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    },

    create : function (params, payload) {

      console.log('create class!');

      var deferred = $q.defer();

      programClassesResource.create({semester: params.semester, program: params.program}, payload, 
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    },

    update : function (params, payload) {

      delete payload.id;
      delete payload.created;
      delete payload.updated;

      console.log('called update');

      payload._method = 'put';

      var deferred = $q.defer();

      programClassResource.update({semester: params.semester, program: params.program, class: params.class}, payload, 
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    },

    remove : function (params) {

      var deferred = $q.defer(),
          payload = { _method: 'delete' };

      programClassResource.remove({semester: params.semester, program: params.program, class: params.class}, payload, 
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    },
    addAuth : function (payload) {

      payload._method = 'put';

      var deferred = $q.defer();

      authorizationsResource.add({}, payload, 
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    },

    removeAuth : function (payload) {

      payload._method = 'delete';

      var deferred = $q.defer();

      authorizationsResource.remove({}, payload, 
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    }

  };

  return factory;

}]);