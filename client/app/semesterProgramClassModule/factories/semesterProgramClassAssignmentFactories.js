// semesterProgramClassAssignmentFactories.js

semesterProgramClassModule.factory('classAssignmentFactory', ['$http', '$resource', '$q', function ($http, $resource, $q) {


  var classAssignmentsResource = $resource('api/semester/:semester/program/:program/class/:class/assignments.json',

    { semester: '@semester', program: '@program', class: '@class' },

    {

      'query': {method: 'GET'},

      'create': {method: 'POST'}

    });


  var classAssignmentResource = $resource('api/semester/:semester/program/:program/class/:class/assignment/:assg.json',

    { semester: '@semester', program: '@program', class: '@class', assg: '@assg' },

    {

      'get': {method: 'GET'},

      'update': {method: 'POST'},

      'remove': {method: 'POST'} 

    });


  var factory = {

    query: function (params) {

      var deferred = $q.defer();

      classAssignmentsResource.query({semester: params.semester, program: params.program, class: params.class},
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    },

    get: function (params) {

      var deferred = $q.defer();

      classAssignmentResource.get({semester: params.semester, program: params.program, class: params.class, assg: params.assg},
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    },

    create : function (params, payload) {
      
      var deferred = $q.defer();

      classAssignmentsResource.create({semester: params.semester, program: params.program, class: params.class}, payload, 
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    },

    update : function (params, payload) {

      payload._method = 'put';

      var deferred = $q.defer();

      classAssignmentResource.update({semester: params.semester, program: params.program, class: params.class, assg: params.assg}, payload, 
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    },

    remove : function (params) {

      var payload = { _method: 'delete' };

      var deferred = $q.defer();

      classAssignmentResource.remove({semester: params.semester, program: params.program, class: params.class, assg: params.assg}, 
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    }

  };

  return factory;

}]);