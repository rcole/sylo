  module.exports = function(grunt) {

  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-banner');

  grunt.registerTask('default', ['clean','concat','compass:dev','copy','usebanner']);
  grunt.registerTask('release', ['clean','concat','uglify','compass:dist','copy:assets']);
  grunt.registerTask('js', ['jshint','clean','concat','copy']);
  grunt.registerTask('tpl-watch', ['clean','copy:tpl','watch:tpl']);
  grunt.registerTask('app-watch', ['clean','concat','compass:dev','copy','watch:js']);
  grunt.registerTask('sass-watch', ['clean','compass:dev','copy','watch:sass']);



  // Print a timestamp (useful for when watching)
  grunt.registerTask('timestamp', function() {
    grunt.log.subhead(Date());
  });

  grunt.initConfig({
        
    pkg: grunt.file.readJSON('package.json'),
    
    banner:
    '/*! <%= pkg.title || pkg.name %> - v<%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %>\n' +
    ' * Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author %>;\n' +
    ' */\n',

    distdir: 'app/webroot',
    
    src: {
      tpl:['client/app/**/*.tpl.html'],
      module: {
        acadServ: 'client/app/_academicservicesModule/**/*.js',
        admin: 'client/app/_adminModule/**/*.js',
        progLead: 'client/app/_programleaderModule/**/*.js',
        student: 'client/app/_studentModule/**/*.js',
        teacher: 'client/app/_teacherModule/**/*.js',
        extcodes: 'client/app/extcodes/**/*.js',
        global: ['client/app/global/**/*.js','client/app/globalFactories/**/*.js'],
        modules: 'client/app/modules/**/*.js',
        myclasses: 'client/app/myclasses/**/*.js',
        program: 'client/app/program/**/*.js',
        semProg: 'client/app/semesterProgramModule/**/*.js',
        semProgClass: 'client/app/semesterProgramClassModule/**/*.js',
        semesters: 'client/app/semesters/**/*.js',
        users: 'client/app/users/**/*.js',
        submit: 'client/app/submit/**/*.js',
        userVoice: 'client/app/userVoice/**/*.js',
        analytics: 'client/app/analytics/**/*.js',
        submissionList: 'client/app/submissionsList/**/*.js'
      },
      angular: ['client/vendor/angular/*.js','client/vendor/angular/modules/*.js'],
      other: 'client/vendor/other/*.js'
    },
    
    clean: {
      options: {
        src: ['<%= distdir %>/*'],
        force: 'true'
      }
    },
    
    copy: {
      assets: {
        files: [{ dest: '<%= distdir %>', src : '**', expand: true, cwd: 'client/assets/' }]
      },
      tpl: {
        files: [{expand: true, cwd: 'client/', src: ['*.tpl.html','**/*.tpl.html'], dest: '<%= distdir %>/tpl/', flatten: true, filter: 'isFile'}]
      }
    },
    
    compass: {  
      sassDir: ['client/sass'],            // Task
      dist: {                   // Target
        options: {              // Target options
          sassDir: 'client/sass',
          cssDir: '<%= distdir %>/css',
          outputStyle: 'compressed',
          environment: 'production',
          noLineComments: 'true'
        }
      },
      dev: {                    // Another target
        options: {
          sassDir: 'client/sass',
          cssDir: '<%= distdir %>/css',
          environment: 'development',
          outputStyle: 'expanded'
        }
      }
    },
    usebanner: {
      release: {
        options: {
            position: 'top',
            banner: '<%= banner %>',
            linebreak: true
        },
        files: {
            src: [ '<%= distdir %>/js/student.js', '<%= distdir %>/js/programLeader.js', '<%= distdir %>/js/admin.js', '<%= distdir %>/js/teacher.js', '<%= distdir %>/js/academicServices.js', '<%= distdir %>/js/student.min.js', '<%= distdir %>/js/programLeader.min.js', '<%= distdir %>/js/admin.min.js', '<%= distdir %>/js/teacher.min.js', '<%= distdir %>/js/academicServices.min.js' ]
        }
      }
    },
    concat:{
      adminApp: {
        src:['<%= src.module.admin %>','<%= src.module.global %>','<%= src.module.modules %>','<%= src.module.myclasses %>','<%= src.module.program %>','<%= src.module.semProg %>','<%= src.module.semProgClass %>','<%= src.module.semesters %>','<%= src.module.users %>','<%= src.module.extcodes %>','<%= src.module.userVoice %>','<%= src.module.analytics %>','<%= src.module.submissionList %>'],
        dest: '<%= distdir %>/js/admin.js'
      },
      teacherApp: {
        src:['<%= src.module.teacher %>','<%= src.module.global %>','<%= src.module.modules %>','<%= src.module.myclasses %>','<%= src.module.program %>','<%= src.module.semProg %>','<%= src.module.semProgClass %>','<%= src.module.semesters %>','<%= src.module.users %>','<%= src.module.extcodes %>','<%= src.module.userVoice %>','<%= src.module.analytics %>','<%= src.module.submissionList %>'],
        dest: '<%= distdir %>/js/teacher.js'
      },
      academicServicesApp: {
        src:['<%= src.module.acadServ %>','<%= src.module.global %>','<%= src.module.modules %>','<%= src.module.myclasses %>','<%= src.module.program %>','<%= src.module.semProg %>','<%= src.module.semProgClass %>','<%= src.module.semesters %>','<%= src.module.users %>','<%= src.module.extcodes %>','<%= src.module.userVoice %>','<%= src.module.analytics %>','<%= src.module.submissionList %>'],
        dest: '<%= distdir %>/js/academicServices.js'
      },
      programLeaderApp: {
        src:['<%= src.module.progLead %>','<%= src.module.global %>','<%= src.module.modules %>','<%= src.module.myclasses %>','<%= src.module.program %>','<%= src.module.semProg %>','<%= src.module.semProgClass %>','<%= src.module.semesters %>','<%= src.module.users %>','<%= src.module.extcodes %>','<%= src.module.userVoice %>','<%= src.module.analytics %>','<%= src.module.submissionList %>'],
        dest: '<%= distdir %>/js/programLeader.js'
      },
      studentApp: {
        src:['<%= src.module.semesters %>','<%= src.module.global %>','<%= src.module.submit %>','<%= src.module.extcodes %>','<%= src.module.student %>','<%= src.module.userVoice %>','<%= src.module.users %>','<%= src.module.analytics %>','<%= src.module.submissionList %>'],
        dest: '<%= distdir %>/js/student.js'
      },
      angular: {
        src:'<%= src.angular %>',
        dest: '<%= distdir %>/js/angular.js'
      },
      other: {
        src:'<%= src.other %>',
        dest: '<%= distdir %>/js/libs.js'
      }
    },
    uglify: {
      options: {
        banner: '<%= banner %>',
        compress: {
          drop_console: true
        }
      },
      dist: {
        files: {
          '<%= distdir %>/js/admin.min.js': ['<%= distdir %>/js/libs.js','<%= distdir %>/js/angular.js','<%= distdir %>/js/admin.js'],
          '<%= distdir %>/js/teacher.min.js': ['<%= distdir %>/js/libs.js','<%= distdir %>/js/angular.js','<%= distdir %>/js/teacher.js'],
          '<%= distdir %>/js/academicServices.min.js': ['<%= distdir %>/js/libs.js','<%= distdir %>/js/angular.js','<%= distdir %>/js/academicServices.js'],
          '<%= distdir %>/js/programLeader.min.js': ['<%= distdir %>/js/libs.js','<%= distdir %>/js/angular.js','<%= distdir %>/js/programLeader.js'],
          '<%= distdir %>/js/student.min.js': ['<%= distdir %>/js/libs.js','<%= distdir %>/js/angular.js','<%= distdir %>/js/student.js']
        }
      }
    },
    
    jshint: {
      files: ['gruntfile.js', 'client/app/**/*.js', 'test/**/*.js'],
      options: {
        // options here to override JSHint defaults
        globals: {
          jQuery: true,
          console: true,
          angular: true,
          module: true,
          document: true
        }
      }
    },
    
    watch: {
      js:{
        files: ['<%= jshint.files %>'],
        tasks: [/*'jshint',*/'concat']
      },
      sass: {
        files: ['client/sass/*.scss','client/sass/includes/*.scss'],
        tasks: ['compass:dev']
      },
      tpl: {
        files: ['client/*.tpl.html','client/**/*.tpl.html'],
        tasks: ['copy:tpl']
      }
    }
  });
};