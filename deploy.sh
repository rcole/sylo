#!/bin/bash

usage()
{
cat << EOF
usage: $0 options

This script deploy web application

OPTIONS:
   -h      Show this message
   -t      Task type, can be ‘prepare′ or ‘finish′. Default is `prepare`
EOF
}

export LITHIUM_ENVIRONMENT=test

TASK=
REMOTE="git@bitbucket.org:rcole/sylo.git"
BASEDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [ -d "$BASEDIR/_repo" ]; then
	cd "$BASEDIR/_repo"
	/usr/bin/git fetch --tags
	npm update
else
	/usr/bin/git clone --recursive $REMOTE "$BASEDIR/_repo"
	cd "$BASEDIR/_repo"
	npm install
fi

REVISION="$(/usr/bin/git describe --tags `/usr/bin/git rev-list --tags --max-count=1`)"

while getopts “ht:” OPTION
do
     case $OPTION in
         h)
             usage
             exit 1
             ;;
         t)
             TASK=$OPTARG
             ;;
         ?)
             usage
             exit
             ;;
     esac
done

if [ "$TASK" == "finish" ]
then
     if [ ! -d "$BASEDIR/$REVISION" ]; then
        echo "$(tput setaf 1)Latest version $REVISION is NOT checked out!$(tput sgr0)"
        exit 1
     fi

     cd $BASEDIR
     rm public
     ln -s "$BASEDIR/$REVISION" public
     exit 1
fi

if [ -d "$BASEDIR/$REVISION" ]; then
  echo "$(tput setaf 2)Latest version $REVISION is already checked out!$(tput sgr0)"
  exit 1
fi

echo "Copying files..."

/usr/bin/rsync -avhrltD --stats --human-readable "$BASEDIR/_repo/" "$BASEDIR/$REVISION/" | /usr/bin/pv -lep -s "$(find $BASEDIR/_repo | wc -l)" > /dev/null

cd "$BASEDIR/$REVISION"

/usr/bin/git checkout "tags/$REVISION"
/usr/bin/git submodule update --init --recursive
# grunt release

/bin/chmod 750 -R "$BASEDIR/$REVISION"

# copy db config
cp "$BASEDIR/$REVISION/app/config/bootstrap/connections.sample.php" "$BASEDIR/$REVISION/app/config/bootstrap/connections.php"

# uploads and resources and key
rm -r "$BASEDIR/$REVISION/app/webroot/uploads"
ln -s "$BASEDIR/uploads" "$BASEDIR/$REVISION/app/webroot/uploads"

rm -r "$BASEDIR/$REVISION/app/resources"
ln -s "$BASEDIR/resources" "$BASEDIR/$REVISION/app/resources"

ln -s "$BASEDIR/pc-work-upload-system-e9e4371e9fd0.p12" "$BASEDIR/$REVISION/app/keys/gcskey.p12"