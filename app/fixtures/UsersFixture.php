<?php

namespace app\fixtures;

use app\extensions\helper\Debug;

class UsersFixture extends \app\extensions\data\Fixture {

	protected static $_model = 'app\models\Users';

	protected static $_fixtures = [
		// Admins
		[
			'id' => 2,
			'first_name' => 'Ryan',
			'last_name' => 'Cole',
			'password' => 'ryan.c@praguecollege.cz',
			'email' => 'ryan.c@praguecollege.cz',
			'status' => 1,
			'timezone' => 'Europe/Budapest',
			'created' => 0,
			'updated' => 0
		]	
	];
}

?>