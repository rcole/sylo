<?php

namespace app\fixtures;

use app\extensions\helper\Debug;

class ConcurrencesFixture extends \app\extensions\data\Fixture {

	protected static $_model = 'app\models\Concurrences';

	protected static $_fixtures = [
		/**
		 * Intro to Rasters - Fall
		 */
		[
			'id' => 1,
			'class_id' => 1,
			'semester_id' => 3,
		],

		/**
		 * Tracing Rasters - Winter
		 */
		[
			'id' => 2,
			'class_id' => 2,
			'semester_id' => 4,
		],

		/**
		 * Intro to Pottery - Winter
		 */
		[
			'id' => 3,
			'class_id' => 3,
			'semester_id' => 4,
		],

		/**
		 * Photoshop 101 - Winter
		 */
		[
			'id' => 4,
			'class_id' => 4,
			'semester_id' => 4,
		],

		/**
		 * Intro to Vectors - Winter
		 */
		[
			'id' => 5,
			'class_id' => 5,
			'semester_id' => 4,
		],
	];
}

?>