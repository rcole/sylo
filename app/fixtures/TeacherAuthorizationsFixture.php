<?php

namespace app\fixtures;

use app\extensions\helper\Debug;

class TeacherAuthorizationsFixture extends \app\extensions\data\Fixture {

	protected static $_model = 'app\models\TeacherAuthorizations';

	protected static $_fixtures = [
		/**
		 * Ryan - Intro to Pottery (Winter)
		 */
		[
			'id' => 1,
			'teacher_id' => 2,
			'class_id' => 3,
			'semester_id' => 4
		],
		/**
		 * Ryan - Intro to Vectors (Winter)
		 */
		[
			'id' => 2,
			'teacher_id' => 2,
			'class_id' => 5,
			'semester_id' => 4
		],

		/**
		 * Housni - Intro to Rasters (Fall)
		 */
		[
			'id' => 3,
			'teacher_id' => 1,
			'class_id' => 1,
			'semester_id' => 3
		],
		/**
		 * Housni - Tracing Rasters (Winter)
		 */
		[
			'id' => 4,
			'teacher_id' => 1,
			'class_id' => 2,
			'semester_id' => 4
		]
	];
}

?>