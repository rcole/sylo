<?php

namespace app\fixtures;

use app\extensions\helper\Debug;

class RolesFixture extends \app\extensions\data\Fixture {

	protected static $_model = 'app\models\Roles';

	protected static $_fixtures = [
		[
			'id' 		  => 1,
			'title'		  => 'Administrator',
			'label' 	  => 'admin',
			'description' => 'Administrator role',
			'home' 		  => '/profile',
			'status' 	  => 1,
			'rank'		  => 1
		],
		[
			'id' 		  => 2,
			'title'		  => 'Program Leader',
			'label' 	  => 'prgLead',
			'description' => 'Program Leader role',
			'home' 		  => '/profile',
			'status' 	  => 1,
			'rank'		  => 2
		],
		[
			'id' 		  => 3,
			'title'		  => 'Teacher',
			'label' 	  => 'teacher',
			'description' => 'Teacher role',
			'home' 		  => '/profile',
			'status' 	  => 1,
			'rank'		  => 3
		],
		[
			'id' 		  => 4,
			'title'		  => 'Academic Services',
			'label' 	  => 'acdServ',
			'description' => 'Academic Services role',
			'home' 		  => '/profile',
			'status' 	  => 1,
			'rank'		  => 2
		],
		[
			'id' 		  => 5,
			'title'		  => 'External Verifier',
			'label' 	  => 'extVer',
			'description' => 'External Verifier role',
			'home' 		  => '/profile',
			'status' 	  => 1,
			'rank'		  => 2
		],
		[
			'id' 		  => 6,
			'title'		  => 'Student Records',
			'label' 	  => 'studRec',
			'description' => 'Student Records role',
			'home' 		  => '/profile',
			'status' 	  => 1,
			'rank'		  => 2
		],
		[
			'id' 		  => 7,
			'title'		  => 'Student',
			'label' 	  => 'student',
			'description' => 'Student role',
			'home' 		  => '/profile',
			'status' 	  => 1,
			'rank'		  => 4
		]
	];
}

?>