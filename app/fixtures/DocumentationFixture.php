<?php

namespace app\fixtures;

use app\extensions\helper\Debug;

class DocumentationFixture extends \app\extensions\data\Fixture {

	protected static $_model = 'app\models\Documentation';

	protected static $_fixtures = [
		[
			'id' => 1,
			'class_id' => 1,
			'teacher_id' => 1,
			'semester_id' => 4,
			'title' => 'Documentation for Intro to Rasters by Housni',
		],
		[
			'id' => 2,
			'class_id' => 1,
			'teacher_id' => 2,
			'semester_id' => 4,
			'title' => 'Documentation for Intro to Rasters by Ryan',
		],
		[
			'id' => 3,
			'class_id' => 2,
			'teacher_id' => 2,
			'semester_id' => 4,
			'title' => 'Documentation for Tracing Rasters by Ryan',
		],
		[
			'id' => 4,
			'class_id' => 3,
			'teacher_id' => 1,
			'semester_id' => 4,
			'title' => 'Documentation for Intro to Pottery by Housni',
		],
		[
			'id' => 5,
			'class_id' => 4,
			'teacher_id' => 1,
			'semester_id' => 4,
			'title' => 'Documentation for Photoshop 101 by Housni',
		],
	];
}

?>