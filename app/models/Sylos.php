<?php

namespace app\models;

use app\extensions\helper\Debug;

class Sylos extends \app\extensions\data\Model {

	protected $_meta = [
		'constraints' => [
			[
				'type'   => 'primary',
				'column' => 'id'
			],
		],
		'table' => [
			'charset' => 'utf8',
			'collate' => 'utf8_unicode_ci',
			'engine'  => 'MyISAM'
		]
	];

	protected $_schema = [
		'id' => ['type' => 'id'],
		'created' => ['type' => 'integer', 'null' => false],
		'updated' => ['type' => 'integer', 'null' => false]
	];

	public $validates = [];

	/**
	 * Formats output for JSON
	 */
	public static function prepareJSONOutput($record, $key) {

		$data[$key] = $record->to('array', ['indexed' => false]);

		if (1 == count($record)) {
				$data['_error'] = array_pop($data[$key]);
		}

		if (1 < count($record)) {
				$error = array_pop($data[$key]);
				$data['_error'] = array_pop($error);
		}

		return $data;
	}
}

?>