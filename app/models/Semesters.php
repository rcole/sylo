<?php

namespace app\models;

use lithium\util\Inflector;
use app\extensions\helper\Debug;

/**
 * A `Semesters` record can tell us the year and months this current semester belongs to.
 */
class Semesters extends \app\extensions\data\Model {

	public $hasMany = ['Enrollments', 'Concurrences', 'TeacherAuthorizations', 'Documentation', 'Feedback', 'Assignments'];

	protected $_meta = [
		'constraints' => [
			[
				'type'   => 'primary',
				'column' => 'id'
			],
			[
				'type'   => 'unique',
				'column' => 'title',
				'index'  => true,
				'key'    => true,
			],
			[
				'type'   => 'unique',
				'column' => 'slug',
				'index'  => true,
				'key'    => true,
			],
		],
		'table' => [
			'charset' => 'utf8',
			'collate' => 'utf8_unicode_ci',
			'engine'  => 'MyISAM'
		]
	];

	protected $_schema = [
		'id' => ['type' => 'id'],
		'title' => [
			'type' => 'string',
			'length' => 255,
			'null' => false
		],
		'slug' => [
			'type' => 'string',
			'length' => 255,
			'null' => false,
			'comment' => 'Slug of the title. Added automatically'
		],
		'year' => [
			'type' => 'integer',
			'length' => 4,
			'null' => false,
			'comment' => 'The year for the semester'
		],
		'semester' => [
			'type' => 'integer',
			'length' => 1,
			'null' => false,
			'comment' => 'The semester count where: `semester` > 0 && `semester` < 5'
		],
		'begins' => [
			'type' => 'integer',
			'null' => false
		],
		'ends' => [
			'type' => 'integer',
			'null' => false
		],
		'created' => ['type' => 'integer', 'null' => false],
		'updated' => ['type' => 'integer', 'null' => false]
	];

	public $validates = [];

	/**
	 * Filters the save() method in order to create the `title` based on the values of `year` and
	 * `semester`.
	 */
/*
	public static function __init() {
		parent::__init();

		static::applyFilter('save', function($self, $params, $chain) {
			if ($params['data']) {
				$params['entity']->set($params['data']);
				$params['data'] = [];
			}

			$params['entity']->title = substr($params['entity']->year, 2);
			$params['entity']->title .= str_pad($params['entity']->semester, 2, '0', STR_PAD_LEFT);

			return $chain->next($self, $params, $chain);
		});
	}
*/

	/**
	 * Returns the current semester.
	 */
	public static function current() {
		return static::find('first', [
            'conditions' => [
            	'begins' => ['<=' => time()],
            	'ends' => ['>=' => time()]
            ]
		]);
	}
}

/**
 * Filters the save() method in order to create the `slug` based on the `title`.
 */
Semesters::applyFilter('save', function($self, $params, $chain) {
	if ($params['data']) {
		$params['entity']->set($params['data']);
		$params['data'] = [];
	}

	$params['entity']->slug = Inflector::slug($params['entity']->title);
	return $chain->next($self, $params, $chain);
});

?>