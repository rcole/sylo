<?php

namespace app\models;

use app\extensions\helper\Debug;

class RolesUsers extends \app\extensions\data\Model {

	public $belongsTo = ['Users', 'Roles'];

	protected $_meta = [
		'constraints' => [
			[
				'type'   => 'primary',
				'column' => 'id'
			],
			[
				'type'     => 'foreign_key',
				'column'   => 'role_id',
				'toColumn' => 'id',
				'to'       => 'roles',
			],
			[
				'type'     => 'foreign_key',
				'column'   => 'user_id',
				'toColumn' => 'id',
				'to'       => 'users',
			],
		],
		'table' => [
			'charset' => 'utf8',
			'collate' => 'utf8_unicode_ci',
			'engine'  => 'MyISAM'
		]
	];

	protected $_schema = [
		'id' => ['type' => 'id'],
		'role_id' => [
			'type' => 'integer',
			'null' => false
		],
		'user_id' => [
			'type' => 'integer',
			'null' => false
		],
		'created' => ['type' => 'integer', 'null' => false],
		'updated' => ['type' => 'integer', 'null' => false],
	];

	public $validates = [];
}


?>