<?php

namespace app\models;

use app\extensions\helper\Debug;

/**
 * A `Teachers` role may teach one or more `Classes` which they are linked to via `Authorizations`.
 * They may also perform CRUD operations on `Assignments` which they are entitled to access
 * via `Classes`.
 */
class Teachers extends \app\extensions\data\Model {

	protected $_meta = [
		'source' => 'users',
	];

	/**
	 * Let `li3_db` know that this model should not be processed since it uses the users table.
	 * @var boolean
	 */
	public static $persist = false;

	/**
	 * For RolesUsers, we specify that the key used for the match should be `user_id` and not
	 * `teacher_id` since we are actually querying the `users` table.
	 */
	public $hasMany = ['Documentation', 'TeacherAuthorizations', 'RolesUsers' => [
		'key' => ['id' => 'user_id']
	]];

	public $validates = [];

	/**
	 * Filtering the finder to only return records whose Roles.id = 3. In other words, all records
	 * returned will be those of Teachers.
	 */
	public static function __init() {
		parent::__init();

		static::applyFilter('find', function($self, $params, $chain) {
			$params['options']['conditions']['Roles.id'] = 3;
			$params['options']['with'][] = 'RolesUsers.Roles';
			return $chain->next($self, $params, $chain);
		});
	}

	/**
	 * Overriding Model::title() in order to get a custom title for use in select lists.
	 */
	public function title($entity) {
		return "{$entity->first_name} {$entity->last_name}";
	}
}

?>