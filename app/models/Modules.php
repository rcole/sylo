<?php

namespace app\models;

use lithium\util\Inflector;

use app\extensions\helper\Debug;

/**
 * Sometimes referred to as `Units`, a Module is a single subject (eg: English 101) that runs
 * through every semester.
 */
class Modules extends \app\extensions\data\Model {

	public $belongsTo = ['Programs'];

	public $hasMany = ['Classes'];

	protected $_meta = [
		'constraints' => [
			[
				'type'   => 'primary',
				'column' => 'id'
			],
			[
				'type'     => 'foreign_key',
				'column'   => 'program_id',
				'toColumn' => 'id',
				'to'       => 'programs',
			],
		],
		'table' => [
			'charset' => 'utf8',
			'collate' => 'utf8_unicode_ci',
			'engine'  => 'MyISAM'
		]
	];

	protected $_schema = [
		'id' => ['type' => 'id'],
		'program_id' => [
			'type' => 'integer',
			'null' => false
		],
		'title' => [
			'type' => 'string',
			'length' => 255,
			'null' => false
		],
		'short_name' => [
			'type' => 'string',
			'length' => 255,
			'null' => false
		],
		'unit_number' => [
			'type' => 'string',
			'length' => 255,
			'null' => false
		],
		'slug' => [
			'type' => 'string',
			'length' => 255,
			'null' => false,
			'comment' => 'Slug of the title. Added automatically'
		],
		'created' => ['type' => 'integer', 'null' => false],
		'updated' => ['type' => 'integer', 'null' => false]
	];

	// public $validates = [
	// 	'title' => [
	// 		'unique' => [
	// 			'unique',
	// 			'message' => 'This module title already exists',
	// 			'model' => '\app\models\Modules'
	// 		]
	// 	],
	// 	'unit_number' => [
	// 		'unique' => [
	// 			'unique',
	// 			'message' => 'This module unit number already exists',
	// 			'model' => '\app\models\Modules'
	// 		]
	// 	]
	// ];

	/**
	 * Filters the save() method in order to create the `slug` based on the `title`.
	 */
	public static function __init() {
		parent::__init();

		static::applyFilter('save', function($self, $params, $chain) {
			if ($params['data']) {
				$params['entity']->set($params['data']);
				$params['data'] = [];
			}

			$params['entity']->slug = Inflector::slug($params['entity']->title);
			return $chain->next($self, $params, $chain);
		});
	}
}

?>