<?php

namespace app\models;

use app\extensions\helper\Debug;

class ProgramAuthorizations extends \app\extensions\data\Model {

	public $belongsTo = ['Programs', 'ProgramLeaders'];

	protected $_meta = [
		'constraints' => [
			[
				'type'   => 'primary',
				'column' => 'id'
			],
			[
				'type'     => 'foreign_key',
				'column'   => 'program_id',
				'toColumn' => 'id',
				'to'       => 'programs',
			],
			[
				'type'     => 'foreign_key',
				'column'   => 'program_leader_id',
				'toColumn' => 'id',
				'to'       => 'users',
			],
		],
		'table' => [
			'charset' => 'utf8',
			'collate' => 'utf8_unicode_ci',
			'engine'  => 'MyISAM'
		]
	];

	protected $_schema = [
		'id' => ['type' => 'id'],
		'program_id' => [
			'type' => 'integer',
			'null' => false
		],
		'program_leader_id' => [
			'type' => 'integer',
			'null' => false
		],
		'created' => ['type' => 'integer', 'null' => false],
		'updated' => ['type' => 'integer', 'null' => false]
	];

	public $validates = [];
}

?>