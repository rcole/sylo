<?php

namespace app\models;

use lithium\util\Inflector;
use app\models\Semesters;
use app\models\Users;

use app\extensions\helper\Debug;

/**
 * This is a single instance of a `Modules` record and refers to that single instance for a
 * semester, in particular.
 */
class Classes extends \app\extensions\data\Model {

	public $belongsTo = ['Modules'];

	public $hasMany = ['Assignments', 'TeacherAuthorizations', 'Documentation', 'Enrollments', 'Concurrences', 'Feedback'];

	protected $_meta = [
		'constraints' => [
			[
				'type'   => 'primary',
				'column' => 'id'
			],
			[
				'type'     => 'foreign_key',
				'column'   => 'module_id',
				'toColumn' => 'id',
				'to'       => 'modules',
			],
		],
		'table' => [
			'charset' => 'utf8',
			'collate' => 'utf8_unicode_ci',
			'engine'  => 'MyISAM'
		]
	];

	protected $_schema = [
		'id' => ['type' => 'id'],
		'module_id' => [
			'type' => 'integer',
			'null' => false
		],
		'title' => [
			'type' => 'string',
			'length' => 255,
			'null' => false
		],
		'slug' => [
			'type' => 'string',
			'length' => 255,
			'null' => false,
			'comment' => 'Slug of the title. Added automatically'
		],
		'begins' => [
			'type' => 'integer',
			'null' => true,
			'comment' => 'Can be specified if class has a beginning date different to that of Semesters.begins'
		],
		'ends' => [
			'type' => 'integer',
			'null' => true,
			'comment' => 'Can be specified if class has a ending date different to that of Semesters.ends'
		],
		'created' => ['type' => 'integer', 'null' => false],
		'updated' => ['type' => 'integer', 'null' => false]
	];

	public $validates = [];

	/**
	 * Filters the save() method in order to create the `slug` based on the `title`.
	 * It also creates a `begins` and `ends` date if they are not provided.
	 */
	public static function __init() {
		parent::__init();

		static::applyFilter('save', function($self, $params, $chain) {
			if ($params['data']) {
				$params['entity']->set($params['data']);
				$params['data'] = [];
			}

			$semester = Semesters::find($params['entity']->semester_id);
			if (!$params['entity']->modified('begins')) {
				$params['entity']->begins = $semester->begins;
			}
			if (!$params['entity']->modified('ends')) {
				$params['entity']->ends = $semester->ends;
			}

			$params['entity']->slug = Inflector::slug($params['entity']->title);
			return $chain->next($self, $params, $chain);
		});
	}

	protected static function _optionsForSemesterAndPrograms(array $options) {
		$conditions['Programs.slug'] = $options['program'];
		$conditions['Semesters.slug'] = $options['semester'];
		if (isset($options['class'])) {
			$conditions['Classes.slug'] = $options['class'];
		}

		$fields = Users::safeFields([], 'Teachers');
		$fields[] = 'Classes.*';
		$fields[] = 'Modules.*';
		$fields[] = 'Concurrences.*';
		$fields[] = 'Programs.*';
		$fields[] = 'Semesters.*';
		$fields[] = 'TeacherAuthorizations.*';
		return [
			'order' => ['id' => 'ASC'],
			'with' => ['Modules.Programs', 'Concurrences.Semesters', 'TeacherAuthorizations', 'TeacherAuthorizations.Teachers'],
			'conditions' => $conditions,
			'fields' => $fields
		];
	}

	public static function getClassBySemesterAndProgram(array $options) {
		$conditions['Programs.slug'] = $options['program'];
		$conditions['Semesters.slug'] = $options['semester'];
		if (isset($options['class'])) {
			$conditions['Classes.slug'] = $options['class'];
		}
		return static::find('first', static::_optionsForSemesterAndPrograms($options));
	}

	public static function getClassesBySemesterAndProgram(array $options) {
		$conditions['Programs.slug'] = $options['program'];
		$conditions['Semesters.slug'] = $options['semester'];
		if (isset($options['class'])) {
			$conditions['Classes.slug'] = $options['class'];
		}
		return static::all(static::_optionsForSemesterAndPrograms($options));
	}
}

?>