<?php

namespace app\models;

use app\extensions\helper\Debug;

class Permissions extends \app\extensions\data\Model {

	public $belongsTo = ['Roles', 'Resources'];

	protected $_meta = [
		'constraints' => [
			[
				'type'   => 'primary',
				'column' => 'id'
			],
			[
				'type'     => 'foreign_key',
				'column'   => 'role_id',
				'toColumn' => 'id',
				'to'       => 'roles',
			],
			[
				'type'     => 'foreign_key',
				'column'   => 'resource_id',
				'toColumn' => 'id',
				'to'       => 'resources',
			]
		],
		'table' => [
			'charset' => 'utf8',
			'collate' => 'utf8_unicode_ci',
			'engine'  => 'MyISAM'
		]
	];

	protected $_schema = [
		'id' => ['type' => 'id'],
		'role_id' => [
			'type' => 'integer',
			'null' => false
		],
		'resource_id' => [
			'type' => 'integer',
			'null' => false
		],
		'allowed' => [
			'type' => 'boolean',
			'null' => false,
			'comment' => '0 = denied; 1 = allowed'
		],
		'notes' => [
			'type'       => 'string',
			'length'     => 255,
			'null'       => true,
		],
		'created' => ['type' => 'integer', 'null' => false],
		'updated' => ['type' => 'integer', 'null' => false],
	];

	public $validates = [];
}


?>