<?php

namespace app\models;

use lithium\data\Entity;

use app\extensions\helper\Debug;

class Roles extends \app\extensions\data\Model {

	/**
	 * An array of possible statuses available.
	 *
	 * @var array
	 */
	protected $_statuses = [
		0 => 'Inactive',
		1 => 'Active',
	];

	public $hasMany = ['RolesUsers'];

	public $hasOne = ['Permissions'];

	protected $_schema = [
		'id'           => ['type' => 'id'],
		'title'   => [
			'type'       => 'string',
			'length'     => 100,
			'null'       => false,
		],
		'label'    => [
			'type'       => 'string',
			'length'     => 255,
			'null'       => false,
		],
		'description'    => [
			'type'       => 'string',
			'length'     => 255,
			'null'       => false,
		],
		'home'    => [
			'type'       => 'string',
			'length'     => 255,
			'null'       => false,
			'default'    => '/',
			'comment'    => 'Home URL (relative) for this role',
		],
		'status'       => [
			'type'    => 'integer',
			'length'  => 5,
			'default' => 1,
			'null'    => false,
			'comment' => '1 = enabled; 0 = disabled;'
		],
		'rank' => [
			'type' => 'integer',
			'length'  => 2,
			'default' => 100,
			'null'    => false,
			'comment' => 'The ranking of this role. 1 is the highest rank and 100 is the lowest'
		],
		'created'      => ['type' => 'integer', 'null' => false],
		'updated'      => ['type' => 'integer', 'null' => false]
	];

	/**
	 * @todo
	 */
	public $validates = [
	];

	/**
	 * Returns a specific status text.
	 *
	 * @see Roles::$_statuses
	 * @param lithium\data\Entity $entity
	 * @return array Returns the specific status text based on `$entity->status`
	 */
	public function status(Entity $entity) {
		return $this->_statuses[$entity->status];
	}

	/**
	 * @return array Returns a list of possible statuses a user can have
	 */
	public function statuses() {
		return $this->_statuses;
	}
}

?>