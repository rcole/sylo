<?php

namespace app\models;

use app\extensions\helper\Debug;

/**
 * This model defines the relationship between `Teachers` and `Classes` record. With this, we can
 * figure out which teacher teaches which class.
 */
class TeacherAuthorizations extends \app\extensions\data\Model {

	public $belongsTo = ['Classes', 'Teachers', 'Semesters'];

	protected $_meta = [
		'constraints' => [
			[
				'type'   => 'primary',
				'column' => 'id'
			],
			[
				'type'     => 'foreign_key',
				'column'   => 'class_id',
				'toColumn' => 'id',
				'to'       => 'classes',
			],
			[
				'type'     => 'foreign_key',
				'column'   => 'teacher_id',
				'toColumn' => 'id',
				'to'       => 'teachers',
			],
			[
				'type'     => 'foreign_key',
				'column'   => 'semester_id',
				'toColumn' => 'id',
				'to'       => 'semesters',
			],
		],
		'table' => [
			'charset' => 'utf8',
			'collate' => 'utf8_unicode_ci',
			'engine'  => 'MyISAM'
		]
	];

	protected $_schema = [
		'id' => ['type' => 'id'],
		'class_id' => [
			'type' => 'integer',
			'null' => false
		],
		'teacher_id' => [
			'type' => 'integer',
			'null' => false
		],
		'semester_id' => [
			'type' => 'integer',
			'null' => false
		],
		'created' => ['type' => 'integer', 'null' => false],
		'updated' => ['type' => 'integer', 'null' => false]
	];

	public $validates = [];
}

?>