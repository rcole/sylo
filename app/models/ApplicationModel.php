<?php

namespace app\models;

use app\extensions\helper\Debug;

/**
 * This is a base model that other models can extend from, if they wish, in
 * order to inherit functionality such as custom validation/finders, etc.
 */
class ApplicationModel extends \app\extensions\data\Model {

	/**
	 * Let `li3_db` know that this model should not be processed.
	 * @var boolean
	 */
	public static $persist = false;

	public static function __init() {
		parent::__init();
    }
}

?>