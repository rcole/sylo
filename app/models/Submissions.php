<?php

namespace app\models;

use lithium\core\Libraries;
use lithium\action\Request;
use lithium\net\http\Router;
use app\extensions\helper\Debug;

/**
 * These are files uploaded by `Students` as a part of `Assignments`. A file may not be uploaded
 * beyond a deadline unless an extension code is assigned to it.
 */
class Submissions extends \app\extensions\data\Model {

	public $belongsTo = ['Assignments', 'Students'];

	public $hasOne = ['ExtensionCodes'];

	protected $_meta = [
		'constraints' => [
			[
				'type'   => 'primary',
				'column' => 'id'
			],
			[
				'type'     => 'foreign_key',
				'column'   => 'student_id',
				'toColumn' => 'id',
				'to'       => 'users',
			],
			[
				'type'     => 'foreign_key',
				'column'   => 'assignment_id',
				'toColumn' => 'id',
				'to'       => 'assignments',
			]
		],
		'table' => [
			'charset' => 'utf8',
			'collate' => 'utf8_unicode_ci',
			'engine'  => 'MyISAM'
		]
	];

	protected $_schema = [
		'id' => ['type' => 'id'],
		'assignment_id' => [
			'type' => 'integer',
			'null' => false
		],
		'student_id' => [
			'type' => 'integer',
			'null' => false
		],
		'name' => [
			'type' => 'string',
			'length' => 255,
			'null' => false,
			'comment' => 'Name of the uploaded file'
		],
		'path' => [
			'type' => 'string',
			'length' => 255,
			'null' => false,
			'comment' => 'Path to the uploaded file'
		],
		'receipt' => [
			'type' => 'string',
			'length' => 255, //@todo must be changed, should have some semantic meaning
			'null' => true,
			'comment' => 'Auto generated code for verification'
		],
		'created' => ['type' => 'integer', 'null' => false],
		'updated' => ['type' => 'integer', 'null' => false]
	];

	public $validates = [];

	public static function url(array $data = [], $absolute = false) {
		$request = new Request;
		return Router::match('/', $request, ['absolute' => true]);
	}

	public static function path($key = null, array $data = []) {
		$paths = [];

		if ($data) {
			$paths['path'] = "Semester-{$data['semester']->id}/Program-{$data['assignment']->class->module->program->id}/" .
				"Class-{$data['assignment']->class->id}/Assignment-{$data['assignment_id']}/Student-{$data['student_id']}/";

			$paths['save'] = Libraries::get(true, 'path')  . '/webroot/uploads/' . $paths['path'];
		}
		$paths['chunks'] = Libraries::get(true, 'resources') . '/tmp/Flow/chunks';


		if (null === $key) {
			return $paths;
		}

		return $paths[$key];
	}
}

?>