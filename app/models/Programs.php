<?php

namespace app\models;

use lithium\util\Inflector;
use app\extensions\helper\Debug;

class Programs extends \app\extensions\data\Model {

	public $belongsTo = ['ProgramLeaders'];

	public $hasMany = ['Modules', 'ProgramAuthorizations'];

	protected $_meta = [
		'constraints' => [
			[
				'type'   => 'primary',
				'column' => 'id'
			],
			[
				'type'   => 'unique',
				'column' => 'title',
				'index'  => true,
				'key'    => true,
			],
			[
				'type'   => 'unique',
				'column' => 'slug',
				'index'  => true,
				'key'    => true,
			],
		],
		'table' => [
			'charset' => 'utf8',
			'collate' => 'utf8_unicode_ci',
			'engine'  => 'MyISAM'
		]
	];

	protected $_schema = [
		'id' => ['type' => 'id'],
		'title' => [
			'type' => 'string',
			'length' => 255,
			'null' => false
		],
		'slug' => [
			'type' => 'string',
			'length' => 255,
			'null' => false,
			'comment' => 'Slug of the title. Added automatically'
		],
		'created' => ['type' => 'integer', 'null' => false],
		'updated' => ['type' => 'integer', 'null' => false]
	];

	public $validates = [];
}

/**
 * Filters the save() method in order to create the `slug` based on the `title`.
 */
Programs::applyFilter('save', function($self, $params, $chain) {
	if ($params['data']) {
		$params['entity']->set($params['data']);
		$params['data'] = [];
	}

	$params['entity']->slug = Inflector::slug($params['entity']->title);
	return $chain->next($self, $params, $chain);
});

?>