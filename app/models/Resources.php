<?php

namespace app\models;

use app\extensions\helper\Debug;

class Resources extends \app\extensions\data\Model {

	public $hasOne = ['Permissions'];

	protected $_meta = [
		'constraints' => [
			[
				'type'   => 'primary',
				'column' => 'id'
			],
		],
		'table' => [
			'charset' => 'utf8',
			'collate' => 'utf8_unicode_ci',
			'engine'  => 'MyISAM'
		]
	];

	protected $_schema = [
		'id' => ['type' => 'id'],
		'controller' => [
			'type'       => 'string',
			'length'     => 20,
			'null'       => false,
		],
		'action' => [
			'type'       => 'string',
			'length'     => 20,
			'null'       => false,
		],
		'operation' => [
			'type'       => 'string',
			'length'     => 20,
			'null'       => false,
			'comment'    => 'A particular operation (eg: delete, verify, *, etc)'
		],
		'notes' => [
			'type'       => 'string',
			'length'     => 255,
			'null'       => true,
		],
		'created' => ['type' => 'integer', 'null' => false],
		'updated' => ['type' => 'integer', 'null' => false],
	];

	public $validates = [];
}


?>