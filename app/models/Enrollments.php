<?php

namespace app\models;

use app\extensions\helper\Debug;

/**
 * `Enrollments` will define the relationship between `Students` and `Classes`. The association will
 * come from `FileMaker`.
 */
class Enrollments extends \app\extensions\data\Model {

	public $belongsTo = ['Classes', 'Students', 'Semesters'];

	protected $_meta = [
		'constraints' => [
			[
				'type'   => 'primary',
				'column' => 'id'
			],
			[
				'type'     => 'foreign_key',
				'column'   => 'class_id',
				'toColumn' => 'id',
				'to'       => 'classes',
			],
			[
				'type'     => 'foreign_key',
				'column'   => 'student_id',
				'toColumn' => 'id',
				'to'       => 'users',
			],
			[
				'type'     => 'foreign_key',
				'column'   => 'semester_id',
				'toColumn' => 'id',
				'to'       => 'semesters',
			],
		],
		'table' => [
			'charset' => 'utf8',
			'collate' => 'utf8_unicode_ci',
			'engine'  => 'MyISAM'
		]
	];

	protected $_schema = [
		'id' => ['type' => 'id'],
		'class_id' => [
			'type' => 'integer',
			'null' => false
		],
		'semester_id' => [
			'type' => 'integer',
			'null' => false
		],
		'student_id' => [
			'type' => 'integer',
			'null' => false
		],
		'created' => ['type' => 'integer', 'null' => false],
		'updated' => ['type' => 'integer', 'null' => false]
	];

	public $validates = [];
}

?>