<?php
/**
 * Lithium: the most rad php framework
 *
 * @copyright     Copyright 2013, Union of RAD (http://union-of-rad.org)
 * @license       http://opensource.org/licenses/bsd-license.php The BSD License
 */

/**
 * This layout is used to render error pages in both development and production. It is recommended
 * that you maintain a separate, simplified layout for rendering errors that does not involve any
 * complex logic or dynamic data, which could potentially trigger recursive errors.
 */
use lithium\core\Libraries;
$path = Libraries::get(true, 'path');
?>
<!doctype html>
<html>
<head>
	<title>~ Application Error ~</title>
	<?php echo $this->html->charset();?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!--[if IE]>
  		<?php echo $this->html->style(['ie.css']); ?>
  <![endif]-->

  	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,400,300&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
	<?php echo $this->html->style(['screen.css', 'print.css']); ?>

	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?= $this->path('img/apple-touch-icon-144x144-precomposed.png'); ?>">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?= $this->path('img/apple-touch-icon-114x114-precomposed.png'); ?>">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?= $this->path('img/apple-touch-icon-72x72-precomposed.png'); ?>">
	<link rel="apple-touch-icon-precomposed" href="<?= $this->path('apple-touch-icon-57x57-precomposed.png'); ?>">
	<?php echo $this->html->link('Icon', null, ['type' => 'icon']); ?>
	<base href="<?= $this->request()->env('base'); ?>/">
	<!-- Heap tracking code -->
	<script type="text/javascript">
      window.heap=window.heap||[],heap.load=function(t,e){window.heap.appid=t,window.heap.config=e;var a=document.createElement("script");a.type="text/javascript",a.async=!0,a.src=("https:"===document.location.protocol?"https:":"http:")+"//cdn.heapanalytics.com/js/heap.js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(a,n);for(var o=function(t){return function(){heap.push([t].concat(Array.prototype.slice.call(arguments,0)))}},p=["identify","track"],c=0;c<p.length;c++)heap[p[c]]=o(p[c])};
      heap.load("912591352");
    </script>
</head>
<body class="errorPage">
	<section>
		<img src="img/explosive.svg" alt="App Error" class="explosive"/>
		<h1>BOOM.</h1>
		<h2>
			Something has gone wrong with the app. If you continue to experience this problem, please send a mail to <a href="mailto:ryan.c@praguecollege.cz">ryan.c@praguecollege.cz</a> and describe what you were doing when the problem occurred.  
		</h2>
	</section>
	<footer>
		<p>&copy; 2014 - Prague College | <a href="http://praguecollege.cz">praguecollege.cz</a> | <a href="mailto:ryan.c@praguecollege.cz">ryan.c@praguecollege.cz</a></p>
	</footer>
	<script>heap.track('App Error');</script>
	
</body>
</html>


