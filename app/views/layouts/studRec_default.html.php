<?php
/**
 * Lithium: the most rad php framework
 *
 * @copyright     Copyright 2013, Union of RAD (http://union-of-rad.org)
 * @license       http://opensource.org/licenses/bsd-license.php The BSD License
 */
?>
<!DOCTYPE html>

<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]> 	   <html class="no-js"> <![endif]-->

<html lang="en">
<head>
	<title>Sylo</title>
	<?php echo $this->html->charset();?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!--[if IE]>
  		<?php echo $this->html->style(['ie.css']); ?>
  <![endif]-->

  	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,400,300&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
	<?php echo $this->html->style(['screen.css', 'print.css']); ?>

	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?= $this->path('img/apple-touch-icon-144x144-precomposed.png'); ?>">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?= $this->path('img/apple-touch-icon-114x114-precomposed.png'); ?>">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?= $this->path('img/apple-touch-icon-72x72-precomposed.png'); ?>">
	<link rel="apple-touch-icon-precomposed" href="<?= $this->path('apple-touch-icon-57x57-precomposed.png'); ?>">
	<?php echo $this->html->link('Icon', null, ['type' => 'icon']); ?>
	<base href="<?= $this->request()->env('base'); ?>/">
	<!-- Heap tracking code -->
	<script type="text/javascript">
      window.heap=window.heap||[],heap.load=function(e,t){window.heap.appid=e,window.heap.config=t=t||{};var n=t.forceSSL||"https:"===document.location.protocol,a=document.createElement("script");a.type="text/javascript",a.async=!0,a.src=(n?"https:":"http:")+"//cdn.heapanalytics.com/js/heap-"+e+".js";var o=document.getElementsByTagName("script")[0];o.parentNode.insertBefore(a,o);for(var r=function(e){return function(){heap.push([e].concat(Array.prototype.slice.call(arguments,0)))}},p=["clearEventProperties","identify","setEventProperties","track","unsetEventProperty"],c=0;c<p.length;c++)heap[p[c]]=r(p[c])};
    </script>
</head>
<body class="admin_default" ng-controller="globalsController as globals">
	<div mc-messages></div>
    <!--[if lt IE 7]>
        <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
    <![endif]-->
		<section id="topbar" ng-include="'app/webroot/tpl/topbar.tpl.html'"></section>
		<nav id="sidebar" ng-include="'app/webroot/tpl/adminSidebar.tpl.html'"></nav>
		<section class="pageWidth content" ui-view>
			<h1>No App Configured Yet.</h1>
		</section>

    <!-- this is the set of scripts to include -->
    <?php echo $this->html->script($this->appjs->tags('academicServices')); ?>
</body>
</html>