<?php
/**
 * Lithium: the most rad php framework
 *
 * @copyright     Copyright 2013, Union of RAD (http://union-of-rad.org)
 * @license       http://opensource.org/licenses/bsd-license.php The BSD License
 */
?>
<!doctype html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><html class="no-js"> <![endif]-->
<html lang="en">
<head>
	<?php echo $this->html->charset();?>
	<title>sylo | <?php echo $this->title(); ?></title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!--[if IE]>
  		<?php echo $this->html->style(['ie.css']); ?>
  <![endif]-->

	<?php echo $this->html->style(['screen.css', 'print.css']); ?>

    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?= $this->path('apple-touch-icon-144x144-precomposed.png'); ?>">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?= $this->path('apple-touch-icon-114x114-precomposed.png'); ?>">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?= $this->path('apple-touch-icon-72x72-precomposed.png'); ?>">
	<link rel="apple-touch-icon-precomposed" href="<?= $this->path('apple-touch-icon-57x57-precomposed.png'); ?>">
	<?php echo $this->html->link('Icon', null, ['type' => 'icon']); ?>
</head>
<body class="default">
    <!--[if lt IE 7]>
        <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
    <![endif]-->
		<section id="login">
	        <?= $this->form->create(null, [
	            'url'    => ['Sessions::create'],
	            'method' => 'post',
	        ]); ?>
				<h2>Login to Sylo</h2>
			    <div class="formGroup">
					<input type="email" name="email" class="form-control" placeholder="Prague College Email" id="Email" />
			    	<input type="password" name="password" class="form-control" autocomplete="off" placeholder="Password" id="Password" />
		            <div>
		            	<button type="submit">Log In</button>
			        </div>
			        <div>
			        	<a class="login-link" href="mailto:ryan.c@praguecollege.cz">Lost your password or your kitten? Send me a mail.</a>
			        </div>
			    </div>
        	<?= $this->form->end(); ?>
			<footer>
				<p>&copy; 2014 - Prague College | <a href="http://praguecollege.cz">praguecollege.cz</a> | <a href="mailto:ryan.c@praguecollege.cz">ryan.c@praguecollege.cz</a></p>
			</footer>
		</section>
</body>
</html>