<?php

foreach ($this->data() as $key => $datum) {
	if (is_object($datum) && is_subclass_of($datum, '\lithium\data\Collection')){
		$data[$key] = $datum->to('array', ['indexed' => false]);
		echo json_encode($data, JSON_PRETTY_PRINT);
	}
}

?>