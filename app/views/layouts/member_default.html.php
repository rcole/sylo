<?php
/**
 * Lithium: the most rad php framework
 *
 * @copyright     Copyright 2013, Union of RAD (http://union-of-rad.org)
 * @license       http://opensource.org/licenses/bsd-license.php The BSD License
 */
?>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]>	   <html class="no-js"> <![endif]-->

<html lang="en" ng-app="adminApp" ng-cloak>
<head>
	<?php echo $this->html->charset();?>
	<title>Application &gt; <?php echo $this->title(); ?></title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php echo $this->html->style(['bootstrap.min.css', 'application.css']); ?>
	<?php echo $this->styles(); ?>
    <?php echo $this->html->script(['jquery.min.js', 'modernizr-build.min.js', 'respond.min.js']); ?>
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?= $this->path('apple-touch-icon-144x144-precomposed.png'); ?>">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?= $this->path('apple-touch-icon-114x114-precomposed.png'); ?>">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?= $this->path('apple-touch-icon-72x72-precomposed.png'); ?>">
	<link rel="apple-touch-icon-precomposed" href="<?= $this->path('apple-touch-icon-57x57-precomposed.png'); ?>">
	<?php echo $this->html->link('Icon', null, ['type' => 'icon']); ?>
	<!-- Heap tracking code -->
	<script type="text/javascript">
      window.heap=window.heap||[],heap.load=function(e,t){window.heap.appid=e,window.heap.config=t=t||{};var n=t.forceSSL||"https:"===document.location.protocol,a=document.createElement("script");a.type="text/javascript",a.async=!0,a.src=(n?"https:":"http:")+"//cdn.heapanalytics.com/js/heap-"+e+".js";var o=document.getElementsByTagName("script")[0];o.parentNode.insertBefore(a,o);for(var r=function(e){return function(){heap.push([e].concat(Array.prototype.slice.call(arguments,0)))}},p=["clearEventProperties","identify","setEventProperties","track","unsetEventProperty"],c=0;c<p.length;c++)heap[p[c]]=r(p[c])};
    </script>
</head>
<body>
	<div mc-messages></div>
    <!--[if lt IE 7]>
        <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
    <![endif]-->
	<div class="content hide">
		<div class="row">
			<strong>Member Role</strong>
			<hr>
            <?= $this->flashMessage->show('error',   ['data' => ['class' => 'danger']]); ?>
            <?= $this->flashMessage->show('success', ['data' => ['class' => 'success']]); ?>
            <?= $this->flashMessage->show('info',    ['data' => ['class' => 'info']]); ?>
			<?php echo $this->content(); ?>
		</div>
		<hr>
		<div class="footer">
			<p>&copy; Koobi Inc. 2013</p>
		</div>
	</div>
    <?php echo $this->html->script([
       	'bootstrap.min.js',
    	'application.js'
    ]); ?>
    <script><?php echo $this->scripts(); ?></script>
</body>
</html>