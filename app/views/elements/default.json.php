<?php

use app\models\Sylos;
use app\extensions\helper\Debug;

/**
 * A catch-all JSON output template for any controller actions that don't have a corresponding JSON
 * template.
 *
 * This template grabs the data given to it by the controller, iterates through the data looking
 * for a `\lithium\data\Collection` object (which would mean that the data in the current iteration
 * is a database collection) and then grabds the data of that object without an index since PHP's
 * `json_encode()` encodes non-sequential arrays as objects and we specifically want a JSON array
 * which would contain the actual records as objects.
 *
 * @link http://www.php.net/manual/en/function.json-encode.php
 */
foreach ($this->data() as $key => $datum) {
	if (is_object($datum) && (is_subclass_of($datum, '\lithium\data\Collection') || is_subclass_of($datum, '\lithium\data\Entity'))){
		$data = Sylos::prepareJSONOutput($datum, $key);
		echo json_encode($data, JSON_PRETTY_PRINT);
	}
}
?>