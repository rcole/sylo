<?php $this->title($t('Log In')); ?>

<?= $this->form->create(null, [
    'url' => ['Sessions::create'],
    'method' => 'post',
    'class' => 'form-horizontal'
]); ?>
    <h2>Sign in</h2>
    <?= $this->form->field('email', [
        'template' => '<div{:wrap}>{:label}<div class="col-lg-10">{:input}{:error}</div></div>',
        'wrap' => 'class="form-group"',
        'class' => 'form-control',
        'label' => ['Email' => ['class' => 'col-lg-2 control-label']],
        'type' => 'email',
        'placeholder' => $t('Email'),
    ]); ?>
    <?= $this->form->field('password', [
        'template' => '<div{:wrap}>{:label}<div class="col-lg-10">{:input}{:error}
            <div class="checkbox">
                <label>
                    <input type="checkbox"> Remember me
                </label>
            </div>
            <input type="submit" class="btn btn-default" value="Sign in">
        </div></div>',
        'wrap' => 'class="form-group"',
        'class' => 'form-control',
        'label' => ['Password' => ['class' => 'col-lg-2 control-label']],
        'type' => 'password',
        'autocomplete' => 'off',
        'placeholder' => $t('Password'),
    ]); ?>
    <a class="login-link" href="mailto:ryan.c@praguecollege.cz">Lost your password? Send me mail!</a>
<?=$this->form->end(); ?>