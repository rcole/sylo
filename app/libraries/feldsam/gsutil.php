<?php
/**
 * gsutil PHP Wrapper
 * @author Kristian Feldsam <feldsam@gmail.com>
 * @copyright 2015 Kristian Feldsam <feldsam@gmail.com>
 */

namespace Feldsam;

/**
 * gsutil PHP Wrapper Class. This class implements all gsutil tool commands.
 * gsutil is a Python application that lets you access Google Cloud Storage from the command line. You can use gsutil to do a wide range of bucket and object management tasks, including:
 * 	- Creating and deleting buckets.
 * 	- Uploading, downloading, and deleting objects.
 * 	- Listing buckets and objects.
 * 	- Moving, copying, and renaming objects.
 * 	- Editing object and bucket ACLs.
 *
 * @author Kristian Feldsam <feldsam@gmail.com>
 * @version 0.1.0
 */
class Gsutil {	
	/**
	 * Protected method for invoke gsutil python app
	 * @param  string $args Arguments for gsutil python app
	 * @param  string $stdin Something to pass to stdin
	 * @return string Output of stdout
	 * @throws Exception If something happen in stderr
	 */
	protected static function _exec($args, $stdin = null, $command = 'gsutil -q') {
		$descriptorspec = array(
			0 => array('pipe', 'r'),
			1 => array('pipe', 'w'),
			2 => array('pipe', 'w'),
		);

		$process = proc_open($command . ' ' . $args, $descriptorspec, $pipes);

		//  something goes wrong
		if( ! is_resource($process)) {
			throw new \Exception('Feldsam\\Gsutil: Can\'t open process!');
		}
		
		// pass stdin
		if($stdin !== null) {
			stream_copy_to_stream($stdin, $pipes[0]);
		}
		
		// close stdin
		fclose($pipes[0]);
		
		// check for errors
		if($errors = stream_get_contents($pipes[2])) {
			// close stderr, sdtout and process
			fclose($pipes[1]);
			fclose($pipes[2]);
			proc_close($process);
			
			throw new \Exception('Feldsam\\Gsutil: '.$errors);
		}
		
		// read stdout
		$out = stream_get_contents($pipes[1]);
		
		// close stder, sdtout and process
		fclose($pipes[1]);
		fclose($pipes[2]);
		proc_close($process);
		
		// return output of stdout
		return $out;
	}
	
	public static function copyRecursive($from, $to) {
		return self::_exec('-m cp -r ' . $from . ' ' . $to);
	}
	
	public function getSignedUrl($file) {
		$result = self::_exec('signurl -d 5m -p notasecret ' . LITHIUM_APP_PATH . '/keys/gcskey.p12 ' . $file);
		
		if(preg_match('/https:\\/\\/.+$/', $result, $m)) {
			return $m[0];
		}
		
		throw new \Exception('Can\'t get URL from result!');
	}
	
	public function removeLocalFiles($path) {
		return self::_exec('-rf ' . $path, null, 'rm');
	}
}