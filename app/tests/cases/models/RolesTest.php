<?php

namespace app\tests\cases\models;

use app\fixtures\RolesFixture;
use app\tests\mocks\data\MockRoles;

class RolesTest extends \lithium\test\Unit {

	/**
	 * Loads records from fixtures into test database.
	 */
	public function setUp() {
		MockRoles::remove();
		foreach (RolesFixture::load() as $data) {
			$roles = MockRoles::create();
			$roles->save($data);
		}
	}

	/**
	 * Removes records from test database.
	 */
	public function tearDown() {
		MockRoles::remove();
	}

	public function testAllStatuses() {
		$roles = new MockRoles;
		$result = $roles->statuses();
		$expected = [
			0 => 'Inactive',
			1 => 'Active'
		];
		$this->assertEqual($expected, $result);
	}

	public function testEachStatus() {
		$roles = MockRoles::find(3);
		$roles->status = 0;
		$result = $roles->status();
		$expected = 'Inactive';
		$this->assertEqual($expected, $result);

		$roles = MockRoles::find(1);
		$roles->status = 1;
		$result = $roles->status();
		$expected = 'Active';
		$this->assertEqual($expected, $result);
	}

	public function testAllRoleLabelsExist() {
		$labels = ['admin', 'prgLead', 'teacher', 'acdServ', 'extVer', 'studRec', 'student'];
		foreach (MockRoles::all() as $role) {
			$this->assertContains($role->label, $labels);
		}
	}
}

?>