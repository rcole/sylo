<?php

namespace app\tests\cases\models;

use app\fixtures\UsersFixture;
use app\tests\mocks\data\MockUsers;
use lithium\security\Password;

class UsersTest extends \lithium\test\Unit {

	/**
	 * Loads records from fixtures into test database.
	 */
	public function setUp() {
		MockUsers::remove();
		foreach (UsersFixture::load() as $data) {
			$users = MockUsers::create();
			$users->save($data);
		}
	}

	/**
	 * Removes records from test database.
	 */
	public function tearDown() {
		MockUsers::remove();
	}

	public function testAllStatuses() {
		$users = new MockUsers;
		$result = $users->statuses();
		$expected = [
			0 => 'Inactive',
			1 => 'Active'
		];
		$this->assertEqual($expected, $result);
	}

	public function testEachStatus() {
		$users = MockUsers::find(3);
		$users->status = 0;
		$result = $users->status();
		$expected = 'Inactive';
		$this->assertEqual($expected, $result);

		$users = MockUsers::find(1);
		$users->status = 1;
		$result = $users->status();
		$expected = 'Active';
		$this->assertEqual($expected, $result);
	}

	public function testFullNameAsTitle() {
		$users = MockUsers::find(1);
		$users->first_name = 'Housni';
		$users->last_name = 'Yakoob';
		$result = $users->title();
		$expected = 'Housni Yakoob';
		$this->assertEqual($expected, $result);
	}

	public function testPasswordSaltSaved() {
		$user = MockUsers::find(1);
		$user->save($user->data());
		$this->assertNotNull($user->salt);
	}

	/**
	 * Checks to see if a plain text password is altered on save (hash).
	 */
	public function testPasswordHashOnSave() {
		$password = 'hyakoob';
		$user = MockUsers::find(1);
		$user->password = $password;
		$user->save($user->data());
		$this->assertNotEqual($password, $user->password);
	}

	public function testPasswordHashCheck() {
		$password = 'hyakoob';
		$user = MockUsers::find(1);
		$user->password = $password;
		$user->save($user->data());
		$result = Password::check($password, $user->password);
		$this->assertTrue($result);
	}
}

?>