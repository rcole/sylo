<?php

namespace app\tests\cases\models;

use app\models\Programs;
use app\tests\mocks\data\MockPrograms;
use app\fixtures\ProgramsFixture;

class ProgramsTest extends \lithium\test\Unit {

	public function setUp() {
		MockPrograms::remove();
		foreach (ProgramsFixture::load() as $data) {
			$program = MockPrograms::create();
			$program->save($data);
		}
	}

	public function tearDown() {
		MockPrograms::remove();
	}

	// Tests for all programs
	public function testAllProgramsExist() {
		$programs = MockPrograms::find('all');
		$result = $programs->count();
		$this->assertEqual(2, $result);
	}

	// Tests that `slug` valid based on `title` on create()
	public function testSlugIsFromTitleOnCreate() {
		$data = [
			'program_leader_id' => 1,
			'title' => 'Art & Design'
		];
		$program = Programs::create();
		$program->save($data);
		$this->assertEqual('Art-Design', $program->slug);

		$data = [
			'program_leader_id' => 1,
			'title' => 'Foo bar 101'
		];
		$program = Programs::create();
		$program->save($data);
		$this->assertEqual('Foo-bar-101', $program->slug);
	}

	// Tests that `slug` valid based on `title` on update()
	public function testSlugIsFromTitleOnUpdate() {
		$program = Programs::find('first', [
			'conditions' => ['title' => 'Graphic Design']
		]);
		$program->title = 'Graphic Design 101';
		$program->save();
		$this->assertEqual('Graphic-Design-101', $program->slug);
	}
}

?>