<?php

namespace app\tests\mocks\data;

use lithium\data\entity\Record;
use lithium\data\collection\RecordSet;

class MockSemesters extends \app\models\Semesters {

	protected $_meta = [
		'source' => 'semesters',
		'name' => 'Semesters'
	];

	private static $_data = [
		[
			'id' => 1,
			'year' => 2013,
			'semester' => 1,
			'begins' => 1357430400,
			'ends' => 1364428800
		],
		[
			'id' => 2,
			'year' => 2013,
			'semester' => 2,
			'begins' => 1367107200,
			'ends' => 1374969600
		],
		[
			'id' => 3,
			'year' => 2013,
			'semester' => 3,
			'begins' => 1377648000,
			'ends' => 1382918400
		],
		[
			'id' => 4,
			'year' => 2013,
			'semester' => 4,
			'begins' => 1384041600,
			'ends' => 1387497600
		]
	];

	public static function find($type = 'all', array $options = array()) {
		if (is_numeric($type)) {
			return new Record([
				'data' => static::$_data[$type]
			]);
		}

		switch ($type) {
			case 'first':
				return new Record([
					'data' => static::$_data[0]
				]);
			break;

			default:
			case 'all':
				return new RecordSet([
					'data' => static::$_data
				]);
			break;
		}
	}
}

?>