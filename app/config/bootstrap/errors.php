<?php
/**
 * Lithium: the most rad php framework
 *
 * @copyright     Copyright 2013, Union of RAD (http://union-of-rad.org)
 * @license       http://opensource.org/licenses/bsd-license.php The BSD License
 */
use lithium\core\Environment;
use lithium\core\ErrorHandler;
use lithium\core\Libraries;
use lithium\action\Response;
use lithium\net\http\Media;
use lithium\analysis\Logger;
use lithium\util\String;
use app\models\Sessions;

use app\extensions\helper\Debug;

ErrorHandler::apply('lithium\action\Dispatcher::run', array(), function($info, $params) {

	$request = \lithium\net\http\Router::process($params['request']);
	if (isset($request->params['api']) && $request->params['api'] == 'api') {
		$response = new \lithium\net\http\Response;
		$response->status('404');
	}

	if(preg_match('/^\/uploads\/(.+)/', $request->url, $m))
	{
		require(LITHIUM_APP_PATH . '/libraries/feldsam/gsutil.php');
		
		try
		{
			$url = \Feldsam\Gsutil::getSignedUrl('gs://pc-work-upload/'.$m[1]);

			header('Location: '.$url);
			exit;
		}
		catch(\Exception $e){}
	}

	$e 		  = $info['exception'];
	$template = 'development';
	$status   = $info['exception']->getCode();

	if (Environment::is('production')) {
		if (404 == $e->getCode()) {
			$template = '404';
			$status   = '404';
		} else {
			$template = '500';
			$status   = '500';
		}
	}

	$response = new Response(array(
		'request' => $params['request'],
		'status'  => $status
	));

	Logger::write('error', String::insert(
		'{:remote_host} {:ident} {:Users.id} [{:date}] "{:request}" {:status} {:bytes}',
		[
			'remote_host' => $request->env('REMOTE_ADDR'),
			'ident' => '-',
			'Users.id' => (null !== Sessions::id()) ? Sessions::id() : '-',
			'date' => strftime('%d/%b/%Y:%H:%M:%S %z'),
			'request' => "{$request->method} {$request->env('REQUEST_URI')} {$request->protocol}",
			'status' => $response->status['code'],
			'bytes' => '-'
		]
	));

	Media::render($response, compact('info', 'params'), array(
		'library'    => true,
		'controller' => '_errors',
		'template'   => $template,
		'layout' 	 => 'error',
		'request' 	 => $params['request']
	));
	return $response;
});

Logger::config([
	'default' => [
		'adapter' => 'File',
		'path' => dirname(Libraries::get(true, 'path')) . '/log',
		'timestamp' => false,
		'file' => function($data, $config) {
			/**
			 * @todo Implement log rotation.
			 *       Append the week number to the log and on each log write, delete all log files
			 *       whose week number is 1 less than the current week.
			 */
			return "{$data['priority']}.log";
		},
		'priority' => ['debug', 'notice', 'warning', 'error']
	],
]);

?>
