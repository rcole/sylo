<?php
/**
 * Lithium: the most rad php framework
 *
 * @copyright     Copyright 2013, Union of RAD (http://union-of-rad.org)
 * @license       http://opensource.org/licenses/bsd-license.php The BSD License
 */

/**
 * This file contains a series of method filters that allow you to intercept different parts of
 * Lithium's dispatch cycle. The filters below are used for on-demand loading of routing
 * configuration, and automatically configuring the correct environment in which the application
 * runs.
 *
 * For more information on in the filters system, see `lithium\util\collection\Filters`.
 *
 * @see lithium\util\collection\Filters
 */

use lithium\core\Libraries;
use lithium\core\Environment;
use lithium\action\Dispatcher;
use app\extensions\helper\Debug;

/**
 * This filter intercepts the `run()` method of the `Dispatcher`, and first passes the `'request'`
 * parameter (an instance of the `Request` object) to the `Environment` class to detect which
 * environment the application is running in. Then, loads all application routes in all plugins,
 * loading the default application routes last.
 *
 * Change this code if plugin routes must be loaded in a specific order (i.e. not the same order as
 * the plugins are added in your bootstrap configuration), or if application routes must be loaded
 * first (in which case the default catch-all routes should be removed).
 *
 * If `Dispatcher::run()` is called multiple times in the course of a single request, change the
 * `include`s to `include_once`.
 *
 * @see lithium\action\Request
 * @see lithium\core\Environment
 * @see lithium\net\http\Router
 */
Dispatcher::applyFilter('run', function($self, $params, $chain) {
	Environment::set($params['request']);

	/**
	 * @todo  This is a temporary fix till we can figure out how to detect JSON POST requests with
	 *        the `_method` parameter set to `put` as a PUT request.
	 *        At the moment, what I've done is set a custom detector to treat any POST
	 *        `REQUEST_METHOD` as a PUT request if the parameter `_method` exists and is set to `put`.
	 */
	if (isset($params['request']->data['_method']) && 'put' == strtolower($params['request']->data['_method'])) {
		$params['request']->detect('put', ['REQUEST_METHOD', 'POST']);
	}
	if (isset($params['request']->data['_method']) && 'delete' == strtolower($params['request']->data['_method'])) {
		$params['request']->detect('delete', ['REQUEST_METHOD', 'POST']);
	}

	foreach (array_reverse(Libraries::get()) as $name => $config) {
		if ($name === 'lithium') {
			continue;
		}
		$file = "{$config['path']}/config/routes.php";
		file_exists($file) ? call_user_func(function() use ($file) { include $file; }) : null;
	}
	$controller = $chain->next($self, $params, $chain);

	//$body = reset($controller->body);
	//$error = json_decode($body)->_error;
	//$controller->status($error->status);

	return $controller;
});

Environment::is(function($request) {
	if ($request->env('HTTP_HOST') === 'localhost') {
		return 'development';
	}
	if ($request->env('HTTP_HOST') === 'submitstaging.pragucollege.cz') {
		return 'test';
	}
	return 'production';
});

?>