<?php
/**
 * Lithium: the most rad php framework
 *
 * @copyright     Copyright 2013, Union of RAD (http://union-of-rad.org)
 * @license       http://opensource.org/licenses/bsd-license.php The BSD License
 */

/**
 * The routes file is where you define your URL structure, which is an important part of the
 * [information architecture](http://en.wikipedia.org/wiki/Information_architecture) of your
 * application. Here, you can use _routes_ to match up URL pattern strings to a set of parameters,
 * usually including a controller and action to dispatch matching requests to. For more information,
 * see the `Router` and `Route` classes.
 *
 * @see lithium\net\http\Router
 * @see lithium\net\http\Route
 */
use lithium\net\http\Router;
use lithium\core\Environment;
use lithium\action\Response;
use lithium\security\Auth;
use lithium\util\Inflector;
use lithium\action\Dispatcher;
use lithium\net\http\Media;
use app\models\Programs;
use app\models\Semesters;
use app\models\Classes;
use app\models\Students;
use app\models\Sessions;
use app\models\Roles;
use lithium\analysis\Debugger;

/**
 * With globalization enabled a localized route is configured by connecting a
 * continuation route. Once the route has been connected, all the other
 * application routes become localized and may now carry a locale.
 *
 * Requests to routes like `/en/posts/edit/1138` or `/fr/posts/edit/1138` will
 * carry a locale, while `/posts/edit/1138` keeps on working as it did before.
 */
if ($locales = Environment::get('locales')) {
	$template = '/{:locale:' . join('|', array_keys($locales)) . '}/{:args}';
	Router::connect($template, array(), array('continue' => true));
}

// set the evironment
if($_SERVER['HTTP_HOST'] == 'localhost') {
//	Environment::set('development');
}

/**
 * Attempting to match the role in the URL. This is a continuation route.
 */
Router::connect('/{:debug:debug}/{:args}', [], ['continue' => true]);

/**
 * Adding `/api` prefix to routes.
 * All API requests must also have the `.json` extension.
 */
//Router::connect('/{:api:api}/{:args}.{:type:json}',  [], ['continue' => true]);

/**
 * Routes for .html & .json extension
 */
Router::connect('/{:args}.{:type:html|json}', [], ['continue' => true]);


/**
 * Temporarily setting a debug route in order to display output of variables.
 */
Dispatcher::config(['rules' => [
	'action' => ['action' => function($params) {
		$defaults = [
			'debug' => null,
			'api' => null
		];
		$params += $defaults;

		$action = $params['action'];
		$layout = null;

		if ($params['debug']) {
			$layout = "{$params['debug']}_";
		}

		$label = Sessions::label();
		if ($label && $params['action'] != 'validate') {
			$action = $label . Inflector::camelize($params['action']);
			$layout = "{$label}_";
		}

		$template = ['{:library}/views/users/' . $label . 'View.html.php'];
		if ('Sessions' == $params['controller'] && 'create' == $params['action']) {
			$template = ['{:library}/views/sessions/create.html.php'];
		}

		Media::type('default', null, [
			'view' => 'lithium\template\View',
			'paths' => [
				'layout' => '{:library}/views/layouts/' . $layout . '{:layout}.{:type}.php',
				'partials' => '{:library}/views/{:controller}/{:template}.{:type}.php',
				'template' => $template
			]
		]);

		return $action;
	}]
]]);

/**
 * Add the testing routes. These routes are only connected in non-production environments, and allow
 * browser-based access to the test suite for running unit and integration tests for the Lithium
 * core, as well as your own application and any other loaded plugins or frameworks. Browse to
 * [http://path/to/app/test](/test) to run tests.
 */
if (!Environment::is('production')) {
	Router::connect('/test/{:args}', array('controller' => 'lithium\test\Controller'));
	Router::connect('/test', array('controller' => 'lithium\test\Controller'));
}

/**
 * Sets the default URL.
 *
 * Once logged in, the user is raken to Sylo::index.
 *
 * If the user is not logged in, he is sent to the login screen (Sessions::create)
 *
 * @see: SessionsController::create()
 */
Router::connect('/', ['Sylo::index'], function($request) {
	if (!Auth::check('default')) {
		$location = Router::match('Sessions::create', $request);
		return new Response(compact('location'));
	}

	$request->params['controller'] = 'Sylo';
	$request->params['action'] = 'index';
	return $request;
});

// GET  /login  Sessions::create
// POST  /login  Sessions::create
Router::connect('/login',  ['Sessions::create']);

// GET  /logout  Sessions::destroy
Router::connect('/logout',  ['Sessions::destroy']);


/**
 * Endpoint to show all users for a particular role.
 * GET    /api/role/:role    Roles::users    [N/A]
 */
Router::connect('/{:api:api}/role/{:role:[a-zA-Z]+}', [],
	function($request) {
		if (!$request->is('get')) {
			return false;
		}

		$request->params['controller'] = 'Roles';
		$request->params['action'] = 'users';

		return $request;
	}
);


/**
 * POST    /api/programauthorizations    ProgramAuthorizations::create    [#129]
 * DELETE    /api/programauthorizations    ProgramAuthorizations::destroy    [#129]
 */
Router::connect('/{:api:api}/programauthorizations', [],
	function($request) {
		if (!$request->is('post') && !$request->is('delete')) {
			return false;
		}

		if ($request->is('post')) {
			$request->params['action'] = 'create';
		}

		if ($request->is('delete')) {
			$request->params['action'] = 'destroy';
		}

		$request->params['controller'] = 'ProgramAuthorizations';
		return $request;
	}
);


/**
 * POST    /api/teacherauthorizations    TeacherAuthorizations::create    [#129]
 * DELETE    /api/teacherauthorizations    TeacherAuthorizations::destroy    [#129]
 */
Router::connect('/{:api:api}/teacherauthorizations', [],
	function($request) {
		if (!$request->is('post') && !$request->is('delete')) {
			return false;
		}

		if ($request->is('post')) {
			$request->params['action'] = 'create';
		}

		if ($request->is('delete')) {
			$request->params['action'] = 'destroy';
		}

		$request->params['controller'] = 'TeacherAuthorizations';
		return $request;
	}
);


/**
 * GET    /api/extensioncodes    ExtensionCodes::index    [1.3]
 * @todo GET    /api/programs    Programs::index    [1.1]
 * GET    /api/semesters    Semesters::index    [1.2]
 * GET    /api/users    Users::index    [1.4]
 * POST    /api/extensioncodes    ExtensionCodes::create    [1.3]
 * POST    /api/programs    Programs::create    [1.1]
 * POST    /api/semesters    Semesters::create    [1.2]
 * POST    /api/users    Users::create    [1.4]
 */
Router::connect('/{:api:api}/{:controller:extensioncodes|programs|semesters|users}', [],
	function($request) {
		if (!$request->is('get') && !$request->is('post')) {
			return false;
		}

		if ($request->is('get')) {
			$request->params['action'] = 'index';
		}

		if ($request->is('post')) {
			$request->params['action'] = 'create';
		}

		if ('Extensioncodes' == $request->params['controller']) {
			$request->params['controller'] = 'ExtensionCodes';
		}
		return $request;
	}
);

/**
 * POST    /api/program/:program/modules    Modules::create    [1.1.4]
 * GET    /api/program/:program/modules    Modules::index    [1.1.4]
 */
Router::connect('/{:api:api}/program/{:program:[-_0-9a-zA-Z]+}/{:controller:modules}', [],
	function($request) {
		if (!$request->is('get') && !$request->is('post')) {
			return false;
		}

		if ($request->is('get')) {
			$request->params['action'] = 'index';
		}

		if ($request->is('post')) {
			$request->params['action'] = 'create';
		}

		return $request;
	}
);


/**
 * POST    /api/semester/:semester/program/:program/classes    Classes::create    [1.1.1]
 * GET    /api/semester/:semester/program/:program/classes    Classes::index    [1.1.1]
 */
Router::connect('/{:api:api}/semester/{:semester:[-_0-9a-zA-Z]+}/program/{:program:[-_0-9a-zA-Z]+}/classes', [
		'controller' => 'Classes'
	],
	function($request) {
		if (!$request->is('get') && !$request->is('post')) {
			return false;
		}

		if ($request->is('get')) {
			$request->params['action'] = 'index';
		}

		if ($request->is('post')) {
			$request->params['action'] = 'create';
		}

		return $request;
	}
);


/**
 * GET    /api/semester/:semester/program/:program/class/:class/assignments    Assignments::index    [1.1.1.1]
 * @todo  GET    /api/semester/:semester/program/:program/class/:class/feedback    Feedback::index    [1.1.1.3]
 * GET    /api/semester/:semester/program/:program/class/:class/documentation    Documentation::index    [1.1.1.4]
 * POST    /api/semester/:semester/program/:program/class/:class/assignments    Assignments::create    [1.1.1.1]
 * @todo  POST    /api/semester/:semester/program/:program/class/:class/feedback    Feedback::create    [1.1.1.3]
 * POST    /api/semester/:semester/program/:program/class/:class/documentation    Documentation::create    [1.1.1.4]
 */
Router::connect('/{:api:api}/semester/{:semester:[-_0-9a-zA-Z]+}/program/{:program:[-_0-9a-zA-Z]+}/class/{:class:[-_0-9a-zA-Z]+}/{:controller:assignments|feedback|documentation}', [],
	function($request) {
		if (!$request->is('get') && !$request->is('post')) {
			return false;
		}

		if ($request->is('get')) {
			$request->params['action'] = 'index';
		}

		if ($request->is('post')) {
			$request->params['action'] = 'create';
		}

		return $request;
	}
);


/**
 * GET    /api/semester/:semester/myclasses/:class/assignments    Assignments::index    [1.6.1]
 * @todo  GET    /api/semester/:semester/myclasses/:class/feedback    Feedback::index    [1.6.3]
 * GET    /api/semester/:semester/myclasses/:class/documentation    Documentation::index    [1.6.4]
 * POST    /api/semester/:semester/myclasses/:class/assignments    Assignments::create    [1.6.1]
 * @todo  POST    /api/semester/:semester/myclasses/:class/feedback    Feedback::create    [1.6.3]
 * POST    /api/semester/:semester/myclasses/:class/documentation    Documentation::create    [1.6.4]
 */
Router::connect('/{:api:api}/semester/{:semester:[-_0-9a-zA-Z]+}/myclasses/{:class:[-_0-9a-zA-Z]+}/{:controller:assignments|feedback|documentation}', [],
	function($request) {
		if (!$request->is('get') && !$request->is('post')) {
			return false;
		}

		if ($request->is('get')) {
			$request->params['action'] = 'index';
		}

		if ($request->is('post')) {
			$request->params['action'] = 'create';
		}

		return $request;
	}
);


/**
 * GET    /api/profile    Users::view    [1.5]
 * PUT    /api/profile    Users::edit    [1.5]
 * GET    /profile    Users::view    [1.5]
 * PUT    /profile    Users::edit    [1.5]
 */
Router::connect('/{:api:api}?/profile', [
		'controller' => 'Users',
		'id' => Sessions::id()
	],
	function($request) {
		if (!$request->is('get') && !$request->is('put')) {
			return false;
		}

		if ($request->is('get')) {
			$request->params['action'] = 'profile';
		}

		if ($request->is('put')) {
			$request->params['action'] = 'edit';
		}

		return $request;
	}
);


/**
 * GET    /api/program/:program    Programs::view    [1.1a]
 * PUT    /api/program/:program    Programs::edit    [1.1a]
 * DELETE    /api/program/:program    Programs::destroy    [1.1a]
 */
Router::connect('/{:api:api}/program/{:program:[-_0-9a-zA-Z]+}', [
		'controller' => 'Programs'
	],
	function($request) {
		if (!$request->is('get') && !$request->is('put') && !$request->is('delete')) {
			return false;
		}

		if ($request->is('get')) {
			$request->params['action'] = 'view';
		}

		if ($request->is('put')) {
			$request->params['action'] = 'edit';
		}

		if ($request->is('delete')) {
			$request->params['action'] = 'destroy';
		}

		return $request;
	}
);


/**
 * GET    /api/semester/:semester    Semesters::view    [1.2a]
 * PUT    /api/semester/:semester    Semesters::edit    [1.2a]
 * DELETE    /api/semester/:semester    Semesters::destroy    [1.2a]
 */
Router::connect('/{:api:api}/semester/{:semester:[-_0-9a-zA-Z]+}', [
		'controller' => 'Semesters'
	],
	function($request) {
		if (!$request->is('get') && !$request->is('put') && !$request->is('delete')) {
			return false;
		}

		if ($request->is('get')) {
			$request->params['action'] = 'view';
		}

		if ($request->is('put')) {
			$request->params['action'] = 'edit';
		}

		if ($request->is('delete')) {
			$request->params['action'] = 'destroy';
		}

		return $request;
	}
);


/**
 * GET /api/semester/:semester/program/:program/class/:class    Classes::view    [1.1.1a]
 * PUT /api/semester/:semester/program/:program/class/:class    Classes::edit    [1.1.1a]
 * DELETE /api/semester/:semester/program/:program/class/:class    Classes::destroy    [1.1.1a]
 */
Router::connect('/{:api:api}/semester/{:semester:[-_0-9a-zA-Z]+}/program/{:program:[-_0-9a-zA-Z]+}/class/{:class:[-_0-9a-zA-Z]+}', [
		'controller' => 'Classes'
	],
	function($request) {
		if (!$request->is('get') && !$request->is('put') && !$request->is('delete')) {
			return false;
		}

		if ($request->is('get')) {
			$request->params['action'] = 'view';
		}

		if ($request->is('put')) {
			$request->params['action'] = 'edit';
		}

		if ($request->is('delete')) {
			$request->params['action'] = 'destroy';
		}

		return $request;
	}
);


/**
 * GET /api/program/:program/module/:module    Modules::view    [1.1.4a]
 * PUT /api/program/:program/module/:module    Modules::edit    [1.1.4a]
 * DELETE /api/program/:program/module/:module    Modules::destroy    [1.1.4a]
 */
Router::connect('/{:api:api}/program/{:program:[-_0-9a-zA-Z]+}/module/{:module:[-_0-9a-zA-Z]+}', [
		'controller' => 'Modules'
	],
	function($request) {
		if (!$request->is('get') && !$request->is('put') && !$request->is('delete')) {
			return false;
		}

		if ($request->is('get')) {
			$request->params['action'] = 'view';
		}

		if ($request->is('put')) {
			$request->params['action'] = 'edit';
		}

		if ($request->is('delete')) {
			$request->params['action'] = 'destroy';
		}

		return $request;
	}
);


/**
 * [1.1.1.5] uses this instead of the entry in the URL Structure wiki page.
 *
 * GET /api/semester/:semester/program/:program/class/:class/assignment/:assignment    Assignments::view    [1.1.1.1a]
 * PUT /api/semester/:semester/program/:program/class/:class/assignment/:assignment    Assignments::edit    [1.1.1.1a]
 * DELETE /api/semester/:semester/program/:program/class/:class/assignment/:assignment    Assignments::destroy    [1.1.1.1a]
 */
Router::connect('/{:api:api}/semester/{:semester:[-_0-9a-zA-Z]+}/program/{:program:[-_0-9a-zA-Z]+}/class/{:class:[-_0-9a-zA-Z]+}/assignment/{:assignment:[-_0-9a-zA-Z]+}', [
		'controller' => 'Assignments'
	],
	function($request) {
		if (!$request->is('get') && !$request->is('put') && !$request->is('delete')) {
			return false;
		}

		if ($request->is('get')) {
			$request->params['action'] = 'view';
		}

		if ($request->is('put')) {
			$request->params['action'] = 'edit';
		}

		if ($request->is('delete')) {
			$request->params['action'] = 'destroy';
		}

		return $request;
	}
);


/**
 * GET /api/semester/:semester/program/:program/class/:class/feedback/:id    Feedback::view    [1.1.1.3a]
 * PUT /api/semester/:semester/program/:program/class/:class/feedback/:id    Feedback::edit    [1.1.1.3a]
 * DELETE /api/semester/:semester/program/:program/class/:class/feedback/:id    Feedback::destroy    [1.1.1.3a]
 * GET /api/semester/:semester/program/:program/class/:class/documentation/:id    Documentation::view    [1.1.1.4a]
 * PUT /api/semester/:semester/program/:program/class/:class/documentation/:id    Documentation::edit    [1.1.1.4a]
 * DELETE /api/semester/:semester/program/:program/class/:class/documentation/:id    Documentation::destroy    [1.1.1.4a]
 */
Router::connect('/{:api:api}/semester/{:semester:[-_0-9a-zA-Z]+}/program/{:program:[-_0-9a-zA-Z]+}/class/{:class:[-_0-9a-zA-Z]+}/{:controller:feedback|documentation}/{:id:\d+}', [],
	function($request) {
		if (!$request->is('get') && !$request->is('put') && !$request->is('delete')) {
			return false;
		}

		if ($request->is('get')) {
			$request->params['action'] = 'view';
		}

		if ($request->is('put')) {
			$request->params['action'] = 'edit';
		}

		if ($request->is('delete')) {
			$request->params['action'] = 'destroy';
		}

		return $request;
	}
);


/**
 * GET /api/extensioncode/:id    ExtensionCodes::view    [1.3a]
 * PUT /api/extensioncode/:id    ExtensionCodes::edit    [1.3a]
 * DELETE /api/extensioncode/:id    ExtensionCodes::destroy    [1.3a]
 * GET /api/user/:id    Users::view    [1.4a]
 * PUT /api/user/:id    Users::edit    [1.4a]
 * DELETE /api/user/:id    Users::destroy    [1.4a]
 */
Router::connect('/{:api:api}/{:controller:extensioncode|user}/{:id:\d+}', [],
	function($request) {
		if (!$request->is('get') && !$request->is('put') && !$request->is('delete')) {
			return false;
		}

		if ($request->is('get')) {
			$request->params['action'] = 'view';
		}

		if ($request->is('put')) {
			$request->params['action'] = 'edit';
		}

		if ($request->is('delete')) {
			$request->params['action'] = 'destroy';
		}

		if ('extensioncode' == strtolower($request->params['controller'])) {
			$request->params['controller'] = 'ExtensionCodes';
		}

		if ('user' == strtolower($request->params['controller'])) {
			$request->params['controller'] = 'Users';
		}

		return $request;
	}
);


/**
 * GET    /api/semester/:semester/myclasses/:class/assignment/:assignment    Assignments::view    [1.6.1a]
 * PUT    /api/semester/:semester/myclasses/:class/assignment/:assignment    Assignments::edit    [1.6.1a]
 * DELETE    /api/semester/:semester/myclasses/:class/assignment/:assignment    Assignments::destroy    [1.6.1a]
 */
Router::connect('/{:api:api}/semester/{:semester:[-_0-9a-zA-Z]+}/myclasses/{:class:[-_0-9a-zA-Z]+}/assignment/{:assignment:[-_0-9a-zA-Z]+}', [
		'controller' => 'Assignments'
	],
	function($request) {
		if (!$request->is('get') && !$request->is('put') && !$request->is('delete')) {
			return false;
		}

		if ($request->is('get')) {
			$request->params['action'] = 'view';
		}

		if ($request->is('put')) {
			$request->params['action'] = 'edit';
		}

		if ($request->is('delete')) {
			$request->params['action'] = 'destroy';
		}

		return $request;
	}
);


/**
 * GET    /api/semester/:semester/myclasses/:class/feedback/:id    Feedback::view    [1.6.3a]
 * PUT    /api/semester/:semester/myclasses/:class/feedback/:id    Feedback::edit    [1.6.3a]
 * DELETE    /api/semester/:semester/myclasses/:class/feedback/:id    Feedback::destroy    [1.6.3a]
 * GET    /api/semester/:semester/myclasses/:class/documentation/:id    Documentation::view    [1.6.4a]
 * PUT    /api/semester/:semester/myclasses/:class/documentation/:id    Documentation::edit    [1.6.4a]
 * DELETE    /api/semester/:semester/myclasses/:class/documentation/:id    Documentation::destroy    [1.6.4a]
 */
Router::connect('/{:api:api}/semester/{:semester:[-_0-9a-zA-Z]+}/myclasses/{:class:[-_0-9a-zA-Z]+}/{:controller:feedback|documentation}/{:id:\d+}', [],
	function($request) {
		if (!$request->is('get') && !$request->is('put') && !$request->is('delete')) {
			return false;
		}

		if ($request->is('get')) {
			$request->params['action'] = 'view';
		}

		if ($request->is('put')) {
			$request->params['action'] = 'edit';
		}

		if ($request->is('delete')) {
			$request->params['action'] = 'destroy';
		}

		return $request;
	}
);


/**
 * GET    /api/semesters/current    Semesters::current    [1.2b]
 */
Router::connect('/{:api:api}/{:controller:semesters}/{:action:current}', [],
	function($request) {
		if (!$request->is('get')) {
			return false;
		}
		return $request;
	}
);


/**
 * GET    /api/semester/:semester/myclasses    Classes::index    [1.6]
 */
Router::connect('/{:api:api}/semester/{:semester:[-_0-9a-zA-Z]+}/myclasses', [
		'controller' => 'Classes',
		'action' => 'index'
	],
	function($request) {
		if (!$request->is('get')) {
			return false;
		}
		return $request;
	}
);


/**
* GET    /api/semester/:semester/myclasses/extensioncodes    ExtensionCodes::index    [1.7]
 * GET    /api/semester/:semester/myclasses/submissions    Submissions::index    [1.8]
 */
Router::connect('/{:api:api}/semester/{:semester:[-_0-9a-zA-Z]+}/myclasses/{:controller:extensioncodes|submissions}', [
		'action' => 'index'
	],
	function($request) {
		if (!$request->is('get')) {
			return false;
		}

		if ('extensioncodes' == strtolower($request->params['controller'])) {
			$request->params['controller'] = 'ExtensionCodes';
		}

		return $request;
	}
);


/**
 * GET    /api/semester/:semester/myclasses/:class    Classes::index    [1.6a]
 */
Router::connect('/{:api:api}/semester/{:semester:[-_0-9a-zA-Z]+}/myclasses/{:class:[-_0-9a-zA-Z]+}', [
		'controller' => 'Classes',
		'action' => 'index'
	],
	function($request) {
		if (!$request->is('get')) {
			return false;
		}
		return $request;
	}
);


/**
 * GET    /api/semester/:semester/myclasses/:class/submissions    Submissions::index    [1.6.5]
 */
Router::connect('/{:api:api}/semester/{:semester:[-_0-9a-zA-Z]+}/myclasses/{:class:[-_0-9a-zA-Z]+}/submissions', [
		'controller' => 'Submissions',
		'action' => 'index'
	],
	function($request) {
		if (!$request->is('get')) {
			return false;
		}
		return $request;
	}
);


/**
 *
 * GET    /api/semester/:semester/program/:program/submissions/:student_id    Submissions::index    [1.1.2.1]
 */
Router::connect('/{:api:api}/semester/{:semester:[-_0-9a-zA-Z]+}/program/{:program:[-_0-9a-zA-Z]+}/submissions/{:student_id:\d+}', [
		'controller' => 'Submissions',
		'action' => 'index'
	],
	function($request) {
		if (!$request->is('get')) {
			return false;
		}
		return $request;
	}
);


/**
 * GET    /api/semester/:semester/program/:program/submissions    Submissions::index    [1.1.2]
 * GET    /api/semester/:semester/program/:program/enrollment    Programs::enrollments    [1.1.3]
 */
Router::connect('/{:api:api}/semester/{:semester:[-_0-9a-zA-Z]+}/program/{:program:[-_0-9a-zA-Z]+}/{:controller:submissions|enrollment}', [
		'action' => 'index'
	],
	function($request) {
		if (!$request->is('get')) {
			return false;
		}

		if ('submissions' == strtolower($request->params['controller'])) {
			$request->params['controller'] = 'Submissions';
		}

		if ('enrollment' == strtolower($request->params['controller'])) {
			$request->params['controller'] = 'Programs';
			$request->params['action'] = 'enrollments';
		}

		return $request;
	}
);


/**
 * GET    /api/semester/:semester/myclasses/:class/enrollment    Classes::enrollment    [1.6.2]
 */
Router::connect('/{:api:api}/semester/{:semester:[-_0-9a-zA-Z]+}/myclasses/{:class:[-_0-9a-zA-Z]+}/enrollment', [
		'controller' => 'Classes',
		'action' => 'enrollment'
	],
	function($request) {
		if (!$request->is('get')) {
			return false;
		}

		return $request;
	}
);


/**
 * GET    /api/semester/:semester/myclasses/submissions/:student_id    Submissions::index    [1.6.6.1]
 */
Router::connect('/{:api:api}/semester/{:semester:[-_0-9a-zA-Z]+}/myclasses/{:controller:submissions}/{:student_id:\d+}', [
		'action' => 'index'
	],
	function($request) {
		if (!$request->is('get')) {
			return false;
		}

		return $request;
	}
);


/**
 * GET    /api/semester/:semester/mysubmissions    Submissions::index    [2.2]
 * GET    /api/semester/:semester/myextensioncodes    ExtensionCodes::index    [2.3]
 * GET    /api/semester/:semester/enrolledclasses    Classes::index    [2.5]
 */
Router::connect('/{:api:api}/semester/{:semester:[-_0-9a-zA-Z]+}/{:controller:mysubmissions|myextensioncodes|enrolledclasses}', [
		'action' => 'index'
	],
	function($request) {
		if (!$request->is('get')) {
			return false;
		}

		if ('mysubmissions' == strtolower($request->params['controller'])) {
			$request->params['controller'] = 'Submissions';
		}

		if ('myextensioncodes' == strtolower($request->params['controller'])) {
			$request->params['controller'] = 'ExtensionCodes';
		}

		if ('enrolledclasses' == strtolower($request->params['controller'])) {
			$request->params['controller'] = 'Classes';
		}

		return $request;
	}
);


/**
 * GET    /api/teacher/:user_id/classes    Classes::classes    [3.1]
 * GET    /api/student/:user_id/classes    Classes::classes    [3.2]
 */
Router::connect('/{:api:api}/{:role:teacher|student}/{:user_id:\d+}/{:controller:classes}', [
		'action' => 'classes'
	],
	function($request) {
		if (!$request->is('get')) {
			return false;
		}

		return $request;
	}
);


/**
 * GET    /api/semester/:semester/program/:program/class/:class/students    Enrollments::index    [1.1.1.2]
 * POST    /api/semester/:semester/program/:program/class/:class/students    Enrollments::create    [1.1.1.2]
 * DELETE    /api/semester/:semester/program/:program/class/:class/students    Enrollments::destroy    [1.1.1.2]
 */
Router::connect('/{:api:api}/semester/{:semester:[-_0-9a-zA-Z]+}/program/{:program:[-_0-9a-zA-Z]+}/class/{:class:[-_0-9a-zA-Z]+}/students', [
		'controller' => 'Enrollments'
	],
	function($request) {
		if ($request->is('get')) {
			$request->params['action'] = 'index';
		}

		if ($request->is('post')) {
			$request->params['action'] = 'create';
		}

		if ($request->is('delete')) {
			$request->params['action'] = 'destroy';
		}

		return $request;
	}
);

/**
 * GET    /api/semester/:semester/program/:program/class/:class/submissions    Submissions::index    [1.1.1.5]
 */
Router::connect('/{:api:api}/semester/{:semester:[-_0-9a-zA-Z]+}/program/{:program:[-_0-9a-zA-Z]+}/class/{:class:[-_0-9a-zA-Z]+}/{:controller:submissions}', [
		'controller' => 'Submissions'
	],
	function($request) {
		if ($request->is('get')) {
			$request->params['action'] = 'index';
		}

		return $request;
	}
);

/**
 * GET    /api/semester/:semester/program/:program/class/:class/assignments/:assignment/submissions    Submissions::index    [1.1.1.5]
 * POST    /api/emester/:semester/program/:program/class/:class/assignments/:assignment/submissions    Submissions::create    [1.1.1.5]
 */
Router::connect('/{:api:api}/semester/{:semester:[-_0-9a-zA-Z]+}/program/{:program:[-_0-9a-zA-Z]+}/class/{:class:[-_0-9a-zA-Z]+}/assignments/{:assignment:[-_0-9a-zA-Z]+}/{:controller:submissions}', [
		'controller' => 'Submissions'
	],
	function($request) {
		if ($request->is('get')) {
			$request->params['action'] = 'index';
		}

		if ($request->is('post')) {
			$request->params['action'] = 'create';
		}

		return $request;
	}
);

/**
 * PUT    /api/semester/:semester/program/:program/class/:class/assignments/:assignment/submissions/:submission_id    Submissions::edit    [1.1.1.5]
 * DELETE    /api/semester/:semester/program/:program/class/:class/assignments/:assignment/submissions/:submission_id    Submissions::destroy    [1.1.1.5]
 */
Router::connect('/{:api:api}/semester/{:semester:[-_0-9a-zA-Z]+}/program/{:program:[-_0-9a-zA-Z]+}/class/{:class:[-_0-9a-zA-Z]+}/assignments/{:assignment:[-_0-9a-zA-Z]+}/{:controller:submissions}/{:submission_id:\d+}', [
		'controller' => 'Submissions'
	],
	function($request) {
		if ($request->is('put')) {
			$request->params['action'] = 'edit';
		}

		if ($request->is('delete')) {
			$request->params['action'] = 'destroy';
		}

		return $request;
	}
);


/**
 * POST    /api/submit    Submissions::create    [2.1]
 */
Router::connect('/{:api:api}/submit', [
		'controller' => 'Submissions',
		'action' => 'create'
	],
	function($request) {
		if ($request->is('post') || $request->is('get')) {
			$request->params['action'] = 'create';
		}

		return $request;
	}
);


/**
 * GET    /api/extensioncodes/statuses    ExtensionCodes::statuses
 */
Router::connect('/{:api:api}/extensioncodes/{:action:statuses}', [],
	function($request) {
		if (!$request->is('get')) {
			return false;
		}

		$request->params['controller'] = 'ExtensionCodes';
		return $request;
	}
);
/**
 * GET    /api/extensioncodes/status/:status    ExtensionCodes::status
 */
Router::connect('/{:api:api}/extensioncodes/{:action:status}/{:status:\d+}', [],
	function($request) {
		if (!$request->is('get')) {
			return false;
		}

		$request->params['controller'] = 'ExtensionCodes';
		return $request;
	}
);

/**
 * Any route without the `/api` prefix is sent to the default page.
 */
/*
Router::connect('/{:args}', [], function($request) {
	$where = 'Sessions::create';

	if (Auth::check('default')) {
		$where = Sessions::home();
		if ('student' === Sessions::label()) {
			$where = 'Submissions::create';
		}
	}

	$location = Router::match($where, $request);
	return new Response(compact('location'));
});
*/















/*
// GET  /profile  Users::view
Router::connect('/profile',  [
	'controller' => 'Users',
	'action' => 'view',
	'id' => Sessions::id(),
	'http:method' => 'GET'
]);

// GET  /profile/edit  Users::edit
Router::connect('/profile/edit',  [
	'controller' => 'Users',
	'action' => 'edit',
	'id' => Sessions::id(),
	'http:method' => 'GET'
]);

// PUT  /profile/edit  Users::edit
Router::connect('/profile/edit',  [
	'controller' => 'Users',
	'action' => 'edit',
	'id' => Sessions::id(),
	'http:method' => 'PUT'
]);

// GET  /extensioncodes  ExtensionCodes::index
Router::connect('/extensioncodes',  ['ExtensionCodes::index']);

// GET  /<semester>/myclasses
Router::connect('/myclasses',  ['Classes::index']);

Router::connect('/semester/{:semester:[-_0-9a-zA-Z]+}/{:path:mysubmissions|myextensioncodes}', [],
	function($request) {
		switch($request->params['path']) {
			case 'mysubmissions':
				$request->params['controller'] = 'Submissions';
			break;

			case 'myextensioncodes':
				$request->params['controller'] = 'ExtensionCodes';
			break;
		}
		return $request;
	}
);

// Routes to `/program/:programs/modules`
// Also, redirects from `/:program` to `/:semester/:programs/classes`.
Router::connect('/program/{:program:[-_0-9a-zA-Z]+}/{:args}', [],
	function($request) {
		$defaults = [
			'args' => null
		];
		$params = $request->params + $defaults;
		$program = Programs::find('first', ['conditions' => ['slug' => $params['program']]]);
		if (!$program) {
			return false;
		}

		if ('modules' == reset($params['args'])) {
			$request->params['controller'] = 'Programs';
			$request->params['action'] = 'modules';
			return $request;
		}

		$semester = Semesters::current();
		$location = Router::match("/semester/$semester/program/$program->slug/classes", $request);
		return new Response(compact('location'));
	}
);

// Matches the semester.
// The `semester` can be any 4 digits.
// This could result in a number like `1301` or even `0001`.
// Matches a program name which could be word characters (a-zA-Z0-9_).
// Examples are:
// {{{
// graphic-design101
// graphic-design_101
// graphic-design-101
// media
// vector-designs
// vector_designs
// }}}
// Also matches additional args.
Router::connect('/semester/{:semester:[-_0-9a-zA-Z]+}/program/{:program:[-_0-9a-zA-Z]+}/{:args}', [],
	function($request) {
		$defaults = ['semester' => ''];
		$params = $request->params + $defaults;
		extract($request->params);

		if (!Semesters::find('first', ['conditions' => ['title' => $semester]])) {
			return false;
		}

		if (!Programs::find('first', ['conditions' => ['slug' => $program]])) {
			return false;
		}

		$type = reset($args);
		$request->params['action'] = 'index';

		if (empty($args)) {
			$location = Router::match("/semester/$semester/program/$program/classes", $request);
			return new Response(compact('location'));
		}

		switch($type) {
			case 'submissions':
				$request->params['controller'] = 'Submissions';
			break;

			case 'enrollment':
				$request->params['controller'] = 'Enrollments';
			break;

			case 'classes':
				$request->params['controller'] = 'Classes';
			break;

			default:
				$class = Classes::find('first', ['conditions' => ['slug' => $type]]);
				if (!$class) {
					return false;
				}

				$request->params['class'] = $class->slug;
				$request->params['class_id'] = $class->id;
				if (isset($args[0]) && !isset($args[1])) {
					$location = Router::match("/semester/$semester/program/$program/class/{$class->slug}/assignments", $request);
					return new Response(compact('location'));
				}

				$request->params['controller'] = Inflector::pluralize(Inflector::classify($args[1]));
			break;
		}

		if (2 == count($args)) {
			if ('submissions' == $type) {
				$studentName = $args[1];
				$student = Students::find('first', ['conditions' => ['slug' => $studentName]]);
				if (!$student) {
					return false;
				}
				$request->params['name'] = $studentName;
				$request->params['student_id'] = $student->id;
			}
		}
		return $request;
	}
);

// Matches the semester.
// The `semester` can be any 4 digits.
// This could result in a number like `1301` or even `0001`.
// Ensures the presence for `/myclasses`.
// Also matches additional args.
Router::connect('/semester/{:semester:[-_0-9a-zA-Z]+}/myclasses/{:args}', [],
	function($request) {
		$defaults = ['semester' => ''];
		$params = $request->params + $defaults;
		extract($request->params);

		if (!Semesters::find('first', ['conditions' => ['title' => $semester]])) {
			return false;
		}

		$type = reset($args);
		$request->params['action'] = 'index';

		switch($type) {
			case 'extensioncodes':
				$request->params['controller'] = 'ExtensionCodes';
			break;

			case 'submissions':
				$request->params['controller'] = 'Submissions';
			break;

			default:
				$class = Classes::find('first', ['conditions' => ['slug' => $type]]);
				if (!$class) {
					return false;
				}

				$request->params['class'] = $class->slug;
				$request->params['class_id'] = $class->id;
				if (isset($args[0]) && !isset($args[1])) {
					$location = Router::match("/semester/$semester/myclasses/{$class->slug}/assignments", $request);
					return new Response(compact('location'));
				}
				$request->params['controller'] = Inflector::pluralize(Inflector::classify($args[1]));
				switch($args[1]) {
					case 'enrollment':
						$request->params['controller'] = 'Enrollments';
					break;

					case 'feedback':
						$request->params['controller'] = 'Feedback';
					break;

					case 'documentation':
						$request->params['controller'] = 'Documentation';
					break;
				}
			break;
		}

		if (2 == count($args)) {
			if ('submissions' == $type) {
				$studentName = $args[1];
				$student = Students::find('first', ['conditions' => ['slug' => $studentName]]);
				if (!$student) {
					return false;
				}
				$request->params['name'] = $studentName;
				$request->params['student_id'] = $student->id;
			}
		}
		return $request;
	}
);

// /semester/:semester/myclasses
Router::connect('/semester/{:semester:[-_0-9a-zA-Z]+}/myclasses', [
		'controller' => 'Classes'
	],
	function($request) {
		if (!Semesters::find('first', ['conditions' => ['title' => $request->params['semester']]])) {
			return false;
		}
		return $request;
	}
);

// GET    /collections/validate/email
Router::connect('/{:controller}/{:action:validate}/{:field:\w+}', [
		'http:method' => 'POST'
	],
	function($request) {
		if (!$request->is('post')) {
			return false;
		}
		return $request;
	}
);












// GET    /collections/create    create
Router::connect('/{:controller}/create', [
		'action'      => 'create',
		'http:method' => 'GET'
	],
	function($request) {
		if (!$request->is('get')) {
			return false;
		}
		return $request;
	}
);
// POST    /collections    create (with $_POST data)
Router::connect('/{:controller}', [
		'action'      => 'create',
		'http:method' => 'POST'
	],
	function($request) {
		if (!$request->is('post')) {
			return false;
		}
		return $request;
	}
);

// DELETE    /collections/1    destroy
Router::connect('/{:controller}/{:id:\d+}', [
		'action' => 'destroy',
		'http:method' => 'DELETE'
	],
	function($request) {
		if (!$request->is('delete')) {
			return false;
		}
		return $request;
	}
);

// GET    /collections/1/edit    edit
Router::connect('/{:controller}/{:id:\d+}/edit', [
		'action'      => 'edit',
		'http:method' => 'GET'
	],
	function($request) {
		if (!$request->is('get')) {
			return false;
		}
		return $request;
	}
);
// PUT    /collections/1    edit (with $_POST data)
Router::connect('/{:controller}/{:id:\d+}', [
		'action'      => 'edit',
		'http:method' => 'PUT'
	],
	function($request) {
		if (!$request->is('put')) {
			return false;
		}
		return $request;
	}
);

// GET    /collections/index    index
Router::connect('/{:controller}/index', [
		'action' => 'index',
		'http:method' => 'GET'
	],
	function($request) {
		if (!$request->is('get')) {
			return false;
		}
		return $request;
	}
);

// GET    /collections    index
Router::connect('/{:controller}', [
		'action' => 'index',
		'http:method' => 'GET'
	],
	function($request) {
		if (!$request->is('get')) {
			return false;
		}
		return $request;
	}
);

// GET    /collections/1    view
Router::connect('/{:controller}/{:id:\d+}', [
		'action'      => 'view',
		'http:method' => 'GET'
	],
	function($request) {
		if (!$request->is('get')) {
			return false;
		}
		return $request;
	}
);
*/














// GET    /collections/validate/email    \app\extensions\action\Controller::validate
Router::connect('/{:api:api}/{:controller}/{:action:validate}/{:field:\w+}', [
		'http:method' => 'POST'
	],
	function($request) {
		if (!$request->is('post')) {
			return false;
		}
		return $request;
	}
);

/**
 * ### Database object routes
 *
 * The routes below are used primarily for accessing database objects, where `{:id}` corresponds to
 * the primary key of the database object, and can be accessed in the controller as
 * `$this->request->id`.
 *
 * If you're using a relational database, such as MySQL, SQLite or Postgres, where the primary key
 * is an integer, uncomment the routes below to enable URLs like `/posts/edit/1138`,
 * `/posts/view/1138.json`, etc.
 */
Router::connect('/{:api:api}/{:controller}/{:action}/{:id:\d+}.{:type}', array('id' => null));
Router::connect('/{:api:api}/{:controller}/{:action}/{:id:\d+}');

/**
 * If you're using a document-oriented database, such as CouchDB or MongoDB, or another type of
 * database which uses 24-character hexidecimal values as primary keys, uncomment the routes below.
 */
// Router::connect('/{:controller}/{:action}/{:id:[0-9a-f]{24}}.{:type}', array('id' => null));
// Router::connect('/{:controller}/{:action}/{:id:[0-9a-f]{24}}');

/**
 * Finally, connect the default route. This route acts as a catch-all, intercepting requests in the
 * following forms:
 *
 * - `/foo/bar`: Routes to `FooController::bar()` with no parameters passed.
 * - `/foo/bar/param1/param2`: Routes to `FooController::bar('param1, 'param2')`.
 * - `/foo`: Routes to `FooController::index()`, since `'index'` is assumed to be the action if none
 *   is otherwise specified.
 *
 * In almost all cases, custom routes should be added above this one, since route-matching works in
 * a top-down fashion.
 */
Router::connect('/{:api:api}/{:controller}/{:action}/{:args}');

?>