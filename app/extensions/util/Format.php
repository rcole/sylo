<?php

namespace app\extensions\util;

use lithium\util\Inflector;

class Format {

	/**
	 * Makes $content human readable
	 * 
	 * @see lithium\util\Inflector
	 * @param  string $content Any string to be converted to a human readable format
	 * @return  string  The converted $content
	 */
	public static function humanize($content) {
		return Inflector::humanize($content);
	}
}

?>