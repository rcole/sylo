<?php

namespace app\extensions\util\format;

use DateTimeZone;

class Time extends \app\extensions\util\Format {

    /**
     * Returns an array of human readable timezones that are recognized by PHP.
     *
     * @return array An array of timezones
     */
    public static function timezones() {
        foreach (DateTimeZone::listIdentifiers() as $key => $zone) {
            $timezones[$zone] = static::humanize($zone);
        }
        return $timezones;
    }
}

?>