<?php

namespace app\extensions\helper;

class Debug extends \lithium\template\Helper {

	public static function initialize() {
		ini_set('html_errors', 'On');
		ob_start();
	}

	/**
	 * Takes the output of print_r() and displays in in a prettier way.
	 *
	 * @see      http://php.net/print_r
	 * @param    mixed    $data       The data to output
	 * @param    string   $label      Display label for the output so it's easier
	 *                                to identify the output.
	 * @param    array    $options    For now, it's an array whose key 'style' holds the inline CSS.
	 *                                @see Debug::_output()
	 */
	public static function display($data, $label = null, array $options = array()) {
		self::initialize();

		if (is_bool($data) || is_null($data)) {
			var_dump($data);
		} else {
			print_r($data);
		}

		self::_output($label, $options);
		self::deinitialize();
	}

	/**
	 * Takes the output of var_dump() and displays in in a prettier way.
	 *
	 * @see      http://php.net/var_dump
	 * @param    mixed    $data       The data to output
	 * @param    string   $label      Display label for the output so it's easier
	 *                                to identify the output.
	 * @param    array    $options    For now, it's an array whose key 'style' holds the inline CSS.
	 *                                @see Debug::_output()
	 */
	public static function dump($data, $label = null, array $options = array()) {
		self::initialize();

		var_dump($data);

		self::_output($label, $options);
		self::deinitialize();
	}

	/**
	 * Takes the output of var_export() and displays in in a prettier way.
	 *
	 * @see      http://php.net/var_export
	 * @param    mixed    $data       The data to output
	 * @param    string   $label      Display label for the output so it's easier
	 *                                to identify the output.
	 * @param    array    $options    For now, it's an array whose key 'style' holds the inline CSS.
	 *                                @see Debug::_output()
	 */
	public static function export($data, $label = null, array $options = array()) {
		self::initialize();

		var_export($data);

		self::_output($label, $options);
		self::deinitialize();
	}

	protected static function _output($label, array $options) {
		$options += array(
			'style' => 'style="
				border: 1px solid #000;
				background: #444;
				color: #fff;
				padding: 1em;
				margin: 0.5em;"'
		);

		$label = ($label === null) ? '' : rtrim($label) . ' ';
		$data = ob_get_clean();
		$data = htmlspecialchars($data, ENT_QUOTES);

		echo "
			<div {$options['style']}>
				<code>$label #</code>
				<pre>$data</pre>
			</div>";
	}

	public static function deinitialize() {
		ini_set('html_errors', 'Off');
	}
}
?>