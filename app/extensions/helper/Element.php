<?php

namespace app\extensions\helper;


class Element extends \lithium\template\Helper {
	public function __call($method, $args) {
		$context = $this->_context;
		$vars = isset($args[0])? $args[0]:array();
		$render = function ($type, $context, $method, $vars) {
			return $context->view()->render(array($type => "{$method}"), $vars, $context->request()->params);
		};

		return $render('element', $context, $method, $vars);
	}
}

?>