<?php

namespace app\extensions\helper;

use lithium\core\Environment;

class Appjs extends \lithium\template\Helper {

    // takes a user type and returns a list of scripts to include
    public function tags($user) {
        $scripts = array();
        if(Environment::is('production')){
            array_push($scripts, $user . '.min.js');
        } else {
            array_push($scripts, 'angular.js', 'libs.js', $user . '.js');
        }
        return $scripts; 
    }
}

?>