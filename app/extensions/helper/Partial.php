<?php
namespace app\extensions\helper;

use lithium\template\TemplateException;
use app\extensions\helper\Debug;

class Partial extends \lithium\template\Helper {
    public function __call($method, $args) {
        $context = $this->_context;
        $context->request()->params['controller'] = strtolower($context->request()->params['controller']);
        $vars = isset($args[0]) ? $args[0] : [];
        $render = function ($type, $context, $method, $vars) {
            return $context->view()->render(
                [$type => "{$method}Partial"],
                $vars,
                $context->request()->params
            );
        };

        return $render('partials', $context, $method, $vars);
    }
}

?>
