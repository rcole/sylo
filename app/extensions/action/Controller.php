<?php
/**
 * A layer built over the base controller in order to simplify certain tasks.
 *
 * @author    Housni Yakoob <housni@koobi.co>
 */

namespace app\extensions\action;

use app\models\Sessions;
use li3_flash_message\extensions\storage\FlashMessage;
use lithium\security\validation\RequestToken;
use lithium\util\Inflector;
use lithium\util\Validator;

use app\extensions\helper\Debug;

class Controller extends \lithium\action\Controller {

	protected function _init() {
		$this->_render['negotiate'] = true;

		parent::_init();

		extract($this->request->query);
		$model = 'app\models\\' . $this->_getModelName();
		$query = null;
		if (isset($limit)) {
			$query['limit'] = $limit;
		}
		if (isset($page)) {
			$query['page'] = $page;
		}
		if (!is_null($query)) {
			$model::config(['query' => $query]);
		}

		if (($this->request->is('post') || $this->request->is('put')) && isset($this->request->data['security'])) {
			if (!RequestToken::check($this->request)) {
				$this->flashError('Invalid security token detected.');
				RequestToken::get(['regenerate' => true]);
				$this->redirect($this->request->url, ['exit' => true]);
			}
			unset($this->request->data['security']);
		}
	}

	public function flashInfo($info) {
		return FlashMessage::write('<p class="alert-heading">
					<span class="label label-info">Info</span>
				</p>' . $info,
			'info'
		);
	}

	public function flashSuccess($success) {
		return FlashMessage::write('<p class="alert-heading">
					<span class="label label-success">Success!</span>
				</p>' . $success,
			'success'
		);
	}

	public function flashError($error) {
		return FlashMessage::write('<p class="alert-heading">
					<span class="label label-important">Oops!</span>
				</p>' . $error,
			'error'
		);
	}

	/**
	 * Validates a field against the validation rules within its respective model.
	 *
	 * A POST request must be sent to this action. It matches the route pattern:
	 *     '/{:controller}/{:action:validate}/{:field:\w+}.json'
	 * Therefore, the following is valid:
	 *     /users/validate/email.json
	 * Keep in mind, this must be a POST request and the `json` extension is a part of a
	 * continuiation route.
	 *
	 * `{:field}` would need to match one of the schema fields for the respective model.
	 * This method looks into the corresponding models schema, extracts the rules and performs
	 * a manual validation check.
	 *
	 * @return mixed json_encode() output.
	 */
	final public function validate() {
		$model = 'app\models\\' . $this->_getModelName();
		$object = new $model;
		$field = $this->request->params['field'];
		$rules = [$field => $object->validates[$field]];
		$value = [$field => $this->request->data['validate']];

		$with = $conditions = [];
		if (isset($this->request->data['_relation'])) {
			foreach ($rules as $ruleKey => $rule) {
				foreach ($rule as $eachKey => $eachVal) {
					if (is_numeric($eachKey) && 'unique' == reset($eachVal)) {
						foreach ($this->request->data['_relation'] as $relationKey => $relationVal) {
							$parts = explode('.', $relationKey);
							$matchField = array_pop($parts);
							$with[] = implode('.', $parts);
							$conditions[array_pop($parts) . ".$matchField"] = $relationVal;
						}

						if (!isset($rules[$ruleKey][$eachKey]['options'])) {
							$rules[$ruleKey][$eachKey]['options'] = [];
						}
						$rules[$ruleKey][$eachKey]['options'] += [
							'with' => $with,
							'conditions' => $conditions
						];
					}
				}
			}
		}

		$validation = Validator::check($value, $rules);
		$result['success'] = true;
		if (!empty($validation)) {
			$result['success'] = false;
			$result['reasons'] = array_fill_keys($validation[$field], false);
		}
		return json_encode($result, JSON_PRETTY_PRINT);
	}

	/**
	 * GET /controller/search/q=
	 *
	 * @param [type] $options [description]
	 * @return [type] [description]
	 */
	public function search(array $options = []) {
		$controller = $this->request->controller;
		$defaults = [
			'get' => 'q'
		];
		$options += $defaults;

		$model = 'app\models\\' . $this->_getModelName();

		$$controller = $model::search($this->request->query, $options);

		$this->render([
			'data'     => [
				"$controller"   => $$controller,
				'total'         => count($$controller),
				'search_string' => htmlspecialchars(urldecode($this->request->query[$options['get']]))
			],
			'template' => Sessions::label() . 'Index',
		]);
	}

	protected function _getModelName() {
		return Inflector::pluralize(Inflector::classify($this->request->controller));
	}

	/**
	 * @param  [type]  $dir       [description]
	 * @param  integer $mode      [description]
	 * @param  boolean $recursive [description]
	 * @return [type]             [description]
	 */
	public function makeDirs($dir, $mode = 0777, $recursive = true) {
		$results = [];
		foreach ((array) $dir as $eachDir) {
			if (!is_dir($eachDir)) {
				$results[] = mkdir($eachDir, $mode, $recursive);
			}
		}

		return !in_array(false, $results, true);
	}
}

?>