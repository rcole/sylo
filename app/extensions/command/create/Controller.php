<?php
/**
 * Lithium: the most rad php framework
 *
 * @copyright     Copyright 2012, Union of RAD (http://union-of-rad.org)
 * @license       http://opensource.org/licenses/bsd-license.php The BSD License
 */

namespace app\extensions\command\create;

use lithium\util\Inflector;

/**
 * Generate a Controller class in the `--library` namespace
 *
 * `li3 create controller Posts`
 * `li3 create --library=li3_plugin controller Posts`
 *
 */
class Controller extends \lithium\console\command\create\Controller {

	/**
	 * Get the function prefox names for controller methods.
	 *
	 * Usage:
	 * 		li3 create controller Posts administrators
	 *
	 * This will cause actions to be prefixed with `administrators`.
	 *
	 * Example:
	 * 		public function administrators_index() {
	 *
	 * @param string $request
	 * @return string
	 */
	protected function _prefix($request) {
		if (isset($request->params['args'][0])) {
			return reset($request->params['args']);
		}

		return 'PREFIX';
	}

}

?>
