<?php $this->title('{:title}'); ?>
<h1><?= $t('{:title}') ?></h1>

<div class="table-responsive">
	<table class="table table-striped table-condensed">
			{:datum}
	</table>
</div>