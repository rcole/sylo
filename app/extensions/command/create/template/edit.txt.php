<?php $this->title('Edit {:title}'); ?>

<div>
	<div>
		<h1><?= 'Edit {:title}' ?></h1>
	</div>
	<div>
		<?= $this->form->create(${:singular}, [
			'url' => ['{:name}::edit', 'id' => $this->request()->id],
			'method' => 'put'
		]); ?>
			<fieldset>
				<legend><?= '{:title} Details' ?></legend>
				{:form}
				<div>
					<?= $this->form->submit('Save') ?>
					<input type="reset" value="Reset">
				</div>
			</fieldset>
		<?= $this->form->end(); ?>
	</div>
</div>