<?php $this->title('{:name}'); ?>
<h1><?= '{:name}' ?></h1>
<div class="table-responsive">
	<table class="table table-striped table-condensed">
		<thead>
			<tr>
				{:head}
			</tr>
		</thead>
		<tbody>
			<?php foreach (${:plural} as ${:singular}) : ?>
				<tr>
					{:data}
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
</div>