<?php
	
namespace app\extensions\command;

class MigrateUploads extends \lithium\console\Command {
	
	protected static $_gcsBucket = 'pc-work-upload';
	
	protected static $_ignoreFiles = array('empty', '.DS_Store');
	
	protected static $_uploadsDirectory;
	
	public function _init() {
		static::$_uploadsDirectory = LITHIUM_APP_PATH . '/webroot/uploads';
		
		require(LITHIUM_APP_PATH . '/libraries/feldsam/gsutil.php');
		
		parent::_init();
	}
	
	public function run() {
		$uploadsDirectory = new \RecursiveDirectoryIterator(static::$_uploadsDirectory);
		$iterator = new \RecursiveIteratorIterator($uploadsDirectory);
		
		$directoriesForMigration = array();
		
		foreach($iterator as $file) {
			if(! in_array($file->getPath(), $directoriesForMigration) and $file->isFile() and ! in_array($file->getFilename(), static::$_ignoreFiles) and $file->isReadable() and $file->isWritable()) {
				$directoriesForMigration[] = $file->getPath();
			}
		}
		
		$this->_migrate($directoriesForMigration);
	}
	
	protected function _migrate($arrayOfDirectories) {
		foreach($arrayOfDirectories as $dir) {
			try {
				$result = \Feldsam\Gsutil::copyRecursive($dir, 'gs://' . static::$_gcsBucket . static::_getRelativePath($dir));
				$this->out($result);
				
				// alles ok, so can delete local files
				$result = \Feldsam\Gsutil::removeLocalFiles($dir . '/*');
				$this->out($result);
			}
			catch(\Exception $e) {
				$this->error('Error: ' . $e->getMessage());
			}
		}
	}
	
	protected static function _getRelativePath($dir) {
		return str_replace(static::$_uploadsDirectory, '', $dir);
	}
}