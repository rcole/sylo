<?php
/**
 * A layer built over the base model to add extra functionality for Lithium
 * when it works with li3_doctrine2.
 *
 * @author    Housni Yakoob <housni@koobi.co>
 */

namespace app\extensions\data;

use lithium\security\Password;
use lithium\util\Validator;
use lithium\data\entity\Record;
use lithium\data\collection\RecordSet;

use app\extensions\helper\Debug;

class Model extends \lithium\data\Model {

	public static function __init() {
		/**
		 * Checks to see if a value already exists in the database. In other words, it checks for
		 * uniqueness of the value.
		 * Of the data submitted, if a value for the primary key of its corresponding Model exists, it
		 * will include that as a part of the conditions to check against so that the uniqueness is not
		 * checked against itself.
		 *
		 * `$options['options']` is passed directly into the `find()` method as the second parameter
		 * which means you can use `with`, `conditions`, `fields`, etc. to customize your query.
		 *
		 * {{{
		 * public $validates = [
		 *	'email' => [
		 *		[
		 *			'unique',
		 *			'message' => 'This email is already being used for another account',
		 *			'model' => '\app\models\Users', // the calling model
		 *			'options' => [
		 *				'with' => ['RolesUsers'], // the model to join with
		 *				'conditions' => [ // regular finder conditions
		 *					'Users.first_name' => 'Housni'
		 *					'RolesUsers.role_id' => ['!=' => 1]
		 *				]
		 *				// you can add more finder `$options` here.
		 *			]
		 *		]
		 *	]
		 * ];
		 * }}}
		 */
		Validator::add('unique', function($value, $rule, $options) {
			$defaults = ['options' => []];
			$options += $defaults;

			$model = $options['model'];
			$primary = $model::meta('key');
			$name = $model::meta('name');
			$field = $options['field'];

			if (!isset($options['options']['conditions'])) {
				$options['options']['conditions'] = [];
			}
			$options['options']['conditions'] += ["$name.$field" => $value];
			$finderOptions = $options['options'];

			if (isset($options['values'][$primary]) && !empty($options['values'][$primary])) {
				$finderOptions['conditions']["$name.$primary"] = ['!=' => $options['values'][$primary]];
			}

			$exists = $model::find('count', $finderOptions);
			return ($exists) ? false : true;
		});

		Validator::add('passwordConfirm', function($value, $rule, $options) {
			$with   = $options['with'];
			$field  = $options['field'];

			if ($options['required'] && empty($value)) {
				return false;
			}
			if (!$options['required'] && (
				!array_key_exists($field, $options['values']) ||
				!array_key_exists($with, $options['values'])
			)) {
				return true;
			}

			if ($options['skipEmpty'] && empty($value)) {
				return true;
			}

			if (Password::check($options['values'][$with], $options['values'][$field])) {
				return true;
			}

			return false;
		});

		/**
		 * Adds `created` and `updated` values if those fields exist and they
		 * are not manually set.
		 */
		static::applyFilter('save', function($self, $params, $chain) {
			$datetime = new \DateTime(null, new \DateTimeZone('UTC'));

			if ($params['data']) {
				$params['entity']->set($params['data']);
				$params['data'] = [];
			}

			//do these things only if they don't exist (i.e. on creation of object)
			if (!$params['entity']->exists()) {
				//if 'created' doesn't already exist and is defined in the schema...
				if (!isset($params['entity']->created) && is_array($self::hasField('created'))) {
					$params['entity']->created = $datetime->getTimestamp();
				}
			}

			if (!isset($params['entity']->updated) && is_array($self::hasField('updated'))) {
				$params['entity']->updated = $datetime->getTimestamp();
			}

			$return = $chain->next($self, $params, $chain);

			if (!$return) {
				$params['entity']->set(['_error' => [
					'code' => 1,
					'message' => $params['entity']->errors(),
					'status' => 400
				]]);
			} else {
				$params['entity']->set(['_error' => [
					'code' => 0,
					'message' => '',
					'status' => 200
				]]);
			}

			return $return;
		});

		static::applyFilter('find', function($self, $params, $chain) {
			$return = $chain->next($self, $params, $chain);

			if ('count' == $params['type']) {
				return $return;
			}

			if (isset($params['options']['error']) && false === $params['options']['error']) {
				return $return;
			}

			$error = new \lithium\data\Entity();

			$code = 0;
			$message = '';
			$status = 200;
			if (0 == count($return) || false === $return) {
				$return = new RecordSet(compact('data'));
				$code = 1;
				$message = 'Record not found';
				$status = 400;
			}

			if ('first' == $params['type']) {
				$error->set([
					'code' => $code,
					'message' => $message,
					'status' => $status
				]);
			}

			if ('all' == $params['type']) {
				$error->set(['_error' => [
					'code' => $code,
					'message' => $message,
					'status' => $status
				]]);
			}
			$return->set(['_error' => $error]);

			return $return;
		});
	}

	/**
	 * @todo
	 * @link Consider this http://www.lithium101.com/snippets/50278c5bfdbcf86805000622
	 */
	public static function search($query, array $options) {
		$options += [
			'conditions' => null,
			'fields'     => null,
			'order'      => ['id' => 'DESC'],
			'limit'      => null,
			'page'       => null,
			'with'       => []
		];

		$model = get_called_class();

		$key = $value = null;
		$searchable = array_flip($model::searchable());

		$searchFor = '%' . $query[$options['get']] . '%';

		unset($query[$options['get']]);

		//if no fields are selected, assume all
		if (empty($query)) {
			$query = $searchable;
		}

		$count = 0;
		foreach ($query as $k => $v) {
			//check if fields actually exist in table
			if (in_array($k, array_keys($searchable))) {
				$options['conditions']['or'][$k]['like'] = $searchFor;
			}
		}
		return $model::all($options);
	}

	public static function searchable() {
		$model = get_called_class();

		$searchable = [];
		foreach ($model::schema()->fields() as $key => $val) {
			if (isset($val['searchable'])) {
				$searchable[$val['searchable']] = $key;
			}
		}
		return $searchable;
	}
}

?>