<?php

namespace app\extensions\data;

use app\extensions\helper\Debug;

class Fixture extends \lithium\data\Model {

	public static function model() {
		return static::$_model;
	}

	public static function load($index = null) {
		if (is_numeric($index)) {
			return static::$_fixtures[$index];
		}
		return static::$_fixtures;
	}

}

?>