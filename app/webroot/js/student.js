/*! Sylo - v0.5.3 - 2016-03-13
 * Copyright (c) 2016 Ryan Cole;
 */

// semestersModule.js Starts here 

// move semester routes from classnest and myclassesnest here.

var semestersModule = angular.module('semestersModule', ['smart-table','ngResource']);

semestersModule.config(['$stateProvider','$urlRouterProvider','$locationProvider',function($stateProvider, $urlRouterProvider, $locationProvider){
  
  var semesters = {
    name: "semesters",
    url: "/semesters",
    templateUrl: "app/webroot/tpl/semesterList.tpl.html",
    controller: "semestersController as semesters"
  }, 
  semesterEdit = {
    name: "semesterEdit",
    parent: 'semesters',
    url: "/:semester/edit",
    templateUrl: "app/webroot/tpl/semesterForm.tpl.html",
    controller: "semesterEditController as semForm"
  },
  semesterCreate = {
    name: "semesterCreate",
    parent: 'semesters',
    url: "/create",
    templateUrl: "app/webroot/tpl/semesterForm.tpl.html",
    controller: "semesterCreateController as semForm"
  };

  $stateProvider
	.state(semesters)
	.state(semesterEdit)
	.state(semesterCreate);

}]);
// start semestersController.js

semestersModule.controller('semesterCreateController', ['$scope', '$q', '$timeout', '$state', 'semesterFactory', 'messageCenterService', function ($scope, $q, $timeout, $state, semesterFactory, messageCenterService) {
  
    console.log('semesterCreateController instantiated');

    var ctrl = this;

    ctrl.mode = 'create';

    ctrl.modal = true; 

    ctrl.message = '';

    ctrl.createItem = function(){
  
        semesterFactory.create(ctrl.item).then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while saving the semester.", { timeout: 6000 });

                console.log(data);

            } else {

                ctrl.message = 'Semester Created';

                $timeout(function(){

                    $state.go('^', {reload:true});

                }, 1500);

            }

            $scope.$parent.semesters.loadList();

        },

        function(error){

            console.log(error);

        });

    };

}]);

// start semestersController.js

semestersModule.controller('semesterEditController', ['$scope', '$q', '$stateParams', '$state', '$timeout', 'semesterFactory', 'messageCenterService', function ($scope, $q, $stateParams, $state, $timeout, semesterFactory, messageCenterService) {
  
    console.log('semesterEditController instantiated');

    // get semester
    // map to form model
    // flatten to only scope methods

    var ctrl = this;    
    
    ctrl.mode = 'edit';

    ctrl.modal = true; 

    ctrl.deleting = false;

    ctrl.message = '';

    ctrl.toggleDelete = function(){

        ctrl.deleting = !ctrl.deleting;

    };

    ctrl.deleteItem = function(){
  
        semesterFactory.remove($stateParams).then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while deleting the semester.", { timeout: 6000 });

                console.log(data);

            } else {
                
                ctrl.message = 'Semester Deleted';

                $timeout(function(){

                    $state.go('^', {reload:true});

                }, 1500);
            }

            $scope.$parent.semesters.loadList();

        },

        function(error){

            console.log(error);

        });

    };
    
    ctrl.updateItem = function(){
  
        semesterFactory.update($stateParams, ctrl.item).then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while saving the semester.", { timeout: 6000 });

                console.log(data);

            } else {

                ctrl.message = 'Semester Updated';

                $timeout(function(){

                    $state.go('semesterEdit', {semester: data.semester.slug});

                }, 1500);

            }

            $scope.$parent.semesters.loadList();

        },

        function(error){

            console.log(error);

        });

    };

    semesterFactory.get($stateParams).then(function(data){

        if (data._error.code == 1) {

            messageCenterService.add('danger', "Something went wrong while retrieving the semester.", { timeout: 6000 });

        } else {

            ctrl.item = data.semester;

        }

    },

    function(error){

        console.log(error);

    });
  
}]);

// start semestersController.js

semestersModule.controller('semestersController', ['$scope', '$q', 'semesterFactory', 'messageCenterService', function ($scope, $q, semesterFactory, messageCenterService) {
  
    console.log('semestersController instantiated');

    var ctrl = this;

    ctrl.loaded = false;

    // semester list is also done in globals since we use it app wide basically, but also here so we can reload it rationally.

    ctrl.semesterList = [];

    ctrl.loadList = function(){

        semesterFactory.query().then(function(data){

            ctrl.semesterList = data.semesters;

            ctrl.loaded = true;

        }, function (error){
      
            messageCenterService.add('danger', "Something went wrong. Could you try that again?", { timeout: 6000 });
      
        });

    };

    ctrl.loadList();

}]);

// globalModule.js Starts here 

var globals = angular.module('globals', ['ui.router', 'ngQuickDate', 'MessageCenterModule']);

globals.config(['$stateProvider','$urlRouterProvider','$locationProvider', '$provide', '$httpProvider', '$sceDelegateProvider', function($stateProvider, $urlRouterProvider, $locationProvider, $provide, $httpProvider, $sceDelegateProvider){
    
  $urlRouterProvider.otherwise("/");
  $locationProvider.html5Mode(false);
  
  var profile = {
    name: "profile",
    url: "/profile",
    templateUrl: "app/webroot/tpl/profileEdit.tpl.html",
    controller: "profileController as profile"
  },
  logout = {
      name: "logout",
      url: "/logout",
      template: "",
      controller: "logoutController as logout"
  };
  
  $stateProvider.state(profile);
  $stateProvider.state(logout);

  var dateProperties = ['created','updated','begins','ends','expires_date','due'];

  var propsToDelete = ['created','updated'];

    $httpProvider.interceptors.push('httpInterceptor');
    $httpProvider.interceptors.push('errorInterceptor');

    $httpProvider.defaults.transformResponse.push(function transformRes(res){

        // Unix to Date

        var checker = function r(obj) {
                      
            for (var key in obj) {

                if (typeof obj[key] == "object") {

                    r(obj[key]);
                
                } else if (typeof obj[key] != "function"){

                    if (dateProperties.indexOf(key) > -1){

                        obj[key] = new Date(obj[key]*1000);

                    }

                }
        
            }
            
        };

        if (typeof res == "object") {

            // So we don't process html and other non-json responses. 
            // Since this goes after the built-ins we'll have an object in place of json at this point.

            checker(res);
        }
        return res;
    });

    $httpProvider.defaults.transformRequest.unshift(function transformReq(req){
        // Date to unix
        var checker = function r(obj) {
            for (var key in obj) {
                if (propsToDelete.indexOf(key) > -1){
                    delete obj[key];
                } else {
                    if (typeof obj[key] == "object" && !obj[key] instanceof Date) {
                        r(obj[key]);
                    } else if (typeof obj[key] != "function" && obj[key] !== null){
                        if (dateProperties.indexOf(key) > -1){
                            obj[key] = obj[key].valueOf() / 1000;
                        }
                    }
                }
            }
        };
        checker(req);
        return req;
    });

    // this is for the file viewer module
    $sceDelegateProvider.resourceUrlWhitelist([
      'self',
      'http://*.google.com/**'
    ]);
    
}]);
// start globals.js

globals.controller('globalsController', ['$scope', '$location', '$stateParams', '$state', '$rootScope', 'globalsFactory','semesterFactory', 'programsFactory', 'usersFactory', 'profileFactory', 'uvFactory', 'analyticsFactory', 'messageCenterService', function($scope, $location, $stateParams, $state, $rootScope, globalsFactory, semesterFactory, programsFactory, usersFactory, profileFactory, uvFactory, analyticsFactory, messageCenterService){
    // globalsController as globals

    var ctrl = this;

    // still need this until all instances of old select2 plugin are removed.
    ctrl.selectOptions = {allowClear:true };

    ctrl.selectedSemester = semesterFactory.selectedSemester; 
    
    semesterFactory.init($stateParams.semester)

    ctrl.semesterList = [];
    ctrl.selectedSemester = null; 
    ctrl.currentSemester = null;
    
    var semesterUpdater = function(){
        // this gets called from the semester factory whenever the selected sem changes. 
        ctrl.selectedSemester = semesterFactory.selectedSemester; 
        ctrl.semesterList = semesterFactory.semesterList;
        ctrl.currentSemester = semesterFactory.currentSemester;
        ctrl.currentSemesterObj = semesterFactory.currentSemesterObj;
        
        // if the selected semester does not match the one in the current route, then update the view
        if($stateParams.semester && $stateParams.semester != semesterFactory.selectedSemester) {
            $state.go('programView', {semester: semesterFactory.selectedSemester});
        }
        
    };

    ctrl.changeSemester = function(selection){
        semesterFactory.setSelectedSemester(selection);
    };
    
    $rootScope.$on('stateUpdate:semester', semesterUpdater);
    
    ctrl.userInfo = {};

    profileFactory.get().then(function(data){

        // find out if the current user is a teacher
        angular.forEach(data.user.roles_users, function(value, key){
            if (value.role_id == 3) {
                data.user.isTeacher = true;
            }
        });
        // sets up user voice user ID
        uvFactory(data.user);
        analyticsFactory.identify(data.user);
        ctrl.userInfo = data.user;

    }, function(error){
        messageCenterService.add('danger', "Something went wrong. Could you try that again?", { timeout: 6000 });
    });

    ctrl.state = {
        userToolsVisible : false,
        toggleUserTools : function(){
            this.userToolsVisible = !this.userToolsVisible;
        }
    };

    $scope.$on('$locationChangeSuccess', function () {
        semesterFactory.init($stateParams.semester);
        ctrl.state.userToolsVisible = false;
    });

    ctrl.teachers = [];

    usersFactory.teachers().then(function (data){
      ctrl.teachers = data.roles;
    }, function (error){
      messageCenterService.add('danger', "Something went wrong. Could you try that again?", { timeout: 6000 });
    });

}]);

// Start nav.js

globals.controller ('navController', ['$scope','$q','$state','$stateParams','programsFactory','globalsFactory', 'messageCenterService', function($scope, $q, $state, $stateParams, programsFactory, globalsFactory, messageCenterService) {

    var ctrl = this;
 
    ctrl.programsListOpen = false;

    ctrl.toggleProgramsList = function(){
 
        ctrl.programsListOpen = !ctrl.programsListOpen; 
  
    };
    
    ctrl.programList = [];

    programsFactory.query().then(function (data){

        ctrl.programList = data.programs;

    }, function (error){

        messageCenterService.add('danger', "Something went wrong. Could you try that again?", { timeout: 6000 });

    });

}]);
// start profile.js

globals.controller('profileController', ['$scope', '$q', '$timeout', 'profileFactory', 'validatorFactory', 'roleClassesFactory', 'messageCenterService', function ($scope, $q, $timeout, profileFactory, validatorFactory, roleClassesFactory, messageCenterService) {
  
    var ctrl = this;

    ctrl.validating = false;

    ctrl.message = '';

    var time, 
        running, 
        fetchDataTimer;

    ctrl.myProfile = {};
  
    profileFactory.get().then(function (data){

        ctrl.myProfile = data.user;

        ctrl.original = angular.copy(ctrl.myProfile);

        getClasses(data.user.id);

    }, function (error){

        messageCenterService.add('danger', "Something went wrong. Could you try that again?", { timeout: 6000 });

    });

    function getClasses(id){

        roleClassesFactory.studentClasses({id: id}).then(function(data){

            ctrl.studentClasses = data.classes;

        });

        roleClassesFactory.teacherClasses({id: id}).then(function(data){

            ctrl.teacherClasses = data.classes;

        });

    }    

    ctrl.validateEmail = function(){

        var mailToCheck = ctrl.myProfile.email;

        $timeout.cancel(fetchDataTimer);

        $scope.myProfileEditForm.email_address.$setValidity('unique', true);
        
        if (mailToCheck){ 

            ctrl.validating = true;

            fetchDataTimer = $timeout(function () {

                validatorFactory('users', 'email', {validate: mailToCheck})

                .then(function(data){

                    // show validation message here with ctrl.emailInvalid == true if not good
                    // set form invalid if mail isn't valid

                    if(data.success === true) {

                        $scope.myProfileEditForm.email_address.$setValidity('unique', true);

                    } else {

                        $scope.myProfileEditForm.email_address.$setValidity('unique', false);

                    }

                    ctrl.validating = false;

                }, function (error){
                
                    messageCenterService.add('danger', "Something went wrong. Could you try that again?", { timeout: 6000 });

                    ctrl.validating = false;
                
                });
            
            }, 800);
            
        }

    };

    ctrl.cancelEdit = function(){ // untested
    
        ctrl.myProfile = angular.copy(ctrl.original);
    
    };

    ctrl.saveProfile = function(){

        profileFactory.update(ctrl.myProfile).then(function (data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while saving yor info.", { timeout: 6000 });

            } else {

                ctrl.passwordEdit = false;

                ctrl.myProfile.password = '';

                ctrl.myProfile.password_again = '';

                ctrl.message = 'Profile Updated';

                $timeout(function(){

                    ctrl.message = '';

                }, 1500);

            }

        }, function (error){

            messageCenterService.add('danger', "Something went wrong. Could you try that again?", { timeout: 6000 });

        });
        
    };

}]);
// start globals.js

globals.controller('studentGlobalsController', ['$scope', '$location', '$stateParams', '$state', 'globalsFactory','semesterFactory', 'programsFactory', 'usersFactory', 'profileFactory', 'uvFactory', 'analyticsFactory', 'messageCenterService', function($scope, $location, $stateParams, $state, globalsFactory, semesterFactory, programsFactory, usersFactory, profileFactory, uvFactory, analyticsFactory, messageCenterService){

    var ctrl = this;

    ctrl.selectOptions = {allowClear:true };

    ctrl.selectedSemester = semesterFactory.selectedSemester; 

    var initSemester = function(){

      if($stateParams.semester){

        semesterFactory.setSelectedSemester($stateParams.semester);

      } else {

        semesterFactory.setSelectedSemester(ctrl.currentSemester); 

      }

    };

    semesterFactory.getCurrentSemester().then(function(data){

      if(data.semester){

        ctrl.currentSemester = data.semester.title;

      } else {

        ctrl.currentSemester = "No Current Semester";

      }

      initSemester();
    
    }, function(error){

      messageCenterService.add('danger', "Something went wrong. Could you try that again?", { timeout: 6000 });      
        
    });
    

    ctrl.userInfo = {};

    profileFactory.get().then(function(data){

        uvFactory(data.user);

        analyticsFactory.identify(data.user);

        ctrl.userInfo = data.user;

    }, function(error){
    
      messageCenterService.add('danger', "Something went wrong. Could you try that again?", { timeout: 6000 });      
    
    });

    ctrl.state = {

        userToolsVisible : false,
    
        toggleUserTools : function(){
  
          this.userToolsVisible = !this.userToolsVisible;
    
        }
    };

    // init on page load only when we have params. Otherwise current semester is not ready yet.

    if($stateParams.semester){

      initSemester();

    }

    $scope.$on('$locationChangeSuccess', function () {

      initSemester();

      ctrl.state.userToolsVisible = false;

    });
    

    ctrl.semesterList = [];

    semesterFactory.query().then(function(data){

      ctrl.semesterList = data.semesters;

    }, function (error){
  
      messageCenterService.add('danger', "Something went wrong. Could you try that again?", { timeout: 6000 });
  
    });

}]);

globals.directive('back', function factory($window) {
 
      return {
 
        restrict   : 'EA',
   
        link: function (scope, element, attrs) {

            element.on('click', function(){

                $window.history.back();

            });
  
        }
 
      };
 
    });
globals.directive('checkSame', ["$document", function ($document) {

	// check-same="lang1 false"
	// first > field to compare with by ID
	// second > true : should match , false : should not match
	
	return {
	
		require: 'ngModel',
	
		link: function (scope, elem, attrs, ctrl) {
	
			var expression = attrs.checkSame,
				firstField = '#' + expression.split(' ')[0],
				match = expression.split(' ')[1];

			var $firstField = angular.element($document[0].querySelector(firstField));
	
			function checkIt(){

				scope.$apply(function () {
		
					var v = elem.val() === $firstField.val();
					
					v = match === 'true' ? v : !v;

					ctrl.$setValidity('match', v);
		
				});

			}

			elem.on('input propertychange', function () {
			
				checkIt();
			
			});

			$firstField.on('input propertychange', function () {
			
				checkIt();
			
			});

		}

	};

}]);
// :p
globals.directive('hideUntilLoaded', [ function () {
        return {
                restrict: 'A',

                scope: {},

                transclude: true,

                template:   '<div class="loadingSpinner"></div>' +
                            '<ng-transclude class="ng-transclude"></ng-transclude>'
        };
}]);
globals.directive("loader", ['$rootScope', '$timeout', function ($rootScope, $timeout) {
   
    return function ($scope, element, attrs) {

        var time, 
        running, 
        fetchDataTimer;

        var $icon = element.find('path');

        $scope.$on("loader_show", function () {

            fetchDataTimer = $timeout(function () {

                // element.addClass('loading');

                $scope.endLoader();
            
            }, 800);
   
        });
   
        $scope.$on("loader_hide", function () {

            $icon.bind("animationiteration webkitAnimationIteration MSAnimationIteration oAnimationIteration", function(){
                
                $icon.unbind();

                // element.removeClass('loading');
            
            });

            $timeout.cancel(fetchDataTimer);
      
        });

        $scope.endLoader = function(){

            $timeout(function () {

                $timeout.cancel(fetchDataTimer);

                $icon.bind("animationiteration webkitAnimationIteration MSAnimationIteration oAnimationIteration", function(){
                
                    $icon.unbind();

                    // element.removeClass('loading');

                });
                        
            }, 10000);

        };
   
    };

}]);
globals.directive('modalBack', ['$window', '$state', '$document', function factory($window, $state, $document) {
 
      return {
 
        restrict   : 'EA',
   
        link: function (scope, element, attrs) {

            var elsewhere = true;

            var clickElsewhere = function() {
            
                if (elsewhere) {
            
                    // go back here

                    $state.go('^');
            
                    $document.off('click', clickElsewhere);
            
                }
            
                elsewhere = true;
            
            };
        
            element.on('click', function(e){

                // catch clicks here as to not close the modal.
        
                elsewhere = false;
            
                $document.off('click', clickElsewhere);
            
                $document.on('click', clickElsewhere);
        
            });
  
        }
 
      };
 
    }]);
globals.directive('personMenu', ['usersFactory', '$document', function (usersFactory, $document) {
		return {
				restrict: 'A',

				scope: {pID : '=personMenu'},
				
				link: function (scope, element, attrs) {

					var elsewhere = true;

					var clickElsewhere = function() {
					
						if (elsewhere) {
					
							element.children().removeClass('personMenuOpen');
					
							$document.off('click', clickElsewhere);
					
						}
					
						elsewhere = true;
					
					};
				
					element.on('click', function(e){
				
						element.children().addClass('personMenuOpen');

						elsewhere = false;
					
						$document.off('click', clickElsewhere);
					
						$document.on('click', clickElsewhere);
				
					});

					usersFactory.get(scope.pID).then(function(result){

						if(result){

							scope.fullName = result.user.first_name + ' ' + result.user.last_name;

							scope.email = result.user.email;

						} else {

							element.off('click');

							scope.fullName = scope.pID;

							scope.email = '';

						}

					});
				
				},

				templateUrl: "app/webroot/tpl/part.personMenu.tpl.html"
		};
}]);
// start extensionsCodesFactories.js

globals.factory('extensioncodesFactory', ['$http', '$resource', '$q', function ($http, $resource, $q) {

  var extcodesResource = $resource('api/extensioncodes.json', {}, 

    {

      'query': {method: 'GET', isArray: false},

      'create': {method: 'POST'}

    });

  var extcodeResource = $resource('api/extensioncode/:id.json',  {id:'@id'},

    {

      'get': {method: 'GET', isArray: false},

      'update': {method: 'POST'},

      'remove': {method: 'POST'}

    }); 

  var factory = {
    
    // add promises
    query : function () {

      var deferred = $q.defer();

      extcodesResource.query({},

        function (resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    },

    get : function (params) {
  
      var deferred = $q.defer();
  
      extcodeResource.get({id: params.id},
        function (resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });
  
      return deferred.promise;
  
    },
  
    create : function (payload) {
  
      var deferred = $q.defer();
  
      extcodesResource.create(payload, 
        function(resp){
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;
  
    },
  
    update : function (params, payload) {
  
      var deferred = $q.defer();
      payload._method = 'put';

      delete payload.id;
      delete payload.created;
      delete payload.updated;

      extcodeResource.update({id: params.id}, payload, 
        function(resp){
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;
  
    },
  
    remove : function (params) {
  
      var deferred = $q.defer(),
          payload = { _method: 'delete' };
  
      extcodeResource.remove({id: params.id}, payload, 
        function(resp){
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;
  
    }
  
  };

  return factory;

}]);
globals.factory('globalsFactory', [ function () {

    // this is mostly a collection of rando util type functions for use globally
    // will probably split this up eventually
    
    var factory = {

      // takes a program or class slug and strips out dashes
      slugCleaner : function(slug){

        var clean;
        
        if(slug) {

          clean = slug.replace(/-/g, " ");

        } else {

          clean = "";

        }
     
        return clean;
     
      },

      // could go into the users factory automatically on query or as middleware in type builders
      makeFullName : function(data){
     
        angular.forEach(data.users, function(value, key){
     
          value.full_name = value.first_name + ' ' + value.last_name;
     
        });
     
      }
    
    };

    return factory;
}]);
globals.factory('httpInterceptor', ['$q', '$rootScope', function ($q, $rootScope) {

    var numLoadings = 0;

    return {
        request: function (request) {
            // Show loader
            
            numLoadings++;
            
            $rootScope.$broadcast("loader_show");
            
            return request || $q.when(request);

        },
        response: function (response) {

            if ((--numLoadings) === 0) {
                // Hide loader
            
                $rootScope.$broadcast("loader_hide");
            }

            return response || $q.when(response);

        },
        responseError: function (response) {

            if ((--numLoadings) === 0) {
                // Hide loader
             
                $rootScope.$broadcast("loader_hide");
            
            }

            return $q.reject(response);
        }
    };
}]);

globals.factory('errorInterceptor', ['$q', '$rootScope', function ($q, $rootScope) {

    var isJson = function(url){
        var splits = url.split('.');
        return splits[splits.length-1] === 'json';
    };
    
    return {
        response: function (response) {
            if(response.data._error !== undefined){
                if(isJson(response.config.url)){
                    var errObj;
                    if(response.data._error._error !== undefined){
                        errObj = response.data._error._error;
                    } else {
                        errObj = response.data._error;
                    }

                    if(errObj.code === 1 && errObj.status >= 450){
                        // do a heap error log
                        analyticsFactory.logError('error: 1 :' + response.config.url);
                        console.warn('error: 1 :', errObj.code, errObj.message, errObj.status, response.config.url);
                        return $q.reject(response);
                    }
                }
            } else if(isJson(response.config.url)){
                // do a heap error log
                analyticsFactory.logError('error: 2 :' + response.config.url);
                console.warn('error: 2 :', response.data);
                return $q.reject(response);
            }

            return response || $q.when(response);

        },
        responseError: function (response) {
            analyticsFactory.logError('error: 3 :' + response.config.url);
            console.log('error: 3 :', response);
            // do a heap error log
            return $q.reject(response);
        }
    };
}]);

globals.factory('profileFactory', ['$http', '$resource', '$q', function ($http, $resource, $q) {
  

  var profileResource = $resource('api/profile.json', {}, 

  {
  
    'get': {method: 'GET', isArray: false, cache: true},

    'update': {method: 'POST'}
  
  });


  var factory = {

    // add promises
    get : function () {

      var deferred = $q.defer();

      profileResource.get({},
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    },

    update : function (payload) {

      payload._method = 'put';

      var deferred = $q.defer();

      profileResource.update({}, payload, 
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    }
  
  };

  return factory;

}]);
// programsFactory.js

globals.factory('programsFactory', ['$http','$resource','$q', function($http, $resource, $q) {
  

  var programsResource = $resource('api/programs.json', {},
    
    { 
    
      'query': {method: 'GET', isArray: false, cache: false},

      'create': {method: 'POST'} 
    
    });


  var programResource = $resource('api/program/:program.json', {program: '@program'},
    
    {
    
      'get': {method: 'GET'},
    
      'update': {method: 'POST'},
    
      'remove': {method: 'DELETE'} 
    
    });

  var authorizationsResource = $resource('api/programauthorizations.json',

    {},

    {

      'add': {method: 'POST'},

      'remove': {method: 'POST'} 

    });


  var factory = {

    query : function () {

      var deferred = $q.defer();

      programsResource.query({},
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    },

    get : function (params) {

      var deferred = $q.defer();

      programResource.get({program: params.program},
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    },

    create : function (payload) {

      var deferred = $q.defer();

      programsResource.create(payload, 
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    },

    update : function (params, payload) {

      payload._method = 'put';

      var deferred = $q.defer();

      programResource.update({program: params.program}, payload, 
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    },

    remove : function (params) {

      var deferred = $q.defer();

      var payload = { _method: 'delete' };

      programResource.remove({program: params.program}, payload, 
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    },
    addAuth : function (payload) {

      payload._method = 'put';

      var deferred = $q.defer();

      authorizationsResource.add({}, payload, 
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    },
    removeAuth : function (payload) {

      payload._method = 'delete';

      var deferred = $q.defer();

      authorizationsResource.remove({}, payload, 
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    }

  };
    
  return factory;

}]);
    // start semesterf.js

globals.factory('semesterFactory', ['$http', '$resource', '$q', '$rootScope', 'messageCenterService', function ($http, $resource, $q, $rootScope, messageCenterService) {
        
        // currentSemester 
        // selectedSemester
        // semesterList
        // setSemester() || 'current'
            // ==> selectCurrentSemester()
        // get 
        // create 
        // update
        // remove 
        // query 
        // init
            // query()
                // puts to semesterList
            // get and set current semester , or null
            // sets selectedSemester based on arg
        
        // order of sem loading
            // stateparams
            // current
            // last ended
            
    var semestersResource, semesterResource, currentSemesterResource, f = {};

    semestersResource = $resource('api/semesters.json', {},
        {
            'query': { method: 'GET', isArray: false, cache: false },
            'create': {method: 'POST'} 
        });

    semesterResource = $resource('api/semester/:semester.json', {semester:'@semester'},
        {
            'get': {method: 'GET'},
            'update': {method: 'POST'},
            'remove': {method: 'POST'}
        }); 

    currentSemesterResource = $resource('api/semesters/current.json', {}, 
        {
            'query': {method: 'GET', isArray: false, cache: true} 
        });

    f.init = function(semesterParam){
        async.parallel([
            function(cb) {
                f.query()
                    .then(function(data){
                        cb(null, data);
                    }, function(err){
                        cb(err, null);
                    });
            },
            function(cb) {
                f.getCurrentSemester()
                    .then(function(data){
                        cb(null, data);
                    }, function(err){
                        cb(err, null);
                    });
            }],
            function(err, results) {
                if (err){
                    messageCenterService.add('danger', "Error retrieving semesters. Could you try that again?", { timeout: 6000 });
                } else {
                    f.semesterList = results[0].semesters;
                    f.currentSemester = results[1].semester.title;
                    f.currentSemesterObj = results[1].semester;
                    if(semesterParam){
                        f.selectedSemester = semesterParam;
                    } else if(f.currentSemester){
                        f.selectedSemester = f.currentSemester;
                    } else {
                        f.selectedSemester = f.semesterList[f.semesterList.length - 1].title;
                    }
                    $rootScope.$broadcast('stateUpdate:semester');
                }
            });
    };
    
    f.semesterList = [];
    
    f.selectedSemester = null;
    
    f.setSelectedSemester = function(s){
        
        if(s === 'current'){
             
        } else {
            f.selectedSemester = s;
        }
        
        $rootScope.$broadcast('stateUpdate:semester');
    
    };
    
    f.query = function () {
    
        // returns list of all semesters
        
        var deferred = $q.defer();
        
        semestersResource.query({}, 
            function(resp) {
            deferred.resolve(resp);
            },
            function(resp){
            deferred.reject(resp);
            });
        
        return deferred.promise;
    
    };
   
    f.get = function (params) {
 
        var deferred = $q.defer();
    
        semesterResource.get({semester: params.semester},
            function(resp) {
            deferred.resolve(resp);
            },
            function(resp){
            deferred.reject(resp);
            });
    
        return deferred.promise;
 
    };
 
    f.create = function (payload) {

        var deferred = $q.defer();
    
        semestersResource.create(payload, 
            function(resp) {
            deferred.resolve(resp);
            },
            function(resp){
            deferred.reject(resp);
            });

        return deferred.promise;
 
    };
 
    f.update = function (params, payload) {
        var deferred = $q.defer();
        payload._method = 'put';
    
        semesterResource.update({semester: params.semester}, payload, 
            function(resp) {
                deferred.resolve(resp);
            },
            function(resp){
                deferred.reject(resp);
            });

        return deferred.promise;
 
    };
 
    f.remove = function (params) {
        var deferred = $q.defer();
        var payload = { _method: 'delete' };
   
        semesterResource.remove({semester: params.semester}, payload, 
            function(resp) {
                deferred.resolve(resp);
            },
            function(resp){
                deferred.reject(resp);
            });

        return deferred.promise; 
    };

        // returns currently active semester
    f.getCurrentSemester = function(){   
        var deferred = $q.defer();
    
        currentSemesterResource.query({},
            function(resp) {
                deferred.resolve(resp);
            },
            function(resp){
                deferred.reject(resp);
            });
    
        return deferred.promise;
   
    };
  
    return f;

}]);
// usersFactories.js

globals.factory('usersFactory', ['$http', '$resource', '$q', 'messageCenterService', function ($http, $resource, $q, messageCenterService) {
  
  var usersResource = $resource('api/users.json', {},
   
    { 
      
      'query' : {method: 'GET', isArray: false},

      'create': {method: 'POST'} 

    });

  var userResource = $resource('api/user/:id.json', {id: '@id'},
    
    {
    
      'get': {method: 'GET', isArray: false, cache: true},      
    
      'update': {method: 'POST'},
    
      'remove': {method: 'POST'}
    
    });

  var roleResource = $resource('api/role/:role.json', {role: '@role'},
   
    { 
      
      'query' : {method: 'GET', isArray: false}

    });


  var factory = {

    query : function () {
    
      var deferred = $q.defer();
    
      usersResource.query({},
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });
    
      return deferred.promise;
    
    },
    
    get: function (params) {

      var deferred = $q.defer();

      // this is to allow for either only an ID string or an object containing an ID property to call the factory

      var ID;
      
      if (params.hasOwnProperty('user')) {

        ID = params.user;

      } else {

        ID = params;

      }

      userResource.get({id: ID},
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;
  
    },
  
    create : function (payload) {
  
      var deferred = $q.defer();
  
      usersResource.create(payload, 
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;
  
    },
  
    update : function (params, payload) {
  
      payload._method = 'put';
  
      var deferred = $q.defer();
  
      userResource.update({id: params.user}, payload, 
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;
  
    },
  
    remove : function (params) {
  
      var payload = { _method: 'delete' };

      var deferred = $q.defer();
  
      userResource.remove({id: params.user}, payload, 
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;
  
    },

    teachers : function(){

      var deferred = $q.defer();
  
      roleResource.query({role: 'teacher'}, 
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    },

    programLeaders : function(){

      var deferred = $q.defer();
  
      roleResource.query({role: 'prgLead'}, 
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    },

    admins : function(){

      var deferred = $q.defer();
  
      roleResource.query({role: 'admin'}, 
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    },

    students : function(){

      var deferred = $q.defer();
  
      roleResource.query({role: 'student'}, 
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    },

    academicServices : function(){

      var deferred = $q.defer();
  
      roleResource.query({role: 'acdServ'}, 
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    },

    externalVerifier : function(){

      var deferred = $q.defer();
  
      roleResource.query({role: 'extVer'}, 
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    },

    studentRecords : function(){

      var deferred = $q.defer();
  
      roleResource.query({role: 'studRec'}, 
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;
    
    }

  };


  function buildUserLists(callback){

    listsBuilt = true;

    factory.query().then(function (data){
  
      angular.forEach(data.users, function(value, key){

        var rolesForAUser = [];

        angular.forEach(value.roles_users, function(value, key){

          // pack all roles for each user into an array
          rolesForAUser.push(value.role_id);

        });
        
        // check array for each role ID and push it into the correct array list.
        if(rolesForAUser.indexOf("1") != -1) admins.push(value);
        if(rolesForAUser.indexOf("2") != -1) programLeaders.push(value);
        if(rolesForAUser.indexOf("3") != -1) teachers.push(value);
        if(rolesForAUser.indexOf("4") != -1) academicServices.push(value);
        if(rolesForAUser.indexOf("5") != -1) externalVerifier.push(value);
        if(rolesForAUser.indexOf("6") != -1) studentRecords.push(value);
        if(rolesForAUser.indexOf("7") != -1) students.push(value);
    
      });
      
      return callback;
  
    }, function (error){
  
      messageCenterService.add('danger', "Something went wrong. Could you try that again?", { timeout: 6000 });
  
    });

  }

  return factory;

}]);
// start validatorFactories.js

globals.factory('validatorFactory', ['$http', '$resource', '$q', function ($http, $resource, $q) {
  
  var validatorResource = $resource('api/:collection/validate/:field.json',

    {

      collection : '@collection', 

      field : '@field' 
    
    },
    
    {

      'validate': {method: 'POST'} 

    });

  var factory = function (col, fld, payload) {
    
      // validate some data
    
      var deferred = $q.defer();
    
      validatorResource.validate({collection : col, field : fld}, payload, 
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });
    
      return deferred.promise;
         
  };

  // add methods for getting and setting selected semester
  
  return factory;

}]);
// globalConfig.js

globals.constant('globalConfig', {

    ver: '1.0', 

    longDateFormat: 'DD, d  MM, yy',

    shortDateFormat: ''

  });

// submitModule.js Starts here 

var submitModule = angular.module('submitModule', ['smart-table','ngResource','flow','ui.select']);

submitModule.config(['$stateProvider','$urlRouterProvider','$locationProvider','flowFactoryProvider',function($stateProvider, $urlRouterProvider, $locationProvider,flowFactoryProvider){
  
    var submit = {
        name: "submit",
        url: "/submit",
        templateUrl: "app/webroot/tpl/submitForm.tpl.html",
        controller: "submitController as submit"
    },
    mySubmissions = {
        name: "mysubmissions",
        url: "/mysubmissions",
        templateUrl: "app/webroot/tpl/mySubmissions.tpl.html",
        controller: "mySubmissionsController as mysubmits"
    };

    $stateProvider.state(submit);
    $stateProvider.state(mySubmissions);

    flowFactoryProvider.defaults = {
        target: 'api/submit.json',
        permanentErrors: [404, 415, 500, 501],
        maxChunkRetries: 1,
        chunkRetryInterval: 5000,
        simultaneousUploads: 4
    };

    flowFactoryProvider.on('filesAdded', function ($files) {

    });

}]);

submitModule.constant('ignoredFileTypes', ['.DS_Store', '.localized', 'Thumbs.db', '.dropbox.cache', '.dropbox', 'Icon']);


// start submitController.js

submitModule.controller('mySubmissionsController', ['$scope', '$q', '$interval', 'submitFactory', 'messageCenterService', function ($scope, $q, $interval, submitFactory, messageCenterService) {

    console.log('mySubmissionsController instantiated');

    var ctrl = this;

    ctrl.subsList = [];

    ctrl.subsDisplayList = [];

    var trySemester = $interval(function(){

        if($scope.$parent.globals.currentSemester) {
   
            getSubs();
       
            stopTrying();
            
        }

    }, 100);

    function stopTrying(){

        $interval.cancel(trySemester);

        trySemester = undefined;

    }

    function getSubs(){

        submitFactory.mySubmissions({semester: $scope.$parent.globals.currentSemester})

        .then(function (data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while getting your submissions.", { timeout: 6000 });

            } else {

                ctrl.subsList = data.submissions;

                ctrl.subsDisplayList = [].concat(ctrl.subsList);

                ctrl.loading = false;
            }

        }, 

        function (error){

            console.log('Error');

            console.log(error);

        });

    }




}]);

// start submitController.js

// States:
// no assignment chosen
// assignment chosen
// files added for submission
// submission complete
// submission error

submitModule.controller('submitController', ['$scope', '$q', '$timeout', 'messageCenterService', 'studentFactory', 'ignoredFileTypes', 'analyticsFactory', function ($scope, $q, $timeout, messageCenterService, studentFactory, ignoredFileTypes, analyticsFactory) {
  
    console.log('submitController instantiated');

    var ctrl = this;

    ctrl.submissionComplete = false;

    ctrl.assignmentChosen = false;

    ctrl.hasFiles = false;

    ctrl.hasBeenWarned = false;

    ctrl.showDrop = function(){

        if (ctrl.assignmentChosen && !ctrl.submissionComplete) return true;

        return false;

    };

    ctrl.showTable = function(){

        if (ctrl.hasFiles) return true;

        return false;

    };

    ctrl.showStatement = function(){

        if(ctrl.hasFiles && !ctrl.submissionComplete) return true;

        return false;

    };

    ctrl.currentClasses = [];

    ctrl.selectClass = function(){

        ctrl.assignmentChosen = true;

    };

    (function myLoop (i) { 
        // we have to wait until we recieve the student's ID before we can get a class list
        
        setTimeout(function () {   
                
            if ($scope.globals.userInfo === {} && --i) { 

                myLoop(i);
            
            } else {
            
                getStudentClasses($scope.globals.userInfo.id);
            
            }
        
        }, 1000);

    })(10);  



    function getStudentClasses(stuID){

        studentFactory.query({studentId: stuID})

        .then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong getting the class list.", { timeout: 6000 });

            } else {

                ctrl.currentClasses = data.classes;

                var c = data.classes;
               
                var a = [];

                for (var i = c.length - 1; i >= 0; i--) {

                    for (var m = c[i].assignments.length - 1; m >= 0; m--) {
                    
                        c[i].assignments[m].classTitle = c[i].title;
                    
                    }

                    a = a.concat(c[i].assignments);
                
                }

                ctrl.currentAssignments = a;

            }

        },

        function(error){

            messageCenterService.add('danger', "Something went wrong getting the class list.", { timeout: 6000 });

        });

    }

    ctrl.groupAssignments = function(assgn){

        return assgn.classTitle;

    };
    
    ctrl.uploadItems = function($flow){
    
        $flow.opts.query = {student_id: $scope.globals.userInfo.id, assignment_id: ctrl.submissionParams};
    
        $flow.upload();

        ctrl.uploadStart = new Date();
    
    };

    ctrl.canUpload = function(){

        if (ctrl.submissionParams && ctrl.agree) {
    
            return true;
    
        } else {

            return false;

        }

    };

    ctrl.newSubmission = function($flow){

        $flow.cancel();

        ctrl.submissionComplete = false;

        ctrl.assignmentChosen = false;

        ctrl.submissionParams = '';

        ctrl.hasFiles = false;

    };

    ctrl.filesAdded = function($flow, $files){

        ctrl.dropClass = '';

        ctrl.hasFiles = true;

        if($files.length > 8 && !ctrl.hasBeenWarned){

            ctrl.hasBeenWarned = true;

            messageCenterService.add('warning', "You are uploading a lot of files. If they are all part of a single work, please compress them first as a .zip file.", {status: 'permanent' });
                    
        }
        var ignored = [];

        angular.forEach($files, function(value, key) {
            if (ignoredFileTypes.indexOf(value.name) > -1 || value.size === 0){
                ignored.push(value.name);
                $flow.removeFile(value);
            }
        });

        if(ignored.length > 0){
            var ignoredString = "";
            for (var i = ignored.length - 1; i >= 0; i--) {
                ignoredString = ignoredString + ignored[i];
                if(i > 0){
                    ignoredString = ignoredString + ', ';
                }
            };
            messageCenterService.add('info', "These files were ignored: " + ignoredString , {status: 'permanent' });
        }
    };

    ctrl.fileRetry = function($file){
        ctrl.uploadMessage = 'Retrying ' + $file.name;
    };

    ctrl.fileError = function($file){
        ctrl.uploadMessage = 'Failed to upload ' + $file.name;
        messageCenterService.add('danger', "Something went wrong uploading your submission! Please check your connection, and try it again. If you keep having issues, contact your instructor.", { status: messageCenterService.status.permanent  });
    };

    var fetchDataTimer;

    ctrl.complete = function($flow){
        $timeout.cancel(fetchDataTimer);
        ctrl.submissionComplete = true;
        ctrl.count = $flow.files.length;
        ctrl.uploadEnd = new Date();
        
        fetchDataTimer = $timeout(function(){
            makeAndSendReport($flow);
        }, 1200);
    };

    function makeAndSendReport(last$flow){
        var fileError = false,
            uploadDuration = (ctrl.uploadEnd - ctrl.uploadStart) / 1000;

        ctrl.erroredFilesDisplay = [];

        for (var i = last$flow.files.length - 1; i >= 0; i--) {
            var report = {};

            if(last$flow.files[i].error) {
                fileError = true;
                ctrl.erroredFilesDisplay.push(last$flow.files[i].name);
                report.fileName = last$flow.files[i].name;
                report.fileSize = last$flow.files[i].size;
                report.fileError = last$flow.files[i].error;
                report.uploadDuration = uploadDuration;
                analyticsFactory.uploadErrors(report);
            } else {
                report.fileName = last$flow.files[i].name;
                report.fileSize = last$flow.files[i].size;
                report.fileError = last$flow.files[i].error;
                report.uploadDuration = uploadDuration;
                analyticsFactory.uploadSuccess(report);
            }      
        }

        if (fileError) {
            ctrl.submissionResult = 'error';
        } else {
            ctrl.submissionResult = 'success';
        }
    }
}]);

// start submitFactory.js

submitModule.factory('submitFactory', ['$http', '$resource', '$q', function ($http, $resource, $q) {
  

  var submitResource = $resource('api/submit.json', {}, { 'create': {method: 'POST'} });

  var mySubmitsResource = $resource('api/semester/:semester/mysubmissions.json', {semester: '@semester'}, { 'query': {method: 'GET'} });

  var factory = {

    // this is unused so far as we are using $flow for uploads.
    submit : function (payload) {

      var deferred = $q.defer();
  
      submitResource.create(payload, 
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;
 
    },
    mySubmissions : function (params) {

      var deferred = $q.defer();
  
      mySubmitsResource.query(params, 
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;
 
    }
  
  };
  
  return factory;

}]);
submitModule.filter('bytes', function() {

  return function(bytes, precision) {

    if (isNaN(parseFloat(bytes)) || !isFinite(bytes)) return '-';

    if (typeof precision === 'undefined') precision = 1;

    var units = ['bytes', 'kB', 'MB', 'GB', 'TB', 'PB'],

      number = Math.floor(Math.log(bytes) / Math.log(1024));

    return (bytes / Math.pow(1024, Math.floor(number))).toFixed(precision) +  ' ' + units[number];

  };

});
// extcodesModule.js Starts here 

var extcodesModule = angular.module('extcodesModule', ['smart-table','ngResource']);

extcodesModule.config(['$stateProvider','$urlRouterProvider','$locationProvider',function($stateProvider, $urlRouterProvider, $locationProvider){

  var extCodes = {
      name: "extCodes",
      url: "/extensioncodes",
      templateUrl: "app/webroot/tpl/extensioncodeList.tpl.html",
      controller: "extensioncodesController as extCodes"
    },
    extCodeEdit = {
      name: "extCodeEdit",
      parent: "extCodes",
      url: "/:id/edit",
      templateUrl: "app/webroot/tpl/extensioncodeForm.tpl.html",
      controller: "extensioncodeEditController as extForm"
    },
    extCodeCreate = {
      name: "extCodeCreate",
      parent: "extCodes",
      url: "/create",
      templateUrl: "app/webroot/tpl/extensioncodeForm.tpl.html",
      controller: "extensioncodeCreateController as extForm"
    };

  $stateProvider
	.state(extCodes)
	.state(extCodeEdit)
	.state(extCodeCreate);

}]);
// start extensionsCodesControllers.js

extcodesModule.controller('extensioncodeCreateController', ['$scope', '$q', '$timeout', '$stateParams', '$state', 'roleClassesFactory', 'extensioncodesFactory', 'usersFactory', 'messageCenterService', function ($scope, $q, $timeout, $stateParams, $state, roleClassesFactory, extensioncodesFactory, usersFactory, messageCenterService) {

    var ctrl = this;
    
    var currentSemester = ctrl.currentSemester; 
    // to get this loaded in time... janky, but otherwise would need nested promises in loadXXX functions
    
    ctrl.mode = 'create';

    ctrl.modal = true; 

    ctrl.message = '';

    ctrl.studentList = [];

    ctrl.prgList = [];

    ctrl.studentAssgns = [];

    ctrl.item = {};

    usersFactory.students().then(function (data){

        ctrl.studentList = data.roles;

    }, function (error){

        messageCenterService.add('danger', "Something went wrong while looking up the student list. Try reloading the page.", { timeout: 6000 });

    });

    ctrl.loadAssignments = function(item){

        ctrl.item.assignment_id = '';

        roleClassesFactory.studentClasses({id: item}).then(function(data){

            var classes =  data.classes;

            var result = [];

            for (var a = classes.length - 1; a >= 0; a--) {
                
                for (var b = classes[a].assignments.length - 1; b >= 0; b--) {
                    
                    result.push(classes[a].assignments[b]);
                
                }

            }

            ctrl.studentAssgns = result;

        },

        function(error){

            messageCenterService.add('danger', "Something went wrong while looking up the assignment list. Try reloading the page.", { timeout: 6000 });

        });

    };
  
    ctrl.createItem = function(){
  
        extensioncodesFactory.create(ctrl.item).then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while saving the extension code.", { timeout: 6000 });

            } else {

                ctrl.message = 'Extension Code Created';

                $timeout(function(){

                    $state.go('^', {reload:true});

                }, 1500);

            }

            $scope.$parent.extCodes.loadList();

        },

        function(error){

            messageCenterService.add('danger', "Something went wrong while creating this extension code. Try again, sorry. I hate myself.", { timeout: 6000 });

        });

    };
  
}]);
// start extensionsCodesControllers.js

extcodesModule.controller('extensioncodeEditController', ['$scope', '$q', '$timeout', '$stateParams', '$state', 'extensioncodesFactory', 'usersFactory', 'roleClassesFactory', 'messageCenterService', function ($scope, $q, $timeout, $stateParams, $state, extensioncodesFactory, usersFactory, roleClassesFactory, messageCenterService) {

    var ctrl = this;

    var currentSemester = ctrl.currentSemester; // to get this loaded in time... janky, but otherwise would need nested promises in loadXXX functions

    ctrl.extList = [];

    ctrl.displayList = [];
    
    ctrl.studentList = [];

    ctrl.mode = 'edit';

    ctrl.modal = true; 

    ctrl.deleting = false;

    ctrl.message = '';

    ctrl.studentAssgns = [];

    ctrl.item = {};

    ctrl.toggleDelete = function(){

        ctrl.deleting = !ctrl.deleting;

    };

    ctrl.deleteItem = function(){
  
        extensioncodesFactory.remove($stateParams).then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while deleting the code.", { timeout: 6000 });

            } else {
                
                ctrl.message = 'Extension Code Deleted';

                $timeout(function(){

                    $state.go('^', {reload:true});

                }, 1500);
            }

            $scope.$parent.extCodes.loadList();

        },

        function(error){

            messageCenterService.add('danger', "Something went wrong while deleting the extension code. Try again.", { timeout: 6000 });

        });

    };
    
    ctrl.updateItem = function(){
  
        extensioncodesFactory.update($stateParams, ctrl.item).then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while saving the code.", { timeout: 6000 });

            } else {

                ctrl.message = 'Extension Code Updated';

                $timeout(function(){

                    ctrl.message = '';

                }, 1500);

            }

            $scope.$parent.extCodes.loadList();

        },

        function(error){

            messageCenterService.add('danger', "Something went wrong while updating this extension code. Try again!", { timeout: 6000 });

        });

    };

    extensioncodesFactory.get($stateParams).then(function(data){

        if (data._error.code == 1) {

            messageCenterService.add('danger', "Something went wrong while retrieving the extension code.", { timeout: 6000 });

        } else {

            ctrl.item = data.extensionCode;

            ctrl.loadAssignments(data.extensionCode.student_id);

        }

    },

    function(error){

        messageCenterService.add('danger', "Something went wrong while looking up the extension codes. Try reloading the page.", { timeout: 6000 });

    });

    usersFactory.students().then(function (data){

        ctrl.studentList = data.roles;

    }, function (error){
    
        messageCenterService.add('danger', "Something went wrong while looking for the students. Try reloading the page, I dunno.", { timeout: 6000 });
    
    });

    ctrl.loadAssignments = function(item){

        ctrl.item.assignment_id = '';

        roleClassesFactory.studentClasses({id: item}).then(function(data){

            var classes =  data.classes;

            var result = [];

            for (var a = classes.length - 1; a >= 0; a--) {
                
                for (var b = classes[a].assignments.length - 1; b >= 0; b--) {
                    
                    result.push(classes[a].assignments[b]);
                
                }

            }

            ctrl.studentAssgns = result;

        },

        function(error){

            messageCenterService.add('danger', "Something went wrong while looking up the assignments. Try reloading the page.", { timeout: 6000 });

        });

    };
      
}]);
// start extensionsCodesControllers.js

extcodesModule.controller('extensioncodesController', ['$scope', '$q', 'extensioncodesFactory', 'programsFactory', 'classesFactory', 'usersFactory', 'classAssignmentFactory', 'messageCenterService', function ($scope, $q, extensioncodesFactory, programsFactory, classesFactory, usersFactory, classAssignmentFactory, messageCenterService) {

    var ctrl = this;

    var currentSemester = ctrl.currentSemester; // to get this loaded in time... janky, but otherwise would need nested promises in loadXXX functions
    
    ctrl.extList = [];

    ctrl.displayList = [];

    ctrl.loaded = false;

    ctrl.loadList = function(){

        extensioncodesFactory.query().then(function(data){

            ctrl.loaded = true;

            ctrl.extList = data.extensionCodes;

            ctrl.displayList = data.extensionCodes;

        }, function (error){

            messageCenterService.add('danger', "Something went wrong. Could you try that again?", { timeout: 6000 });

        });

    };

    ctrl.loadList();

}]);
// stuModule.js Starts here 

var studentApp = angular.module('studentApp' , [
  // uploads, prolly more too
  'globals',
  'semestersModule',
  'flow',
  'submitModule',
  'extcodesModule',
  'MessageCenterModule',
  'uservoiceModule',
  'usersModule',
  'analyticsModule',
  'submissionsListModule'
]);

// don't we need student routes? I think these are not done yet.

studentApp.config(['$stateProvider','$urlRouterProvider','$locationProvider',function($stateProvider, $urlRouterProvider, $locationProvider){
    
  $urlRouterProvider.otherwise("/submit");
  $locationProvider.html5Mode(false);
    
}]);
// studentFactory.js

// this is janky, but only makes sense here I think. Maybe could move to global module, or if profile stuff gets split off it would make sense there.

studentApp.factory('studentFactory', ['$http','$resource','$q', function($http, $resource, $q) {

  var enrolledClassesResource = $resource('api/student/:studentId/classes.json', {studentId: '@studentId'},

    { 'query' : { method: 'GET', isArray: false, cache: true } });

  var factory = {

    query : function (params) {

      var deferred = $q.defer();

      enrolledClassesResource.query({studentId: params.studentId},

        function (resp) {

          deferred.resolve(resp);

        });

      return deferred.promise;

    }

  };

  return factory;

}]);
// usersModule.js Starts here 

var uservoiceModule = angular.module('uservoiceModule', []);

uservoiceModule.config(['$stateProvider','$urlRouterProvider','$locationProvider',function($stateProvider, $urlRouterProvider, $locationProvider){
    
    console.log('user voice facotry called.');

    UserVoice=window.UserVoice||[];

    (function(){

      var uv=document.createElement('script');

      uv.type='text/javascript';

      uv.async=true;

      uv.src='//widget.uservoice.com/PfyxW3kgH950qRwRdgXMeQ.js';

      var s=document.getElementsByTagName('script')[0];

      s.parentNode.insertBefore(uv,s);

    })();

    UserVoice.push(['set', {
      accent_color: '#6aba2e',
      trigger_color: 'white',
      trigger_background_color: '#6aba2e'
    }]);

    UserVoice.push(['addTrigger', { mode: 'contact', trigger_position: 'bottom-left' }]);

    // Autoprompt for Satisfaction and SmartVote (only displayed under certain conditions)
    // UserVoice.push(['autoprompt', {}]);

    // the user info gets filled in when the profile endpoint is called in globals controller by the uvFactory.

}]);
uservoiceModule.factory('uvFactory', [function() {    

    factory = function(profile){

        UserVoice.push(['identify', {
    
            email: profile.email,
    
            name: profile.first_name + ' ' + profile.last_name,
    
            id: profile.id,
    
            type: profile.roles_users[0].role_id
    
        }]);

    };

    return factory;
    
}]);
// usersModule.js Starts here 

var usersModule = angular.module('usersModule', ['smart-table','ngResource']);

usersModule.config(['$stateProvider','$urlRouterProvider','$locationProvider',function($stateProvider, $urlRouterProvider, $locationProvider){
    
    var users = {
        name: "users",
        url: "/users",
        templateUrl: "app/webroot/tpl/userList.tpl.html",
        controller: "usersController as users"
    },
    user = {
        name: "user",
        url: "/user/:user",
        templateUrl: "app/webroot/tpl/part.editUserForm.tpl.html",
        controller: "userController as user",
        parent: users
    },
    editUser = {
        name: "editUser",
        url: "/:user/edit",
        templateUrl: "app/webroot/tpl/userForm.tpl.html",
        controller: "userEditController as userForm",
        parent: 'users'
    },
    createUser = {
        name: "createUser",
        url: "/create",
        templateUrl: "app/webroot/tpl/userForm.tpl.html",
        controller: "userCreateController as userForm",
        parent: 'users'
    },
    importUsers = {
        name: "importUsers",
        url: "/import",
        templateUrl: "app/webroot/tpl/importForm.tpl.html",
        controller: "usersImportController as usersImport",
        parent: 'users'
    };

    $stateProvider
        .state(users)
        .state(user)
        .state(editUser)
        .state(createUser)
        .state(importUsers);

}]);

usersModule.constant('userRoles', [
    {   
        role_id: '1',
        role_title: 'Admin'
    },
    {
        role_id: '2',
        role_title: 'Program Leader'
    },
    {
        role_id: '3',
        role_title: 'Teacher'
    },
    {
        role_id: '4',
        role_title: 'Adacemic Services'
    },
    {
        role_id: '5',
        role_title: 'External Verifier'
    },
    {
        role_id: '6',
        role_title: 'Student Records'
    },
    {
        role_id: '7',
        role_title: 'Student'
    }
]);

usersModule.constant('userStatus', [
    
    { 
        status_id : '1',
        status_title: 'Active'
    },
    { 
        status_id : '2',
        status_title:'Inactive'
    }
    
]);



usersModule.controller('userController', ['ctrl','$q','usersFactory','$stateParams','$state', function (ctrl, $q, usersFactory, $stateParams, $state) {
  
  console.log('singleUserController instantiated');
  

}]);
// usersControllers.js

usersModule.controller('userCreateController', ['$scope', '$state', '$timeout', 'usersFactory', 'messageCenterService', 'userRoles', function ($scope, $state, $timeout, usersFactory, messageCenterService, userRoles) {
  
  console.log('userCreateController instantiated');

  var ctrl = this;

    ctrl.mode = 'create';

    ctrl.modal = true; 

    ctrl.message = '';

    ctrl.passwordEdit = true;

    ctrl.rolesList = userRoles;

    ctrl.createItem = function(){

        ctrl.item.roles_users = [];

        for (var i = ctrl.roles.length - 1; i >= 0; i--) {

            var temp = {
            
                role_id: ctrl.roles[i]
            
            };
            
            ctrl.item.roles_users.push(temp);
        
        }
  
        usersFactory.create(ctrl.item).then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while saving the user.", { timeout: 6000 });

                console.log(data);

            } else {

                ctrl.message = 'User Created';

                $timeout(function(){

                    $state.go('^', {reload:true});

                }, 1500);

            }

            $scope.$parent.users.loadList();

        },

        function(error){

            console.log(error);

        });

    };

}]);
// usersControllers.js

usersModule.controller('userEditController', ['$scope', '$stateParams', '$state', '$timeout', 'usersFactory', 'messageCenterService', 'userRoles', function ($scope, $stateParams, $state, $timeout, usersFactory, messageCenterService, userRoles) {
  
    console.log('userEditController instantiated');

    var ctrl = this;

    ctrl.mode = 'edit';

    ctrl.modal = true; 

    ctrl.deleting = false;

    ctrl.message = '';

    ctrl.rolesList = userRoles;

    ctrl.toggleDelete = function(){

        ctrl.deleting = !ctrl.deleting;

    };

    ctrl.deleteItem = function(){
  
        usersFactory.remove($stateParams).then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while deleting the user.", { timeout: 6000 });

                console.log(data);

            } else {
                
                ctrl.message = 'User Deleted';

                $timeout(function(){

                    $state.go('^', {reload:true});

                }, 1500);
            }

            $scope.$parent.users.loadList();

        },

        function(error){

            console.log(error);

        });

    };
    
    ctrl.updateItem = function(){

        ctrl.item.roles_users = [];

        for (var i = ctrl.roles.length - 1; i >= 0; i--) {

            var temp = {
            
                role_id: ctrl.roles[i],

                user_id: ctrl.item.id
            
            };
            
            ctrl.item.roles_users.push(temp);
        
        }
  
        usersFactory.update($stateParams, ctrl.item).then(function(data){

            if (data._error.code == 1) {

                messageCenterService.add('danger', "Something went wrong while saving the user.", { timeout: 6000 });

                console.log(data);

            } else {

                ctrl.message = 'User Updated';

                $timeout(function(){

                    ctrl.message = '';

                }, 1500);

            }

            $scope.$parent.users.loadList();

        },

        function(error){

            console.log(error);

        });

    };

    usersFactory.get($stateParams).then(function(data){

        if (data._error.code == 1) {

            messageCenterService.add('danger', "Something went wrong while retrieving the user.", { timeout: 6000 });

        } else {

            ctrl.item = data.user;

            ctrl.roles = [];

            for (var i = ctrl.item.roles_users.length - 1; i >= 0; i--) {
          
                ctrl.roles.push(ctrl.item.roles_users[i].role_id);
          
            }

        }

    },

    function(error){

        console.log(error);

    });

}]);
// usersControllers.js

usersModule.controller('usersController', ['$scope', 'usersFactory', 'messageCenterService', function ($scope, usersFactory, messageCenterService) {
  
    console.log('usersController instantiated');

    var ctrl = this;

    ctrl.usersList = [];

    ctrl.displayList = [];

    ctrl.loaded = false;

    ctrl.loadList = function(){

        usersFactory.query().then(function (data){
    
            ctrl.usersList = data.users;

            ctrl.displayList = [].concat(ctrl.usersList);

            ctrl.loaded = true;
      
        }, function (error){
      
            messageCenterService.add('danger', "Something went wrong. Could you try that again?", { timeout: 6000 });
      
        });

    };

    ctrl.loadList();

}]);
// usersControllers.js

usersModule.controller('usersImportController', ['$scope', 'usersFactory', 'messageCenterService', function ($scope, usersFactory, messageCenterService) {
  
  console.log('usersImportController instantiated');

  var ctrl = this;

  var toInject;

  ctrl.checkJson = function(){

    ctrl.jsonStatus = '';
    
    try {
    
        var o = JSON.parse(ctrl.jsonText);

        // Handle non-exception-throwing cases:
        // Neither JSON.parse(false) or JSON.parse(1234) throw errors, hence the type-checking,
        // but... JSON.parse(null) returns 'null', and typeof null === "object", 
        // so we must check for that, too.
    
        if (o && typeof o === "object" && o !== null) {

          toInject = o;

          ctrl.jsonStatus = 'Valid';

          console.log(toInject);
        
        }
    
    }
    
    catch (e) { 

      console.log(e);

      ctrl.jsonStatus = 'Invalid';

    }

  };

  ctrl.importAsJson = function(){

    ctrl.status = [];

    // for mass importing, pareses text to json and imports

    async.each(toInject.users, function(file, callback) {

      // Perform operation on file here.

      ctrl.status.push('Processing user: ' + file.email);

      console.log('Processing user ' + file.email);

      usersFactory.create(file)

      .then(function(data){

        console.log('data: ', data);

          if (data._error.code == 1) {

            ctrl.status.push('ERROR: ' + file.email);

              callback(data._error);

          } else {

              console.log('user processed');

              ctrl.status.push('User processed: ' + file.email);
    
              callback(); 

          }

      },
      function(error){
        console.log(error);
      });
      
    }, function(err){
      
        if( err ) {

          // One of the iterations produced an error.
          
          // All processing will now stop.

          console.log('Something went wrong', err);
       
        } else {
        
          console.log('All files have been processed successfully');

          ctrl.status.push('All users have been imported successfully');
       
        }
    
    });

  };

}]);
// roleClassesFactories.js

usersModule.factory('roleClassesFactory', ['$http', '$resource', '$q', function($http, $resource, $q) {

  var teacherResource = $resource('api/teacher/:id/classes.json', {id: '@id'},
    {
      'get': {
        method: 'GET',
        isArray: false,
        cache: true
      }
    });


  var studentResource = $resource('api/student/:id/classes.json', {id: '@id'},
    {
      'get': {
        method: 'GET',
        isArray: false,
        cache: true
      }
    });


  var factory = {

    teacherClasses: function(params) {

      var deferred = $q.defer(),
          ID;

      if (params.id !== undefined) {
        ID = params.id;
      } else {
        ID = params;
      }

      teacherResource.get({id: ID },
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp) {
          deferred.reject(resp);
        });

      return deferred.promise;

    },

    studentClasses: function(params) {

      var deferred = $q.defer(),
          ID;

      if (typeof(params.id) !== undefined) {
        ID = params.id;
      } else {
        ID = params;
      }

      studentResource.get({id: ID },
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });

      return deferred.promise;

    }

  };

  return factory;

}]);
// usersRolesFilter.js

usersModule.filter('makeUserRole', ['userRoles', function(userRoles) {

    return function(input) {

        for (var i = userRoles.length - 1; i >= 0; i--) {
    
            if (userRoles[i].role_id == input) {
        
                return userRoles[i].role_title;

            }
            
        }

    };

}]);


usersModule.filter('makeUserStatus', ['userStatus', function(userStatus) {

    return function(input) {

        for (var i = userStatus.length - 1; i >= 0; i--) {
    
            if (userStatus[i].status_id == input) {
        
                return userStatus[i].status_title;

            }
            
        }

    };

}]);
// usersModule.js Starts here 

var uservoiceModule = angular.module('analyticsModule', []);

uservoiceModule.config(['$stateProvider','$urlRouterProvider','$locationProvider',function($stateProvider, $urlRouterProvider, $locationProvider){
    
  // maybe do setup here. Right now Heap setup is on each template in the head element. I could probably move it here.

}]);
uservoiceModule.factory('analyticsFactory', [function() {    

    var isLocal = window.location.hostname === 'localhost' || window.location.hostname === 'submiststaging.praguecollege.cz'; // add staging url here as well

    if(isLocal){
        heap.load("2094147294");
    } else {
        heap.load("912591352");
    }

    factory = {
        identify: function(profile){
            if(!isLocal){
                heap.identify({
                    name: profile.first_name + ' ' + profile.last_name,
                    syloId: profile.id,
                    userRole: profile.roles_users[0].role_id,
                    email: profile.email
                });
            }
        },

        uploadErrors : function(report){
            if(!isLocal) heap.track('Upload Errors', report);
        },
        uploadSuccess : function(report){
            if(!isLocal) heap.track('Upload Success', report);
        },
        logError : function (report) { 
            if(!isLocal) heap.track('Ajax Error', report);
         }
    };

    return factory;
    
}]);
// submissionsListModule.js Starts here 

var submissionsListModule = angular.module('submissionsListModule', ['smart-table','ngResource']);
submissionsListModule.directive('fileviewer', ['ngModalDefaults', '$sce', '$location', function(ngModalDefaults, $sce, $location) {
      
      return {
      
        restrict: 'E',

        require: '^submissionList',
      
        scope: {
      
          show: '=',
            
          onClose: '&?',

          subsArray: '=',

          subIndex: '='
      
        },
      
        replace: true,
      
        transclude: true,
      
        link: function(scope, element, attrs) {

          var webNativeFiles = ['jpeg','jpg','png','bmp','gif','tiff','svg','mp3','mpeg','wav','ogg','mp4'];

          var setupCloseButton, setupStyle;

          scope.dialogTitle = '';

          scope.pathToFile = '';

          scope.fileViewerPath = function(){

            var activeSub = scope.subsArray[scope.subIndex];

            var fe = activeSub.name.split('.').pop().toLowerCase();

            if(webNativeFiles.indexOf(fe) >= 0){

              scope.pathToFile = $location.$$protocol + '://' + $location.$$host + '/uploads/' + activeSub.path + activeSub.name;

            } else {

              scope.pathToFile = 'http://drive.google.com/viewer?url=' + $location.$$protocol + '://' + $location.$$host + '/uploads/' + activeSub.path + activeSub.name + '&embedded=true';

            }

          };
      
          setupCloseButton = function() {

            scope.closeButtonHtml = $sce.trustAsHtml(ngModalDefaults.closeButtonHtml);
      
            return scope.closeButtonHtml;
      
          };
      
          setupStyle = function() {
      
            scope.dialogStyle = {};
      
            if (attrs.width) {
      
              scope.dialogStyle.width = attrs.width;
      
            }
      
            if (attrs.height) {

                scope.dialogStyle.height = attrs.height;
      
                return scope.dialogStyle.height;
      
            }
      
          };
      
          scope.hideModal = function() {

            scope.show = false;
      
            return scope.show;
      
          };
      
          scope.$watch('show', function(newVal, oldVal) {
      
            if (newVal && !oldVal) {

              // on

              scope.fileViewerPath();
      
              document.getElementsByTagName("body")[0].style.overflow = "hidden";
      
            } else {

              // off
      
              document.getElementsByTagName("body")[0].style.overflow = "";
      
            }
      
            if ((!newVal && oldVal) && (scope.onClose !== null)) {

              // off 

              scope.pathToFile = '';
      
              return scope.onClose();
      
            }
      
          });
      
          setupCloseButton();
      
          return setupStyle();
      
        },
      
        templateUrl: 'tpl/fileviewer.tpl.html'
      
      };

    }

  ]);
submissionsListModule.directive('submissionList', ['downloadFactory', function (downloadFactory) {

    return {
        
        restrict: "A",

        scope: {
            list: '=',
            displayAs: '@',
            loaded: '='
        },

        replace: true,

        templateUrl: 'tpl/subList.tpl.html',

        controller: function($scope){

            var unopenableFiles = ['rar','zip','gzip','psd','ai','indd','mov','tar'];

            $scope.subToDownload = {};

            $scope.modalOpen = false;

            $scope.activeSubIndex = '0';

            $scope.showModal = function(index){

                $scope.modalOpen = true;

                $scope.activeSubIndex = index;

            };

            $scope.displayList = [].concat($scope.list);

            $scope.groupByStudent = function() {

                // nothing yet

            };

            $scope.groupByAssignment = function() {

                // nothing yet

            };

            $scope.groupByClass = function() {

                // nothing yet

            };

            $scope.testIfViewable = function(name){

                var fe = name.split('.').pop().toLowerCase();

                if(unopenableFiles.indexOf(fe) >= 0){

                    return true;

                } else {

                    return false;

                }

            };

            $scope.downloadGroup = function(){

                var arr = _.values($scope.subToDownload);

                _.pull(arr, "");

                downloadFactory.download(arr).then(function(data){

                    console.log(data);

                }, function(error){
                
                    console.log('bulk download error: ' + error);
                
                });

            };

        },

        link: function (scope, elem, attrs, ctrl) {

    
        }

    };

}]);
// submissions list module

submissionsListModule.factory("downloadFactory", ['$http', '$resource', '$q', function ($http, $resource, $q) {

  var downloadGroupResource = $resource('api/download/submissions.json', {},
    
    {

      'download': {method: 'POST'} 

    });


  var factory = {
    
    download : function (downloadList) {
      // this should be passed an array of submission IDs
 
      var deferred = $q.defer();
 
      downloadGroupResource.download({submissions_to_download: downloadList},
        function(resp) {
          deferred.resolve(resp);
        },
        function(resp){
          deferred.reject(resp);
        });
 
      return deferred.promise;
 
    }

  };
  
  return factory;

}]);
submissionsListModule.provider("ngModalDefaults", function() {

    return {

      options: {

        closeButtonHtml: "<span class='ng-modal-close-x'>X</span>"

      },

      $get: function() {

        return this.options;

      },

      set: function(keyOrHash, value) {

        var k, v, _results;

        if (typeof keyOrHash === 'object') {

          _results = [];

          for (k in keyOrHash) {

            v = keyOrHash[k];

            _results.push(this.options[k] = v);

          }

          return _results;

        } else {

          this.options[keyOrHash] = value;

          return this.options[keyOrHash];

        }

      }

    };

  });