<?php

namespace app\controllers;

use app\models\Students;
use lithium\g11n\Message;

use app\extensions\helper\Debug;

class StudentsController extends \app\extensions\action\Controller {

	/**
	 */
	protected function PREFIXIndex() {
	}

	/**
	 */
	protected function PREFIXCreate() {
		$student = Students::create();

		if ($this->request->is('post')) {
			$student->save($this->request->data);
		}

		return compact('student');
	}


	/**
	 */
	public function PREFIXEdit() {
		$student = Students::find($this->request->id);

		if ($this->request->is('put')) {
			$student->save($this->request->data);
		}
		return compact('student');
	}


	/**
	 * [ short description of PREFIXDestroy action ]
	 *
	 * [ long description of PREFIXDestroy action ]
	 *
	 * @todo   Incomplete
	 * @url    DELETE /students/:id
	 */
	public function PREFIXDestroy() {
		$student = Students::find($this->request->id);

		$student->delete();

		return compact('student');
	}


	/**
	 * [ short description of PREFIXView action ]
	 *
	 * [ long description of PREFIXView action ]
	 *
	 * @todo   Incomplete
	 * @url GET /students/1
	 */
	public function PREFIXView() {
		$student = Students::find($this->request->id);
		return compact('student');
	}
}

?>