<?php

namespace app\controllers;

use app\models\Permissions;
use lithium\g11n\Message;

use app\extensions\helper\Debug;

class PermissionsController extends \app\extensions\action\Controller {
	/**
	 * [ short description of PREFIXDestroy action ]
	 *
	 * [ long description of PREFIXDestroy action ]
	 *
	 * @todo   Incomplete
	 * @url    DELETE /permissions/:id
	 */
	public function PREFIXDestroy() {
		extract(Message::aliases());
		$permission = Permissions::find($this->request->id);

		if ($permission->delete()) {
			$this->flashSuccess($t('The permission was deleted.'));
			return $this->redirect(['Permissions::index']);
		}

		$this->flashError($t('The permission could not be deleted.'));
		return $this->redirect($this->request->referer());
	}

	/**
	 * [ short description of PREFIXEdit action ]
	 *
	 * [ long description of PREFIXEdit action ]
	 *
	 * @todo   Incomplete
	 * @url    GET /permissions/:id/edit
	 * @url    PUT /permissions/:id/edit
	 */
	public function PREFIXEdit() {
		$permission = Permissions::find($this->request->id);
		if ($this->request->is('put')) {
			extract(Message::aliases());

			if ($permission->save($this->request->data)) {
				$this->flashSuccess($t('The permission was updated.'));
				return $this->redirect([
					'Permissions::view',
					'id' => $permission->id
				]);
			}
			$this->flashError($t('The permission could not be updated.'));
		}
		return compact('permission');
	}

	/**
	 * [ short description of PREFIXIndex action ]
	 *
	 * [ long description of PREFIXIndex action ]
	 *
	 * @todo   Incomplete
	 * @url    GET /permissions
	 * @url    GET /permissions/index
	 */
	public function PREFIXIndex() {
		$limit = 10;
		$page  = $this->request->page ?: 1;

		$permissions = Permissions::all([
			'limit' => $limit,
			'page'  => $page,
			'order' => ['id' => 'ASC']
		]);

		$total = Permissions::find('count');
		return compact('permissions', 'total', 'limit', 'page');
	}

	/**
	 * [ short description of PREFIXCreate action ]
	 *
	 * [ long description of PREFIXCreate action ]
	 *
	 * @todo   Incomplete
	 * @url    GET  /permissions/create
	 * @url    POST /permissions/create
	 */
	public function PREFIXCreate() {
		$permission = Permissions::create();

		if ($this->request->is('post')) {
			extract(Message::aliases());

			if ($permission->save($this->request->data)) {
				$this->flashSuccess($t('The permission was added.'));
				return $this->redirect([
					'Permissions::view',
					'id' => $permission->id
				]);
			}
			$this->flashError($t('The permission could not be added.'));
		}

		return compact('permission');
	}

	/**
	 * Allows users with role `PREFIX` to search within this model.
	 *
	 * @todo   Incomplete
	 * @see    app\models\Permissions
	 * @url    GET /permissions/search/q=<search term>&<field_1>=&<field_2>...
	 */
	public function PREFIXSearch() {
		return $this->search();
	}

	/**
	 * [ short description of PREFIXView action ]
	 *
	 * [ long description of PREFIXView action ]
	 *
	 * @todo   Incomplete
	 * @url GET /permissions/1
	 */
	public function PREFIXView() {
		$permission = Permissions::find($this->request->id);
		return compact('permission');
	}
}

?>