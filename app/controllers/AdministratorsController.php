<?php

namespace app\controllers;

use app\models\Administrators;
use lithium\g11n\Message;

use app\extensions\helper\Debug;

class AdministratorsController extends \app\extensions\action\Controller {

	/**
	 * [ short description of PREFIXDestroy action ]
	 *
	 * [ long description of PREFIXDestroy action ]
	 *
	 * @todo   Incomplete
	 * @url    DELETE /administrators/:id
	 */
	public function PREFIXDestroy() {
		extract(Message::aliases());
		$administrator = Administrators::find($this->request->id);

		if ($administrator->delete()) {
			$this->flashSuccess($t('The administrator was deleted.'));
			return $this->redirect(['Administrators::index']);
		}

		$this->flashError($t('The administrator could not be deleted.'));
		return $this->redirect($this->request->referer());
	}

	/**
	 * [ short description of PREFIXEdit action ]
	 *
	 * [ long description of PREFIXEdit action ]
	 *
	 * @todo   Incomplete
	 * @url    GET /administrators/:id/edit
	 * @url    PUT /administrators/:id/edit
	 */
	public function PREFIXEdit() {
		$administrator = Administrators::find($this->request->id);
		if ($this->request->is('put')) {
			extract(Message::aliases());

			if ($administrator->save($this->request->data)) {
				$this->flashSuccess($t('The administrator was updated.'));
				return $this->redirect([
					'Administrators::view',
					'id' => $administrator->id
				]);
			}
			$this->flashError($t('The administrator could not be updated.'));
		}
		return compact('administrator');
	}

	/**
	 * [ short description of PREFIXIndex action ]
	 *
	 * [ long description of PREFIXIndex action ]
	 *
	 * @todo   Incomplete
	 * @url    GET /administrators
	 * @url    GET /administrators/index
	 */
	public function PREFIXIndex() {
		$limit = 10;
		$page  = $this->request->page ?: 1;

		$administrators = Administrators::all([
			'limit' => $limit,
			'page'  => $page,
			'order' => ['id' => 'ASC']
		]);

		$total = Administrators::find('count');
		return compact('administrators', 'total', 'limit', 'page');
	}

	/**
	 * [ short description of PREFIXCreate action ]
	 *
	 * [ long description of PREFIXCreate action ]
	 *
	 * @todo   Incomplete
	 * @url    GET  /administrators/create
	 * @url    POST /administrators/create
	 */
	public function PREFIXCreate() {
		$administrator = Administrators::create();

		if ($this->request->is('post')) {
			extract(Message::aliases());

			if ($administrator->save($this->request->data)) {
				$this->flashSuccess($t('The administrator was added.'));
				return $this->redirect([
					'Administrators::view',
					'id' => $administrator->id
				]);
			}
			$this->flashError($t('The administrator could not be added.'));
		}

		return compact('administrator');
	}

	/**
	 * Allows users with role `PREFIX` to search within this model.
	 *
	 * @todo   Incomplete
	 * @see    app\models\Administrators
	 * @url    GET /administrators/search/q=<search term>&<field_1>=&<field_2>...
	 */
	public function PREFIXSearch() {
		return $this->search();
	}

	/**
	 * [ short description of PREFIXView action ]
	 *
	 * [ long description of PREFIXView action ]
	 *
	 * @todo   Incomplete
	 * @url GET /administrators/1
	 */
	public function PREFIXView() {
		$administrator = Administrators::find($this->request->id);
		return compact('administrator');
	}
}

?>