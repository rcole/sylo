<?php

namespace app\controllers;

use app\models\Semesters;
use lithium\g11n\Message;

use app\extensions\helper\Debug;

class SemestersController extends \app\extensions\action\Controller {

	/**
	 * A list of Semesters [1.2]
	 *
	 * @url    GET /semesters
	 */
	protected function _index() {
		$semesters = Semesters::all(['order' => ['id' => 'ASC']]);
		return compact('semesters');
	}

	public function adminIndex() {
		return $this->_index();
	}

	public function teacherIndex() {
		return $this->_index();
	}

	public function studentIndex() {
		return $this->_index();
	}

	/**
	 * [1.2]
	 *
	 * @url    POST /semesters
	 */
	public function adminCreate() {
		$semester = Semesters::create();

		if ($this->request->is('post')) {
			$semester->save($this->request->data);
		}

		return compact('semester');
	}

	/**
	 * View a single Semester [1.2a]
	 *
	 * @url    GET /semester/:semester
	 */
	public function adminView() {
		$semester = Semesters::find('first', [
			'conditions' => ['slug' => $this->request->semester]
		]);
		return compact('semester');
	}

	public function teacherView() {
		$semester = Semesters::find('first', [
			'conditions' => ['slug' => $this->request->semester]
		]);
		return compact('semester');
	}

	public function studentView() {
		$semester = Semesters::find('first', [
			'conditions' => ['slug' => $this->request->semester]
		]);
		return compact('semester');
	}

	public function prgLeadView() {
		$semester = Semesters::find('first', [
			'conditions' => ['slug' => $this->request->semester]
		]);
		return compact('semester');
	}

	public function acdServView() {
		$semester = Semesters::find('first', [
			'conditions' => ['slug' => $this->request->semester]
		]);
		return compact('semester');
	}

	/**
	 * Edit a Semester [1.2a]
	 *
	 * @url    PUT /semester/:semester
	 */
	public function adminEdit() {
		$semester = Semesters::find('first', [
			'conditions' => ['slug' => $this->request->semester]
		]);

		if ($this->request->is('put')) {
			$semester->save($this->request->data);
		}

		return compact('semester');
	}

	/**
	 * Delete a Semester [1.2a]
	 *
	 * @url    DELETE /semester/:semester
	 */
	public function adminDestroy() {
		$semester = Semesters::find('first', [
			'conditions' => ['slug' => $this->request->semester]
		]);

		$semester->delete();

		return $this->redirect($this->request->referer());
	}

	/**
	 * Returns the current semester.
	 */
	protected function _current() {
		$semester = Semesters::current();
		return compact('semester');
	}

	public function adminCurrent() {
		return $this->_current();
	}

	public function teacherCurrent() {
		return $this->_current();
	}

	public function studentCurrent() {
		return $this->_current();
	}
}

?>