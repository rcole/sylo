<?php

namespace app\controllers;

use app\models\Teachers;
use lithium\g11n\Message;

use app\extensions\helper\Debug;

class TeachersController extends \app\extensions\action\Controller {

	/**
	 * [ short description of PREFIXDestroy action ]
	 *
	 * [ long description of PREFIXDestroy action ]
	 *
	 * @todo   Incomplete
	 * @url    DELETE /teachers/:id
	 */
	public function PREFIXDestroy() {
		extract(Message::aliases());
		$teacher = Teachers::find($this->request->id);

		if ($teacher->delete()) {
			$this->flashSuccess($t('The teacher was deleted.'));
			return $this->redirect(['Teachers::index']);
		}

		$this->flashError($t('The teacher could not be deleted.'));
		return $this->redirect($this->request->referer());
	}

	/**
	 * [ short description of PREFIXEdit action ]
	 *
	 * [ long description of PREFIXEdit action ]
	 *
	 * @todo   Incomplete
	 * @url    GET /teachers/:id/edit
	 * @url    PUT /teachers/:id/edit
	 */
	public function PREFIXEdit() {
		$teacher = Teachers::find($this->request->id);
		if ($this->request->is('put')) {
			extract(Message::aliases());

			if ($teacher->save($this->request->data)) {
				$this->flashSuccess($t('The teacher was updated.'));
				return $this->redirect([
					'Teachers::view',
					'id' => $teacher->id
				]);
			}
			$this->flashError($t('The teacher could not be updated.'));
		}
		return compact('teacher');
	}

	/**
	 * [ short description of PREFIXIndex action ]
	 *
	 * [ long description of PREFIXIndex action ]
	 *
	 * @todo   Incomplete
	 * @url    GET /teachers
	 * @url    GET /teachers/index
	 */
	public function PREFIXIndex() {
		$limit = 10;
		$page  = $this->request->page ?: 1;

		$teachers = Teachers::all([
			'limit' => $limit,
			'page'  => $page,
			'order' => ['id' => 'ASC']
		]);

		$total = Teachers::find('count');
		return compact('teachers', 'total', 'limit', 'page');
	}

	/**
	 * [ short description of PREFIXCreate action ]
	 *
	 * [ long description of PREFIXCreate action ]
	 *
	 * @todo   Incomplete
	 * @url    GET  /teachers/create
	 * @url    POST /teachers/create
	 */
	public function PREFIXCreate() {
		$teacher = Teachers::create();

		if ($this->request->is('post')) {
			extract(Message::aliases());

			if ($teacher->save($this->request->data)) {
				$this->flashSuccess($t('The teacher was added.'));
				return $this->redirect([
					'Teachers::view',
					'id' => $teacher->id
				]);
			}
			$this->flashError($t('The teacher could not be added.'));
		}

		return compact('teacher');
	}

	/**
	 * Allows users with role `PREFIX` to search within this model.
	 *
	 * @todo   Incomplete
	 * @see    app\models\Teachers
	 * @url    GET /teachers/search/q=<search term>&<field_1>=&<field_2>...
	 */
	public function PREFIXSearch() {
		return $this->search();
	}

	/**
	 * [ short description of PREFIXView action ]
	 *
	 * [ long description of PREFIXView action ]
	 *
	 * @todo   Incomplete
	 * @url GET /teachers/1
	 */
	public function PREFIXView() {
		$teacher = Teachers::find($this->request->id);
		return compact('teacher');
	}
}

?>