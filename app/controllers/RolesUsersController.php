<?php

namespace app\controllers;

use app\models\RolesUsers;
use lithium\g11n\Message;

use app\extensions\helper\Debug;

class RolesUsersController extends \app\extensions\action\Controller {
	/**
	 * [ short description of PREFIXDestroy action ]
	 *
	 * [ long description of PREFIXDestroy action ]
	 *
	 * @todo   Incomplete
	 * @url    DELETE /rolesUsers/:id
	 */
	public function PREFIXDestroy() {
		extract(Message::aliases());
		$rolesUser = RolesUsers::find($this->request->id);

		if ($rolesUser->delete()) {
			$this->flashSuccess($t('The rolesUser was deleted.'));
			return $this->redirect(['RolesUsers::index']);
		}

		$this->flashError($t('The rolesUser could not be deleted.'));
		return $this->redirect($this->request->referer());
	}

	/**
	 * [ short description of PREFIXEdit action ]
	 *
	 * [ long description of PREFIXEdit action ]
	 *
	 * @todo   Incomplete
	 * @url    GET /rolesUsers/:id/edit
	 * @url    PUT /rolesUsers/:id/edit
	 */
	public function PREFIXEdit() {
		$rolesUser = RolesUsers::find($this->request->id);
		if ($this->request->is('put')) {
			extract(Message::aliases());

			if ($rolesUser->save($this->request->data)) {
				$this->flashSuccess($t('The rolesUser was updated.'));
				return $this->redirect([
					'RolesUsers::view',
					'id' => $rolesUser->id
				]);
			}
			$this->flashError($t('The rolesUser could not be updated.'));
		}
		return compact('rolesUser');
	}

	/**
	 * [ short description of PREFIXIndex action ]
	 *
	 * [ long description of PREFIXIndex action ]
	 *
	 * @todo   Incomplete
	 * @url    GET /rolesUsers
	 * @url    GET /rolesUsers/index
	 */
	public function PREFIXIndex() {
		$limit = 10;
		$page  = $this->request->page ?: 1;

		$rolesUsers = RolesUsers::all([
			'limit' => $limit,
			'page'  => $page,
			'order' => ['id' => 'ASC']
		]);

		$total = RolesUsers::find('count');
		return compact('rolesUsers', 'total', 'limit', 'page');
	}

	/**
	 * [ short description of PREFIXCreate action ]
	 *
	 * [ long description of PREFIXCreate action ]
	 *
	 * @todo   Incomplete
	 * @url    GET  /rolesUsers/create
	 * @url    POST /rolesUsers/create
	 */
	public function PREFIXCreate() {
		$rolesUser = RolesUsers::create();

		if ($this->request->is('post')) {
			extract(Message::aliases());

			if ($rolesUser->save($this->request->data)) {
				$this->flashSuccess($t('The rolesUser was added.'));
				return $this->redirect([
					'RolesUsers::view',
					'id' => $rolesUser->id
				]);
			}
			$this->flashError($t('The rolesUser could not be added.'));
		}

		return compact('rolesUser');
	}

	/**
	 * Allows users with role `PREFIX` to search within this model.
	 *
	 * @todo   Incomplete
	 * @see    app\models\RolesUsers
	 * @url    GET /rolesUsers/search/q=<search term>&<field_1>=&<field_2>...
	 */
	public function PREFIXSearch() {
		return $this->search();
	}

	/**
	 * [ short description of PREFIXView action ]
	 *
	 * [ long description of PREFIXView action ]
	 *
	 * @todo   Incomplete
	 * @url GET /rolesUsers/1
	 */
	public function PREFIXView() {
		$rolesUser = RolesUsers::find($this->request->id);
		return compact('rolesUser');
	}
}

?>