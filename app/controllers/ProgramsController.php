<?php

namespace app\controllers;

use app\models\Programs;
use app\models\ProgramAuthorizations;
use app\models\Semesters;
use app\models\Enrollments;
use app\models\Authorizations;
use app\models\Modules;
use app\models\Classes;
use app\models\Submissions;
use app\models\Students;
use app\models\Teachers;
use app\models\Users;
use lithium\g11n\Message;
use lithium\analysis\Debugger;

class ProgramsController extends \app\extensions\action\Controller {

	/**
	 * A list of all Programs [1.1]
	 *
	 * @url    GET /programs
	 */
	protected function _index() {
		$programsFields = Users::safeFields([], 'ProgramLeaders');
		$programsFields[] = 'Programs.*';
		$programs = Programs::all([
			'fields' => $programsFields,
			'with' => ['ProgramAuthorizations.ProgramLeaders', 'Modules.Classes.Concurrences.Semesters'],
			'order' => ['id' => 'ASC']
		]);

		$studentCount = Students::all([
			'fields' => ['DISTINCT(Students.id) AS _ID_'],
			'with' => ['Enrollments.Semesters'],
			'conditions' => [
				'Semesters.begins' => ['<=' => time()],
				'Semesters.ends' => ['>=' => time()]
			],
			'error' => false
		])->count();

		$semesterClassesCount = Semesters::count([
			'with' => ['Concurrences.Classes'],
			'conditions' => [
				'Semesters.begins' => ['<=' => time()],
				'Semesters.ends' => ['>=' => time()]
			],
			'error' => false
		]);

		$modulesCount = Modules::count([
			'with' => ['Classes.Concurrences.Semesters'],
			'conditions' => [
				'Semesters.begins' => ['<=' => time()],
				'Semesters.ends' => ['>=' => time()]
			],
			'error' => false
		]);

		$teacherCount = Teachers::count([
			'with' => ['TeacherAuthorizations.Semesters'],
			'conditions' => [
				'Semesters.begins' => ['<=' => time()],
				'Semesters.ends' => ['>=' => time()]
			],
			'error' => false
		]);
/*
$expectedSubmissions = ?
$submissionCount = Programs > Modules > Classes > Assignments > Submissions
*/
// @TODO
$expectedSubmissions = $submissionCount = 'TODO';
		return compact('programs', 'studentCount', 'teacherCount', 'semesterClassesCount', 'modulesCount', 'expectedSubmissions', 'submissionCount');
	}

	public function adminIndex() {
		return $this->_index();
	}

	public function teacherIndex() {
		return $this->_index();
	}

	/**
	 * [1.1]
	 *
	 * @url    POST /programs
	 */
	public function adminCreate() {
		$program = Programs::create();

		if ($this->request->is('post')) {
			$program->save($this->request->data);

			foreach ((array) $this->request->data['program_authorizations'] as $prgLead) {
				$prgLead['program_id'] = $program->id;
				$currentProgram = ProgramAuthorizations::find('first', [
					'conditions' => [
						'program_leader_id' => $prgLead['program_leader_id'],
						'program_id' => $prgLead['program_id']
					]
				]);
				if (!$currentProgram->exists()) {
					$programAuthorizations = ProgramAuthorizations::create();
					$programAuthorizations->save($prgLead);
				}
			}
		}

		return compact('program');
	}

	/**
	 * View a single program [1.1a]
	 *
	 * @url    GET /program/:program
	 */
	public function adminView() {
		$program = Programs::find('first', [
			'conditions' => ['slug' => $this->request->program],
			'with' => ['ProgramAuthorizations.ProgramLeaders']
		]);
		return compact('program');
	}

	/**
	 * Edit a program [1.1a]
	 *
	 * @url    PUT /program/:program
	 */
	public function adminEdit() {
		$program = Programs::find('first', [
			'conditions' => ['slug' => $this->request->program]
		]);
		if ($this->request->is('put')) {
			$program->save($this->request->data);

			foreach ((array) $this->request->data['program_authorizations'] as $prgLead) {
				$prgLead['program_id'] = $program->id;
				$currentProgram = ProgramAuthorizations::find('first', [
					'conditions' => [
						'program_leader_id' => $prgLead['program_leader_id'],
						'program_id' => $prgLead['program_id']
					]
				]);
				if (!$currentProgram->exists()) {
					$programAuthorizations = ProgramAuthorizations::create();
					$programAuthorizations->save($prgLead);
				}
			}
		}
		return compact('program');
	}

	/**
	 * Delete a program [1.1a]
	 *
	 * @url    DELETE /program/:program
	 */
	public function adminDestroy() {
		$program = Programs::find('first', [
			'conditions' => ['slug' => $this->request->program]
		]);

		$program->delete();

		return compact('program');
	}


	/**
	 * A list of Students enrolled in a Program [1.1.3]
	 *
	 * @url    GET /semester/:semester/program/:program/enrollment
	 */
	protected function _enrollments() {
		$students = Students::all([
			'with' => ['Enrollments.Semesters', 'Enrollments.Classes.Modules.Programs'],
			'conditions' => [
				'Semesters.slug' => $this->request->semester,
				'Programs.slug' => $this->request->program
			],
		]);

		return compact('students');
	}

	public function adminEnrollments() {
		return $this->_enrollments();
	}

	public function prgLeadEnrollments() {
		return $this->_enrollments();
	}

	public function acdServEnrollments() {
		return $this->_enrollments();
	}













































/*
	public function adminIndex() {
		$programs = Programs::all([
			'with' => ['ProgramAuthorizations.ProgramLeaders', 'Modules.Concurrences.Semesters']
		]);

		$studentCount = Enrollments::find('count', [
			'fields' => ['DISTINCT Students.id'],
			'with' => ['Students']
		]);

		$teacherCount = Authorizations::find('count', [
			'fields' => ['DISTINCT Teachers.id'],
			'with' => ['Teachers']
		]);

		$semesterClassesCount = Semesters::find('count', [
			'fields' => ['distinct Classes.id'],
			'conditions' => [
				'Semesters.begins' => ['<=' => time()],
				'Semesters.ends' => ['>=' => time()]
			],
			'with' => ['Classes']
		]);

		$modulesCount = Modules::find('count');

		$classes = Classes::find('all', [
			'fields' => [
				'COUNT(DISTINCT Assignments.id) as assignmentsCount',
				'COUNT(DISTINCT Enrollments.id) as enrollmentsCount'
			],
			'with' => ['Assignments', 'Enrollments'],
			'conditions' => [
				'Assignments.id' => ['is not' => null],
				'Enrollments.id' => ['is not' => null]
			]
		]);

		$expectedSubmissions = 0;
		foreach ($classes as $class) {
			$expectedSubmissions += $class->assignmentsCount * $class->enrollmentsCount;
		}

		$submissionCount = Submissions::find('count');

		return compact('programs', 'studentCount', 'teacherCount', 'semesterClassesCount', 'modulesCount', 'expectedSubmissions', 'submissionCount');
	}
*/

/*
	public function adminModules() {
		$program = Programs::find('first', [
			'with' => ['Modules'],
			'conditions' => ['Programs.slug' => $this->request->params['program']]
		]);
		return compact('program');
	}
*/


	/**
	 * Allows users with role `PREFIX` to search within this model.
	 *
	 * @todo   Incomplete
	 * @see    app\models\Programs
	 * @url    GET /programs/search/q=<search term>&<field_1>=&<field_2>...
	 */
	public function PREFIXSearch() {
		return $this->search();
	}
}

?>