<?php

namespace app\controllers;

use app\models\Submissions;
use app\models\Sessions;
use app\models\Teachers;
use app\models\Students;
use app\models\Users;
use app\models\Assignments;
use Flow;

use app\extensions\helper\Debug;

class SubmissionsController extends \app\extensions\action\Controller {

	/**
	 * Displays a list of Submissions by a single student [1.1.2.1]
	 *
	 * Displays a list of submissions in a program [1.1.2]
	 *
	 * Displays a list of submissions for Assignments.slug = :assignment [1.1.1.5]
	 *
	 * @url    GET /semester/:semester/program/:program/submissions/:student_id
	 * @url    GET /semester/:semester/program/:program/submissions
	 * @url    GET /semester/:semester/program/:program/class/:class/assignments/:assignment/submissions
	 */
	protected function _index() {
		$conditions = [
			'Semesters.slug' => $this->request->semester,
			'Programs.slug' => $this->request->program
		];

		$with[] = 'Assignments.Classes.Modules.Programs';
		$with[] = 'Assignments.Classes.Concurrences.Semesters';

		if (isset($this->request->params['student_id'])) {
			$conditions['Submissions.student_id'] = $this->request->student_id;
		}

		if (isset($this->request->params['class'])) {
			$conditions['Classes.slug'] = $this->request->class;
			$conditions['Assignments.slug'] = $this->request->assignment;

			$with[] = 'Assignments';
			$with[] = 'Assignments.Classes';
		}

		$submissions = Submissions::all([
			'order' => ['id' => 'ASC'],
			'with' => $with,
			'conditions' => $conditions
		]);

		return compact('submissions');
	}

	public function adminIndex() {
		$fields = Users::safeFields([], 'Teachers');
		$fields[] = 'Submissions.*';
		$fields[] = 'Assignments.*';
		$fields[] = 'Classes.*';
		$fields[] = 'Concurrences.*';
		$fields[] = 'Semesters.*';
		$fields[] = 'TeacherAuthorizations.*';

		$conditions = [
			'Semesters.slug' => $this->request->semester,
			'Teachers.id' => Sessions::id()
		];

		$with = [
			'Assignments.Classes.TeacherAuthorizations.Teachers',
			'Assignments.Classes.Concurrences.Semesters'
		];

		if (isset($this->request->params['student_id'])) {
			$conditions['Students.id'] = $this->request->student_id;
			$with[] = 'Students';
		}

		if (isset($this->request->params['class'])) {
			$conditions['Classes.slug'] = $this->request->class;
		}

		$submissions = Submissions::all([
			'fields' => $fields,
			'with' => $with,
			'conditions' => $conditions
		]);

		return compact('submissions');
	}

	public function prgLeadIndex() {
		return $this->_index();
	}

	public function acdServIndex() {
		return $this->_index();
	}

	/**
	 * A list of files submitted as Class submissions in an authorized Class  [1.6.5]
	 *
	 * A list of Submissions in an authorized Class [1.8]
	 *
	 * A list of Submissions from a single student in an authorized Class [1.6.6.1]
	 *
	 * @url    GET /semester/:semester/myclasses/:class/submissions
	 * @url    GET /semester/:semester/myclasses/submissions
	 * @url    GET /semester/:semester/myclasses/submissions/:student_id
	 */
	public function teacherIndex() {
		$fields = Users::safeFields([], 'Teachers');
		$fields[] = 'Submissions.*';
		$fields[] = 'Assignments.*';
		$fields[] = 'Classes.*';
		$fields[] = 'Concurrences.*';
		$fields[] = 'Semesters.*';
		$fields[] = 'TeacherAuthorizations.*';

		$conditions = [
			'Semesters.slug' => $this->request->semester,
			'Teachers.id' => Sessions::id()
		];

		$with = [
			'Assignments.Classes.TeacherAuthorizations.Teachers',
			'Assignments.Classes.Concurrences.Semesters'
		];

		if (isset($this->request->params['student_id'])) {
			$conditions['Students.id'] = $this->request->student_id;
			$with[] = 'Students';
		}

		if (isset($this->request->params['class'])) {
			$conditions['Classes.slug'] = $this->request->class;
		}

		$submissions = Submissions::all([
			'fields' => $fields,
			'with' => $with,
			'conditions' => $conditions
		]);

		return compact('submissions');
	}

	/**
	 * A list of Submissions by the current Student [2.2]
	 *
	 * @url    GET /semester/:semester/mysubmissions
	 */
	public function studentIndex() {
		$fields = Users::safeFields([], 'Students');
		$fields[] = 'Submissions.*';
		$fields[] = 'Assignments.*';
		$fields[] = 'Classes.*';
		$fields[] = 'Concurrences.*';
		$fields[] = 'Semesters.*';

		$submissions = Submissions::all([
			'fields' => $fields,
			'with' => [
				'Students',
				'Assignments.Classes.Concurrences.Semesters'
			],
			'conditions' => [
				'Students.id' => Sessions::id(),
				'Semesters.slug' => $this->request->semester,
			]
		]);

		return compact('submissions');
	}


	/**
	 * Create a submission for Assignments.slug = :assignment [1.1.1.5]
	 *
	 * @url    POST /semester/:semester/program/:program/class/:class/assignments/:assignment/submissions
	 */
	protected function _create() {
		$submission = Submissions::create();

		if ($this->request->is('post')) {
			$submission->save($this->request->data);
		}

		return compact('submission');
	}

	public function adminCreate() {
		return $this->_create();
	}

	public function prgLeadCreate() {
		return $this->_create();
	}

	public function acdServCreate() {
		return $this->_create();
	}

	/**
	 * Create a submission [2.1]
	 *
	 * @url    GET /submit
	 * @url    POST /submit
	 */
	public function studentCreate() {
		if ($this->request->is('get')) {
			$assignment_id = $_GET['assignment_id'];
			$student_id = $_GET['student_id'];
		} else {
			$assignment_id = $_POST['assignment_id'];
			$student_id = $_POST['student_id'];
		}
		$assignment = Assignments::find('first', [
			'conditions' => ['Assignments.id' => $assignment_id],
			'with' => ['Classes', 'Classes.Modules', 'Classes.Modules.Programs']
		]);

		$semester = \app\models\Semesters::current();

		$chunksDir = Submissions::path('chunks');
		$storageDir = Submissions::path('save', [
			'semester' => $semester,
			'assignment' => $assignment,
			'assignment_id' => $assignment_id,
			'student_id' => $student_id
		]);
		$this->makeDirs($storageDir);

		$path = Submissions::path('path', [
			'semester' => $semester,
			'assignment' => $assignment,
			'assignment_id' => $assignment_id,
			'student_id' => $student_id
		]);

		$config = new Flow\Config();
		$config->setTempDir($chunksDir);
		$file = new Flow\File($config);

		if ($this->request->is('get')) {
			if ($file->checkChunk()) {
				$this->response->status(200);
			} else {
				$this->response->status(404);
				return $this;
			}
			$assignment_id = $_GET['assignment_id'];
			$student_id = $_GET['student_id'];
			$name = $_GET['flowFilename'];
		} else {
			if ($file->validateChunk()) {
				$file->saveChunk();
			} else {
				// error, invalid chunk upload request, retry
				$this->response->status(400);
				return $this;
			}
			$assignment_id = $_POST['assignment_id'];
			$student_id = $_POST['student_id'];
			$name = $_POST['flowFilename'];
		}
		$data = [
			'assignment_id' => $assignment_id,
			'student_id' => $student_id,
			'name' => $name,
			'path' => $path,
		];

		//@todo Create a `receipt`
		$submission = Submissions::find('first', ['conditions' => [
			'assignment_id' => $assignment_id,
			'student_id' => $student_id,
			'name' => $name
		]]);
		if (!$submission->exists()) {
			$submission = Submissions::create();
		}

		if ($file->validateFile() && $file->save($storageDir . $name)) {
			// File upload was completed
			$data += ['receipt' => 'temp'];
		} else {
			// This is not a final chunk, continue to upload
		}

		$submission->save($data);

		if (1 == mt_rand(1, 20)) {
			Flow\Uploader::pruneChunks($chunksDir);
		}
	}


	/**
	 * Edit a submission for Assignments.slug = :assignment [1.1.1.5]
	 *
	 * @url    PUT /semester/:semester/program/:program/class/:class/assignments/:assignment/submissions/:submission_id
	 */
	protected function _edit() {
		$submission = Submissions::find('first', [
			'order' => ['id' => 'ASC'],
			'with' => [
				'Assignments.Classes.Modules.Programs',
				'Assignments.Classes.Concurrences.Semesters',
				'Assignments',
				'Assignments.Classes'
			],
			'conditions' => [
				'Semesters.slug' => $this->request->semester,
				'Programs.slug' => $this->request->program,
				'Classes.slug' => $this->request->class,
				'Assignments.slug' => $this->request->assignment
			]
		]);

		if ($this->request->is('put')) {
			$submission->save($this->request->data);
		}
		return compact('submission');
	}

	public function adminEdit() {
		return $this->_edit();
	}

	public function prgLeadEdit() {
		return $this->_edit();
	}

	public function acdServEdit() {
		return $this->_edit();
	}


	/**
	 * Deletes a submission for Assignments.slug = :assignment [1.1.1.5]
	 *
	 * @url    DELETE /semester/:semester/program/:program/class/:class/assignments/:assignment/submissions/:submission_id
	 */
	public function PREFIXDestroy() {
		$submission = Submissions::find('first', [
			'order' => ['id' => 'ASC'],
			'with' => [
				'Assignments.Classes.Modules.Programs',
				'Assignments.Classes.Concurrences.Semesters',
				'Assignments',
				'Assignments.Classes'
			],
			'conditions' => [
				'Semesters.slug' => $this->request->semester,
				'Programs.slug' => $this->request->program,
				'Classes.slug' => $this->request->class,
				'Assignments.slug' => $this->request->assignment
			]
		]);

		$submission->delete();

		return compact('submission');
	}













	/**
	 * [ short description of PREFIXView action ]
	 *
	 * [ long description of PREFIXView action ]
	 *
	 * @todo   Incomplete
	 * @url GET /submissions/1
	 */
	public function PREFIXView() {
		$submission = Submissions::find($this->request->id);
		return compact('submission');
	}
}

?>