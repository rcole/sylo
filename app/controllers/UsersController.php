<?php

namespace app\controllers;

use app\models\Users;
use app\models\RolesUsers;
use app\models\Sessions;
use lithium\g11n\Message;
use lithium\analysis\Debugger;

class UsersController extends \app\extensions\action\Controller {

	/**
	 * A list of Users [1.4]
	 *
	 * @url    GET /users
	 */
	protected function _index() {
		$fields = Users::safeFields();
		$fields[] = 'RolesUsers.*';
		$fields[] = 'Roles.*';

		$users = Users::all([
			'order' => ['id' => 'ASC'],
			'with'  => ['RolesUsers', 'RolesUsers.Roles'],
			'fields' => $fields
		]);

		return compact('users');
	}

	public function adminIndex() {
		return $this->_index();
	}

	public function prgLeadIndex() {
		return $this->_index();
	}

	public function acdServIndex() {
		return $this->_index();
	}

	/**
	 * [1.4]
	 *
	 * @url    POST /users
	 */
	protected function _create() {
		$user = Users::create();

		if ($this->request->is('post')) {
			$user->save($this->request->data);
			if (isset($this->request->data['roles_users'])) {
				foreach ($this->request->data['roles_users'] as $role) {
					$role['user_id'] = $user->id;
					$currentRole = RolesUsers::find('first', ['conditions' => ['role_id' => $role['role_id'], 'user_id' => $role['user_id']]]);
					if (!$currentRole->exists()) {
						$rolesUsers = RolesUsers::create();
						$rolesUsers->save($role);
					}
				}
			}
		}
		return compact('user');
	}

	public function adminCreate() {
		return $this->_create();
	}

	public function prgLeadCreate() {
		return $this->_create();
	}

	public function acdServCreate() {
		return $this->_create();
	}

	/**
	 * Displays current users profile [1.5]
	 *
	 * @url    GET /profile
	 */
	protected function _profile() {
		$fields = Users::safeFields();
		$fields[] = 'RolesUsers.*';
		$fields[] = 'Roles.*';

		$user = Users::find('first', [
			'with' => ['RolesUsers', 'RolesUsers.Roles'],
			'fields' => $fields,
			'conditions' => ['id' => Sessions::id()]
		]);
		return compact('user');
	}

	public function adminProfile() {
		return $this->_profile();
	}

	public function acdServProfile() {
		return $this->_profile();
	}

	public function extVerProfile() {
		return $this->_profile();
	}

	public function studentProfile() {
		return $this->_profile();
	}

	public function studRecProfile() {
		return $this->_profile();
	}

	public function teacherProfile() {
		return $this->_profile();
	}

	public function prgLeadProfile() {
		return $this->_profile();
	}

	/**
	 * Allows current user to edit profile [1.5]
	 *
	 * @url    PUT /profile
	 */
	protected function _edit() {
		$fields = Users::safeFields();
		$fields[] = 'RolesUsers.*';

		$user = Users::find('first', [
			'with' => 'RolesUsers',
			'fields' => $fields,
			'conditions' => ['id' => Sessions::id()]
		]);

		if ($this->request->is('put')) {
			$user->save($this->request->data);
			if (isset($this->request->data['roles_users'])) {
				foreach ($this->request->data['roles_users'] as $role) {
					$currentRole = RolesUsers::find('first', ['conditions' => ['role_id' => $role['role_id'], 'user_id' => $role['user_id']]]);
					if (!$currentRole->exists()) {
						$rolesUsers = RolesUsers::create();
						$rolesUsers->save($role);
					}
				}
			}
		}
		return compact('user');
	}

	public function acdServEdit() {
		return $this->_edit();
	}

	public function adminEdit() {
		return $this->_edit();
	}

	public function extVerEdit() {
		return $this->_edit();
	}

	public function studentEdit() {
		return $this->_edit();
	}

	public function studRecEdit() {
		return $this->_edit();
	}

	public function teacherEdit() {
		return $this->_edit();
	}

	public function prgLeadEdit() {
		return $this->_edit();
	}

	/**
	 * Displays a users profile [1.4a]
	 *
	 * @url    GET /user/:id
	 */
	protected function _view() {
		$fields = Users::safeFields();
		$fields[] = 'RolesUsers.*';
		$fields[] = 'Roles.*';

		$user = Users::find('first', [
			'with' => ['RolesUsers', 'RolesUsers.Roles'],
			'fields' => $fields,
			'conditions' => ['id' => $this->request->id]
		]);
		return compact('user');
	}

	public function adminView() {
		return $this->_view();
	}

	public function teacherView() {
		return $this->_view();
	}

	public function acdServView() {
		return $this->_view();
	}

	public function prgLeadView() {
		return $this->_view();
	}

	public function extVerView() {
		return $this->_view();
	}

	public function studRecView() {
		return $this->_view();
	}




















	/**
	 * Should activate account with a token
	 *
	 * @todo
	 */
	public function activate() {}

	/**
	 * Should allow user to change password
	 *
	 * @todo
	 */
	public function changePassword() {}

	/**
	 * Should allow user to request a password reset
	 *
	 * @todo
	 */
	public function requestResetPassword() {}

	/**
	 * Should allow user to reset password
	 *
	 * @todo
	 */
	public function resetPassword() {}

	/**
	 * @todo Delete RolesUsers association for this user.
	 *
	 * Deletes a single user.
	 *
	 * @url    DELETE /users/:id
	 */
	public function adminDestroy() {
		$user = Users::find($this->request->id);

		$user->delete();
		return compact('user');
	}
}

?>