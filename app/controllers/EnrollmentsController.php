<?php

namespace app\controllers;

use app\models\Enrollments;
use app\models\Students;
use app\models\Users;

use app\extensions\helper\Debug;

class EnrollmentsController extends \app\extensions\action\Controller {

	/**
	 * Delete students association with a Class [1.1.1.2]
	 *
	 * @url    DELETE  /semester/:semester/program/:program/class/:class/students
	 */
	protected function _destroy() {
		foreach ($this->request->data['enrollments'] as $enrollments) {
			$enrollment = Enrollments::find('first', [
				'with' => ['Semesters', 'Classes', 'Students'],
				'conditions' => [
					'Semesters.id' => $enrollments['semester_id'],
					'Classes.id' => $enrollments['class_id'],
					'Students.id' => $enrollments['student_id'],
				]
			]);

			$enrollment->delete();
		}
		return compact('enrollment');
	}

	public function adminDestroy() {
		return $this->_destroy();
	}

	public function prgLeadDestroy() {
		return $this->_destroy();
	}

	public function acdServDestroy() {
		return $this->_destroy();
	}

	/**
	 */
	public function PREFIXEdit() {
		$enrollment = Enrollments::find($this->request->id);
		if ($this->request->is('put')) {
			extract(Message::aliases());

			if ($enrollment->save($this->request->data)) {
				$this->flashSuccess($t('The enrollment was updated.'));
				return $this->redirect([
					'Enrollments::view',
					'id' => $enrollment->id
				]);
			}
			$this->flashError($t('The enrollment could not be updated.'));
		}
		return compact('enrollment');
	}



	/**
	 * A list of Students enrolled in a Class [1.1.1.2]
	 *
	 * @url    GET /semester/:semester/program/:program/class/:class/students
	 */
	protected function _index() {
		$fields = Users::safeFields([], 'Students');
		$fields[] = 'Enrollments.*';
		$fields[] = 'Semesters.*';
		$fields[] = 'Modules.*';
		$fields[] = 'Classes.*';
		$fields[] = 'Programs.*';

		$students = Students::all([
			'with' => ['Enrollments.Semesters', 'Enrollments.Classes', 'Enrollments.Classes.Modules.Programs'],
			'conditions' => [
				'Semesters.slug' => $this->request->semester,
				'Programs.slug' => $this->request->program,
				'Classes.slug' => $this->request->class
			],
			'fields' => $fields
		]);

		return compact('students');
	}

	public function adminIndex() {
		return $this->_index();
	}

	public function prgLeadIndex() {
		return $this->_index();
	}

	public function acdServIndex() {
		return $this->_index();
	}




	/**
	 * Add students to a Class [1.1.1.2]
	 *
	 * @url    GET  /semester/:semester/program/:program/class/:class/students
	 */
	public function _create() {
		$enrollment = Enrollments::create();

		if ($this->request->is('post')) {
			foreach ($this->request->data['enrollments'] as $enrollmentData) {
				$enrolled = Enrollments::find('first', ['conditions' => [
					'class_id' => $enrollmentData['class_id'],
					'semester_id' => $enrollmentData['semester_id'],
					'student_id' => $enrollmentData['student_id'],
				]]);
				if (!$enrolled->exists()) {
					$enrollment = Enrollments::create();
					$enrollment->save($enrollmentData);
				}
			}
		}

		return compact('enrollment');
	}

	public function adminCreate() {
		return $this->_create();
	}

	public function prgLeadCreate() {
		return $this->_create();
	}

	public function acdServCreate() {
		return $this->_create();
	}


	/**
	 * [ short description of PREFIXView action ]
	 *
	 * [ long description of PREFIXView action ]
	 *
	 * @todo   Incomplete
	 * @url GET /enrollments/1
	 */
	public function PREFIXView() {
		$enrollment = Enrollments::find($this->request->id);
		return compact('enrollment');
	}
}

?>