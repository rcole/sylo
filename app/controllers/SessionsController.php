<?php

namespace app\controllers;

use app\models\Users;
use app\models\Roles;
use app\models\Sessions;
use lithium\security\Auth;
use lithium\g11n\Message;

use app\extensions\helper\Debug;

class SessionsController extends \app\extensions\action\Controller {

	/**
	 * Logs user in.
	 *
	 * Public login function, the point at which we decide the role of the
	 * user who logs in and set up his session vars, etc.
	 * All can directly access this.
	 *
	 * All defined roles are iterated through against Auth::check() to
	 * determine which user is logged in.
	 * Upon a successful login, user is redirected to the path defined in
	 * `roles`.`home`.
	 * On an invalid login, validation errors are presented.
	 *
	 * @url    POST /users/login
	 */
	public function create() {
		$user = Users::create();

		if ($this->request->is('post')) {
			extract(Message::aliases());
			if ($auth = Auth::check('default', $this->request)) {
				if (!isset($auth['id'])) {
					$this->_destroy();
					return $this->redirect(['Sessions::create']);
					die();
				}
				$user = Users::find($auth['id'], ['with' => ['RolesUsers', 'RolesUsers.Roles']]);
				Sessions::initiate(compact('user'));
				$this->flashSuccess($t('Welcome {:fullName}!', [
					'fullName' => Sessions::fullName()
				]));
				return $this->redirect(['Sylo::index']);
			}
			$this->flashError($t('Incorrect email or password.'));
		}
		return compact('user');
	}

	/**
	 * Generic logout method.
	 *
	 * Clears sessions and user authentication defined in:
	 *     app/config/bootstrap/session.php
	 *
	 * @see    app/config/bootstrap/session.php
	 */
	protected function _destroy() {
		Auth::clear('default');
		Sessions::clear();

		extract(Message::aliases());
		$this->flashSuccess($t('You have been logged out.'));
		return $this->redirect(['Sessions::create']);
	}

	public function adminDestroy() {
		return $this->_destroy();
	}

	public function prgLeadDestroy() {
		return $this->_destroy();
	}

	public function teacherDestroy() {
		return $this->_destroy();
	}

	public function acdServDestroy() {
		return $this->_destroy();
	}

	public function extVerDestroy() {
		return $this->_destroy();
	}

	public function studRecDestroy() {
		return $this->_destroy();
	}

	public function studentDestroy() {
		return $this->_destroy();
	}
}
?>