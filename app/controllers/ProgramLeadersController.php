<?php

namespace app\controllers;

use app\models\ProgramLeaders;
use lithium\g11n\Message;

use app\extensions\helper\Debug;

class ProgramLeadersController extends \app\extensions\action\Controller {

	/**
	 * [ short description of PREFIXDestroy action ]
	 *
	 * [ long description of PREFIXDestroy action ]
	 *
	 * @todo   Incomplete
	 * @url    DELETE /programLeaders/:id
	 */
	public function PREFIXDestroy() {
		extract(Message::aliases());
		$programLeader = ProgramLeaders::find($this->request->id);

		if ($programLeader->delete()) {
			$this->flashSuccess($t('The programLeader was deleted.'));
			return $this->redirect(['ProgramLeaders::index']);
		}

		$this->flashError($t('The programLeader could not be deleted.'));
		return $this->redirect($this->request->referer());
	}

	/**
	 * [ short description of PREFIXEdit action ]
	 *
	 * [ long description of PREFIXEdit action ]
	 *
	 * @todo   Incomplete
	 * @url    GET /programLeaders/:id/edit
	 * @url    PUT /programLeaders/:id/edit
	 */
	public function PREFIXEdit() {
		$programLeader = ProgramLeaders::find($this->request->id);
		if ($this->request->is('put')) {
			extract(Message::aliases());

			if ($programLeader->save($this->request->data)) {
				$this->flashSuccess($t('The programLeader was updated.'));
				return $this->redirect([
					'ProgramLeaders::view',
					'id' => $programLeader->id
				]);
			}
			$this->flashError($t('The programLeader could not be updated.'));
		}
		return compact('programLeader');
	}

	/**
	 * [ short description of PREFIXIndex action ]
	 *
	 * [ long description of PREFIXIndex action ]
	 *
	 * @todo   Incomplete
	 * @url    GET /programLeaders
	 * @url    GET /programLeaders/index
	 */
	public function PREFIXIndex() {
		$limit = 10;
		$page  = $this->request->page ?: 1;

		$programLeaders = ProgramLeaders::all([
			'limit' => $limit,
			'page'  => $page,
			'order' => ['id' => 'ASC']
		]);

		$total = ProgramLeaders::find('count');
		return compact('programLeaders', 'total', 'limit', 'page');
	}

	/**
	 * [ short description of PREFIXCreate action ]
	 *
	 * [ long description of PREFIXCreate action ]
	 *
	 * @todo   Incomplete
	 * @url    GET  /programLeaders/create
	 * @url    POST /programLeaders/create
	 */
	public function PREFIXCreate() {
		$programLeader = ProgramLeaders::create();

		if ($this->request->is('post')) {
			extract(Message::aliases());

			if ($programLeader->save($this->request->data)) {
				$this->flashSuccess($t('The programLeader was added.'));
				return $this->redirect([
					'ProgramLeaders::view',
					'id' => $programLeader->id
				]);
			}
			$this->flashError($t('The programLeader could not be added.'));
		}

		return compact('programLeader');
	}

	/**
	 * Allows users with role `PREFIX` to search within this model.
	 *
	 * @todo   Incomplete
	 * @see    app\models\ProgramLeaders
	 * @url    GET /programLeaders/search/q=<search term>&<field_1>=&<field_2>...
	 */
	public function PREFIXSearch() {
		return $this->search();
	}

	/**
	 * [ short description of PREFIXView action ]
	 *
	 * [ long description of PREFIXView action ]
	 *
	 * @todo   Incomplete
	 * @url GET /programLeaders/1
	 */
	public function PREFIXView() {
		$programLeader = ProgramLeaders::find($this->request->id);
		return compact('programLeader');
	}
}

?>